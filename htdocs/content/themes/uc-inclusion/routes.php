<?php

/**
 * Theme routes.
 *
 * The routes defined inside your theme override any similar routes
 * defined on the application global scope.
 */

//Route::get('/', 'SiteController@index');
//Route::get('singular', ['equipo', 'uses' => 'About@index']);

Route::get('singular', ['post', 'uses' => 'PostController@show']);
Route::get('template', ['proyectos-recursos', 'uses' => 'SiteController@projectsResources']);
Route::get('template', ['equipo-pace', 'uses' => 'SiteController@showPaceTeam']);
Route::get('template', ['equipo', 'uses' => 'SiteController@Team']);
Route::get('template', ['listado-categorias', 'uses' => 'SiteController@categoryList']);
Route::get('template', ['listado-experiencias-inclusivas', 'uses' => 'SiteController@inclusiveExperiences']);
Route::get('template', 'SiteController@templates');
Route::get('singular', ['equipo', 'uses' => 'TeamController@show']);
Route::get('single', 'SiteController@single');
Route::get('post-type-archive', ['noticias', 'uses' => 'News@index']);
//Route::get('/noticias', 'News@index');
//Route::post('/contacto', 'SiteController@templates');


//Route::get('templates', 'SiteController@templates');

// Paginas Front

Route::get('/front-home', function () {
    return view('pages/front-home');
});

Route::get('/front-admision-equidad', function () {
    return view('pages/front-admision-equidad');
});

Route::get('/front-apoyo-a-estudiantes', function () {
    return view('pages/front-apoyo-a-estudiantes');
});

Route::get('/front-comunidad-inclusiva', function () {
    return view('pages/front-comunidad-inclusiva');
});

Route::get('/front-pace-uc', function () {
    return view('pages/front-pace-uc');
});

Route::get('/front-educacion-media', function () {
    return view('pages/front-educacion-media');
});

Route::get('/front-educacion-superior', function () {
    return view('pages/front-educacion-superior');
});

Route::get('/front-piane-uc', function () {
    return view('pages/front-piane-uc');
});


Route::get('/front-estudiantes', function () {
    return view('pages/front-estudiantes');
});

Route::get('/front-profesores', function () {
    return view('pages/front-profesores');
});

Route::get('/front-comunidad-accesible', function () {
    return view('pages/front-comunidad-accesible');
});

Route::get('/front-noticias', function () {
    return view('pages/front-noticias');
});

Route::get('/front-single-noticias', function () {
    return view('pages/front-single-noticias');
});

Route::get('/front-contacto', function () {
    return view('pages/front-contacto');
});

Route::get('/front-equipo', function () {
    return view('pages/front-equipo');
});

Route::get('/front-single-equipo', function () {
    return view('pages/front-single-equipo');
});

Route::get('/front-apoyos-academicos', function () {
    return view('pages/front-apoyos-academicos');
});

Route::get('/front-proyectos-y-recursos', function () {
    return view('pages/front-proyectos-y-recursos');
});

Route::get('/front-single', function () {
    return view('pages/front-single');
});

Route::get('/front-diagnostico-y-nivelacion', function () {
    return view('pages/front-diagnostico-y-nivelacion');
});