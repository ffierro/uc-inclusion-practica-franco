<?php

namespace Theme\Providers;

use Illuminate\Support\ServiceProvider;

class ThemeServiceProvider extends ServiceProvider
{
    public function register()
    {
        $theme = $this->app->make('wp.theme');
    }
}