<?php

namespace Theme\Providers;

use Illuminate\Support\ServiceProvider;
use Themosis\Core\ThemeManager;
use Themosis\Support\Facades\Asset;

class AssetServiceProvider extends ServiceProvider
{
    /**
     * Theme Assets
     *
     * Here we define the loaded assets from our previously defined
     * "dist" directory. Assets sources are located under the root "assets"
     * directory and are then compiled, thanks to Laravel Mix, to the "dist"
     * folder.
     *
     * @see https://laravel-mix.com/
     */
    public function register()
    {
        /** @var ThemeManager $theme */
        $theme = $this->app->make('wp.theme');

        Asset::add('theme_styles', 'css/theme.css', [], $theme->getHeader('version'))->to('front');
        Asset::add('fancybox_styles', 'css/fancybox.min.css', [], $theme->getHeader('version'))->to('front');
        Asset::add('jquery_js', 'js/libs/jquery.min.js', [], $theme->getHeader('version'))->to('front');
        Asset::add('popper_js', 'js/libs/popper.min.js', [], $theme->getHeader('version'))->to('front');
        Asset::add('bootstrap_js', 'js/libs/bootstrap.min.js', [], $theme->getHeader('version'))->to('front');
        Asset::add('matchHeight_js', 'js/libs/matchHeight.min.js', [], $theme->getHeader('version'))->to('front');
        Asset::add('fancybox_js', 'js/libs/fancybox.min.js', [], $theme->getHeader('version'))->to('front');
        Asset::add('aos_js', 'js/libs/aos.min.js', [], $theme->getHeader('version'))->to('front');
        Asset::add('slick_js', 'js/libs/slick.min.js', [], $theme->getHeader('version'))->to('front');
        Asset::add('theme_js', 'js/theme.min.js', [], $theme->getHeader('version'))->to('front');
        Asset::add('custom_js', 'js/custom.js', [], $theme->getHeader('version'))->to('front');
    }
}


