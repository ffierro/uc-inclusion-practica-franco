<?php

namespace Theme\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use WP_Query;

class PostController extends Controller
{
    const pagination = 8;

    public function __construct()
    {
        // Call assets here in order to share them between multiple requests.
    }

    public function show(Request $request, $post, $query)
    {
        return view('singles/' . $post->post_type, [
            'post'   => $post,
            'fields' => get_fields($post->id),
            'theme'  => app('wp.theme')

        ]);
    }
}
