<?php

namespace Theme\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use WP_Query;

class TeamController extends Controller
{
    public function show(Request $request, $post, $query)
    {


        $fields = get_fields($post->id);
        $_args = array(

            'post_type'          => 'equipo',
            'show_option_all'    => '',
            'orderby'            => 'name',
            'order'              => 'ASC',
            'style'              => 'list',
            'show_count'         => 0,
            'hide_empty'         => 1,
            'use_desc_for_title' => 1,
            'show_option_none'   => __(''),
            'number'             => null,
            'echo'               => 1,
            'depth'              => 1,
            'taxonomy'           => 'categorias',
        );

        $allTerms = get_terms($_args);
        $_allTerms = [];
        $teamTaxonomy = [];
        $_tags_include = [];
        $_tags_exclude = [];


        foreach ($allTerms as $item) {
            $_allTerms[] = $item->slug;

        }


        $filter_tags_items = [];
        $teams = null;
        if (isset($fields['categoria_equipo']) && is_array($fields['categoria_equipo'])) {

            foreach ($fields['categoria_equipo'] as $item) {

                $teamTaxonomy[] = $item->slug;
                $filter_tags_items[] = [
                    'taxonomy' => 'categorias',
                    'field'    => 'slug',
                    'terms'    => $item->slug
                ];
            }


            $tags = array_diff($_allTerms, $teamTaxonomy);

            foreach ($tags as $tag) {
                $_tags_exclude[] = $tag;
            }

            $_arg =
                [
                    'post_type'      => 'equipo',
                    'post__not_in'   => array($post->ID),
                    'post_status'    => 'publish',
                    'posts_per_page' => -1,
                    'tax_query'      => [
                        'relation' => 'AND',
                        [
                            'taxonomy' => 'categorias',
                            'field'    => 'slug',
                            'terms'    => $_tags_exclude,
                            'operator' => 'NOT IN'
                        ],
                        $filter_tags_items
                    ]
                ];

            $teams = query_posts($_arg);

        }

        return view('singles/equipo', [
            'post'   => $post,
            'fields' => get_fields($post->id),
            'theme'  => app('wp.theme'),
            'teams'  => $teams

        ]);
    }
}
