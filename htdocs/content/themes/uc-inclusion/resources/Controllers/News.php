<?php

namespace Theme\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use WP_Query;

class News extends Controller
{
  const pagination = 8;

  public function __construct()
  {
    // Call assets here in order to share them between multiple requests.
  }

  public function index(Request $request)
  {
    //https://stackoverflow.com/questions/24838864/how-do-i-get-pagination-to-work-for-get-posts-in-wordpress/24847152
    $filter_tags = [];
    $current_filter_slug = '';
    $terms = get_terms([
      'taxonomy' => 'etiqueta',
      'hide_empty' => false,
    ]);


    $paged = $request->input('pag') ? $request->input('pag') : 1;
    $filter = $request->input('filter');
    $section = $request->input('section');


    if ($filter) {

      if ($filter !== 'all') {
        $filter_tags =
          [
            [
              'taxonomy' => 'etiqueta',
              'field' => 'slug',
              'terms' => $filter
            ]
          ];
      }
      $current_filter_slug = $filter;
    }

    if ($section) {

      if ($section !== 'all') {
        $filter_tags =
          [
            [
              'taxonomy' => 'categorias_noticias',
              'field' => 'slug',
              'terms' => $section
            ]
          ];
      }
      $current_filter_slug = $filter;
    }

    // PAGINADOR
    // Obtenemos todos los post

    $all_post_news = get_posts([
      'post_type' => 'noticias',
      'posts_per_page' => -1,
      'paged' => $paged,
      'tax_query' => $filter_tags,
    ]);
    //how many total posts are there?
    $post_count = count($all_post_news);
    //how many pages do we need to display all those posts?
    $num_pages = ceil($post_count / self::pagination);
    //let's make sure we don't have a page number that is higher than we have posts for
    if ($paged > $num_pages || $paged < 1) {
      $paged = $num_pages;
    }

    $news = get_posts([
      'post_type' => 'noticias',
      'posts_per_page' => self::pagination,
      'paged' => $paged,
      'tax_query' => $filter_tags,

    ]);


    return view('templates.page.news', [
      'news' => $news,
      'terms' => $terms,
      'current_filter_slug' => $current_filter_slug,
      'pagination_post_per_page' => self::pagination,
      'pagination_post_count' => $post_count,
      'pagination_num_pages' => $num_pages,
      'pagination_paged' => $paged,

    ]);

  }
}
