<?php

namespace Theme\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use SiteFunction;
use WP_Query;

class SiteController extends Controller
{
	const pagination = 8;

	public function __construct()
	{
		// Call assets here in order to share them between multiple requests.
	}

	public function showPaceTeam(Request $request, $post, $query)
	{
		$parent = wp_get_post_parent_id($post);
		$filter_tags =
			[
				[
					'taxonomy' => 'categorias',
					'field'    => 'slug',
					'terms'    => 'pace-uc'
				]
			];


		$all_post = get_posts([
			'post_type'      => 'equipo',
			'posts_per_page' => -1,
			'tax_query'      => $filter_tags

		]);


		return view('templates/' . $post->post_type . '/' . get_page_template_slug($post->id), [
			'parent' => $parent ? get_post($parent) : null,
			'post'   => $post,
			'teams'  => $all_post,
			'fields' => get_fields($post->id),

		]);
	}

	public function projectsResources(Request $request, $post, $query)
	{
		$parent = wp_get_post_parent_id($post);
		$paged = $request->input('pag') ? $request->input('pag') : 1;
		$paged_resources = $request->input('paged_resources') ? $request->input('paged_resources') : 1;

		$filter = $request->input('filter');
		$filter_tags = [];


		if ($filter) {

			if ($filter !== 'all') {
				$filter_tags =
					[
						[
							'taxonomy' => 'categorias',
							'field'    => 'slug',
							'terms'    => $filter
						]
					];
			}
			$current_filter_slug = $filter;
		}
		$all_post = get_posts([
			'post_type'      => 'descarga',
			'posts_per_page' => -1,
			'paged'          => $paged,
			'tax_query'      => $filter_tags,

		]);
		//how many total posts are there?
		$post_count = count($all_post);
		//how many pages do we need to display all those posts?
		$num_pages = ceil($post_count / self::pagination);
		//let's make sure we don't have a page number that is higher than we have posts for
		if ($paged > $num_pages || $paged < 1) {
			$paged = $num_pages;
		}
		$downloads_project = get_posts([
			'post_type'      => 'descarga',
			// 'posts_per_page' => self::pagination,
			'posts_per_page' => -1,
			//  'paged'          => $paged,
			//  'tax_query'      => $filter_tags,
			'meta_key'       => 'tipo_de_descarga',
			'meta_value'     => 'proyecto'

		]);
		$downloads_resources = get_posts([
			'post_type'      => 'descarga',
			'posts_per_page' => -1,
			// 'paged'          => $paged,
			//'tax_query'      => $filter_tags,
			'meta_key'       => 'tipo_de_descarga',
			'meta_value'     => 'recursos'

		]);


		$videos = get_posts([
			'post_type'      => 'post',
			'posts_per_page' => -1,
			'meta_key'       => 'tipo_entrada',
			'meta_value'     => 'video'

		]);
		return view('templates/' . $post->post_type . '/' . get_page_template_slug($post->id), [
			'post'                => $post,
			'fields'              => get_fields($post->id),
			'parent'              => $parent ? get_post($parent) : null,
			'theme'               => app('wp.theme'),
			'downloads'           => $downloads_project,
			'downloads_resources' => $downloads_resources,
			'videos'              => $videos,

			'pagination_post_per_page' => self::pagination,
			'pagination_post_count'    => $post_count,
			'pagination_num_pages'     => $num_pages,
			'pagination_paged'         => $paged,
		]);
	}

	public function index($post, $query)
	{

		$sliders = get_field('sliders', $post->id);
		$top_text = get_field('top_texto', $post->id);
		$programs_title = get_field('titulo_programa', $post->id);
//        $programs = get_posts([
//            'post_type'      => 'post',
//            'posts_per_page' => -1,
//            'tax_query'      => [
//                'taxonomy' => 'tipo_de_entrada',
//                'field'    => 'slug',
//                'terms'    => 'programas' // you need to know the term_id of your term "example 1"
//
//            ]
//        ]);

		$programs = get_field('programa', $post->id);


		$news = get_posts([
			'post_type'      => 'noticias',
			'posts_per_page' => 3,

		]);
		$experience = [
			'experience_title'       => get_field('experiencias_inclusivas_titulo', $post->id),
			'experience_description' => get_field('experiencias_inclusivas_descripcion', $post->id),
			'experience_name'        => get_field('experiencias_inclusivas_nombre', $post->id),
			'experience_rol'         => get_field('experiencias_inclusivas_rol', $post->id),
			'experience_image'       => get_field('experiencias_inclusivas_imagen', $post->id),
		];

		return view('templates/page/home', [
			'programs_title' => $programs_title,
			'sliders'        => $sliders,
			'top_text'       => strip_tags($top_text, '<strong>'),
			'programs'       => $programs,
			'news'           => $news,
			'experience'     => $experience
		]);

	}

	public function equipo($post, $query)
	{


		$parent = wp_get_post_parent_id($post);
		return view('templates/page/' . get_page_template_slug($post->id), [
			'post'   => $post,
			'fields' => get_fields($post->id),
			'parent' => $parent ? get_post($parent) : null,
			'theme'  => app('wp.theme')
		]);

	}

	public function team($post, $query)
	{

		$fields = get_fields($post->ID);

//        $allTerms = get_terms(array(
////
////            'post_type'          => 'equipo',
////            'show_option_all'    => '',
////            'orderby'            => 'name',
////            'order'              => 'ASC',
////            'style'              => 'list',
////            'show_count'         => 0,
////            'hide_empty'         => 1,
////            'use_desc_for_title' => 1,
////            'show_option_none'   => __(''),
////            'number'             => null,
////            'echo'               => 1,
////            'depth'              => 1,
////            'taxonomy'           => 'categorias',
////        ));
////        foreach ($allTerms as $item) {
////            $_allTerms[] = $item->slug;
////
////        }
////        $tags_exclude = array_diff($_allTerms, ['pace-uc', 'piane-uc']);
////
////
////        $_arg =
////            [
////                'post_type'      => 'equipo',
////                'post__not_in'   => array($post->ID),
////                'post_status'    => 'publish',
////                'posts_per_page' => -1,
////                'tax_query'      => [
////                    'relation' => 'AND',
////                    [
////                        'taxonomy' => 'categorias',
////                        'field'    => 'slug',
////                        'terms'    => $tags_exclude,
////                        'operator' => 'NOT IN'
////                    ],
////                    [
////                        'taxonomy' => 'categorias',
////                        'field'    => 'slug',
////                        'terms'    => ['pace-uc', 'piane-uc'],
////                        'operator' => 'IN'
////                    ],
////                ]
////            ];

		if (is_array($fields) && isset($fields['categorias'])) {

			$categorias = $fields['categorias'];
			$_categorias = [];

			foreach ($categorias as $categoria) {
				$_categorias[] = $categoria['item'];
			}

			$_arg =
				[
					'post_type'      => 'equipo',
					'post__not_in'   => array($post->ID),
					'post_status'    => 'publish',
					'posts_per_page' => -1,
					'tax_query'      => [
						'relation' => 'AND',
						[
							'taxonomy' => 'categorias',
							'field'    => 'id',
							'terms'    => $_categorias,
							'operator' => 'IN'
						],
					],
					'orderby' => 'title',
					'order' => 'ASC',
				];

			$teams = query_posts($_arg);
			//dump($_categorias);

			$_integrantes = [];


			foreach ($teams as $item) {

				if (!$terms = get_the_terms($item->ID, 'categorias')) {
					continue;
				}

				foreach ($terms as $termItem) {
					if (!in_array($termItem->term_id, $_categorias)) {
						continue;
					}

					$_integrantes[$termItem->term_id]['name'] = $termItem->name;
					$_integrantes[$termItem->term_id]['category_id'] = $termItem->term_id;
					$_integrantes[$termItem->term_id]['content'][] = $item;
				}

			}
			$_integrantes=SiteFunction::sortArrayByArray($_integrantes,$_categorias);
		} else {
			$_integrantes = [];
		}

		return view('templates/page/equipo', [
			'post'            => $post,
			'fields'          => $fields,
			//'parent'          => $parent ? get_post($parent) : null,
			'teamsCategories' => $_integrantes,
			'theme'           => app('wp.theme')
		]);

	}

	public function inclusiveExperiences($post, $query)
	{
		$parent = wp_get_post_parent_id($post);
		$fields = get_fields($post->id);

		$itemId = isset($fields['id_pagina_experiencia']) ? $fields['id_pagina_experiencia'] : '';
		$postItems = get_field('experiencias_inclusivas', $itemId);


		return view('templates/' . $post->post_type . '/' . get_page_template_slug($post->id), [
			'post'   => $post,
			'fields' => $fields,
			'parent' => $parent ? get_post($parent) : null,
			'items'  => $postItems,
			'theme'  => app('wp.theme')
		]);
	}

	public function categoryList($post, $query)
	{
		$parent = wp_get_post_parent_id($post);
		$fields = get_fields($post->id);

		$categoryItems = $fields['categoria_a_utilizar'];

		$filter_tags =
			[
				[
					'taxonomy' => 'clasificacion-contenidos',
					'field'    => 'id',
					'terms'    => $categoryItems
				]
			];


		$items = get_posts([
			'post_type'      => 'post',
			'posts_per_page' => -1,
			'tax_query'      => $filter_tags

		]);


		return view('templates/' . $post->post_type . '/' . get_page_template_slug($post->id), [
			'post'   => $post,
			'fields' => $fields,
			'parent' => $parent ? get_post($parent) : null,
			'items'  => $items,
			'theme'  => app('wp.theme')
		]);
	}

	public function templates($post, $query)
	{

		$parent = wp_get_post_parent_id($post);
		return view('templates/' . $post->post_type . '/' . get_page_template_slug($post->id), [
			'post'   => $post,
			'fields' => get_fields($post->id),
			'parent' => $parent ? get_post($parent) : null,
			'theme'  => app('wp.theme')
		]);

	}

	public function single($post, $query)
	{
		return view('singles/' . $post->post_type, [
			'post'   => $post,
			'fields' => get_fields($post->id),
			'theme'  => app('wp.theme')

		]);
	}
}
