jQuery(document).on('gform_post_render', function (event, form_id, current_page) {

    $('#gform_fields_1 input[type="text"]').addClass('uc-input-style');
    $('#gform_fields_1 input[type="checkbox"]').addClass('uc-checkbox__input');
    $('#gform_fields_1 textarea').removeClass().addClass('uc-input-style').attr('rows', '');
    $('#gform_1 .gform_button').addClass('uc-btn btn-cta btn-inline');

    $('#gform_1 .gfield_checkbox input').addClass('uc-checkbox__input');
    $('#gform_1 .gfield_checkbox label').addClass('uc-checkbox__label');

    $('#gform_1 .gform_body li').each(function (index, element) {

        $(this).wrap("<fieldset class=\"uc-form-group mb-28\"></fieldset>");

    });

    $('#gform_1 .gform_button').wrap("<div class='text-right'></div>");
});


$('#news_filter').change(function (event) {
    event.preventDefault();
    let value = $(this).val();

    if (value) {
        window.location.href = "/noticias/?filter=" + value;

    }


});

$('a.show_hide_elements').on('click', function (event) {
    event.preventDefault();
    $('.item_video').show();
    $(this).remove();
});
// $('.uc-like-btn').on('click', function () {
//     $.ajax({
//         url: themosis.ajaxurl, // Global access to the WordPress ajax handler file
//         type: 'POST',
//         dataType: 'json',
//         data: {
//             action: 'my-custom-action', // Your custom hook/action name
//             security: themosis.nonce, // A nonce value defined by the user with the "add-posts" action
//             number: 2 // The value you want to send
//         }
//     }).done(function (data) {
//         // This should print "4" in the console.
//         console.log(data);
//     });
// });


$('#projects_filter').on('submit', function (e) {
    e.preventDefault();
    return;
});
$('#buscador_proyectos_recursos').on('keyup', function (e) {
    var timeout = null
    var input_value = $(this).val();
    if (!input_value) {
        return;
    }


    var tab = $('.uc-tabs_item-link.active').data('tabtarget');

    clearTimeout(timeout)
    timeout = setTimeout(function () {
        $.ajax({
            url: themosis.ajaxurl, // Global access to the WordPress ajax handler file
            type: 'POST',
            dataType: 'json',
            data: {
                action: 'getProjectsByTab', // Your custom hook/action name
                security: themosis.nonce, // A nonce value defined by the user with the "add-posts" action
                tab: tab, // The value you want to send
                keyword: input_value // The value you want to send
            }
        }).done(function (data) {
            // var returnedData = JSON.parse(data);

            if (data.results > 0) {
                $('div.uc-tab-body .custom_tab[data-tab="' + data.tab + '"]').html('').append(data.content);
            } else {
                $('div.uc-tab-body .custom_tab[data-tab="' + data.tab + '"]').html('');

            }

            //  console.log(data.results);
            // console.log(data);
        });
    }, 500)

});


$(document).ready(function () {

    $('body a').filter(function () {

        return this.hostname !== location.hostname
    }).attr('target', '_blank')

    $('body a').filter(function () {
        return this.hostname.length == 0
    }).attr('target', '')

});