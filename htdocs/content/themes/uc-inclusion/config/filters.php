<?php

use Themosis\Support\Facades\Filter;

Filter::add('themosis_front_global', function ($data) {
    $data['nonce'] = wp_create_nonce('add-posts');
    return $data;
});