<?php

/**
 * Edit this file in order to configure your theme templates.
 *
 * You can define just a template slug by only defining a key.
 * For a better user experience, you can define a display title as a
 * value and as a second argument, you can specify a list of post types
 * where your template is available.
 */
return [
    'custom-template'                 => [__('Custom Template', THEME_TD), ['page']],
    'admision-equidad'                => [__('Admisión Equidad', THEME_TD), ['page']],
    'apoyo-estudiantes'               => [__('Apoyo Estudiantes', THEME_TD), ['page']],
    'comunidad-inclusiva'             => [__('Comunidad Inclusiva', THEME_TD), ['page']],
    'pace-uc'                         => [__('Pace UC', THEME_TD), ['page']],
    'piane-uc'                        => [__('Piane UC', THEME_TD), ['page']],
    'piane-uc-estudiantes'            => [__('Piane UC - Estudiantes', THEME_TD), ['page']],
    'piane-uc-profesores'             => [__('Piane UC - Profesores', THEME_TD), ['page']],
    'piane-uc-comunidad-accesible'    => [__('Piane UC - Comunidad Accesible', THEME_TD), ['page']],
    'home'                            => [__('Home', THEME_TD), ['page']],
    'equipo'                          => [__('Equipo', THEME_TD), ['page']],
    'equipo-pace'                     => [__('Equipo Pace', THEME_TD), ['page']],
    'comunidad-accesible'             => [__('Comunidad Accesible', THEME_TD), ['page']],
    'educacion-media'                 => [__('Educación Media', THEME_TD), ['page']],
    'educacion-superior'              => [__('Educación Superior', THEME_TD), ['page']],
    'contacto'                        => [__('Contacto', THEME_TD), ['page']],
    'apoyo-academico'                 => [__('Apopyo Academico', THEME_TD), ['page']],
    'proyectos-recursos'              => [__('Proyectos y Recursos', THEME_TD), ['page']],
    'diagnostico-nivelacion'          => [__('Diagnosticos y Nivelación', THEME_TD), ['page']],
    'listado-categorias'              => [__('Listado de categorías', THEME_TD), ['page']],
    'listado-experiencias-inclusivas' => [__('Listado de Experiencias Inclusivas', THEME_TD), ['page']],
];
