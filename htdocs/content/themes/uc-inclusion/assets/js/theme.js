//Función que agrega clases según dispositivo
function responsive() {
  if ($(window).width() < 992) {
    $("body")
      .addClass("mobile")
      .removeClass("desktop");
  } else {
    $("body")
      .removeClass("mobile")
      .addClass("desktop");
  }
}

//Validación formulario
function handleFormSubmission() {
  $(".form input, .form textarea").on("keyup", function(e) {
    $(this).addClass("edited");
  });

  $('.form [type="submit"]').on("click.formValidation", function(e) {
    shouldPrevent = false;
    errorList = [];

    $form = $(this).parents("form");
    $inputs = $form.find(
      "input[required], textarea[required], select[required]"
    );

    $inputs.each(function(index, input) {
      errorMessageSelector =
        'label[for="' + $(input).attr("id") + '"] .error-message';
      $form.find(errorMessageSelector).removeAttr("aria-live");
      error = $form.find(errorMessageSelector);
      error.css("display", "none");

      if (!input.validity.valid) {
        error.css("display", "inline-block");
        shouldPrevent = true;
        errorList.push(error);
      } else {
        error.css("display", "none");
      }
    });

    if (!$form[0].checkValidity()) {
      e.preventDefault();
      errorList[0].attr("aria-live", "assertive");
    }
  });
}

// Función para envolver tablas en contenedor para mobile
function tableResize() {
  $(".wp-content table").wrap(
    '<div class="gradient-table"><div class="table-responsive"></div></div>'
  );
}

// Función para envolver iframes en contenedor para mobile
function iframeResize() {
  $(
    '.wp-content iframe[src*="youtube.com"], .wp-content iframe[src*="vimeo.com"]'
  ).wrap('<div class="iframe-responsive"></div>');
  $('.intro iframe[src*="youtube.com"], .intro iframe[src*="vimeo.com"]').wrap(
    '<div class="iframe-responsive"></div>'
  );
}

$(document).ready(function() {
  //Carruseles
  $(".hero__carousel").slick({
    slidesToShow: 1,
    arrows: false,
    dots: true,
    autoplay: true,
    pauseOnDotsHover: true,
    speed: 1600,
    autoplaySpeed: 9000,
  });

  $("#pause").click(function() {
    $(".hero__carousel").slick("slickPause");
    $("#play").show();
    $(this).hide();
  });

  $("#play").click(function() {
    $(".hero__carousel").slick("slickPlay");
    $("#pause").show();
    $(this).hide();
  });

  $(".three-items-carousel").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    prevArrow:
      '<button class="slick-prev" title="Ver item anterior" aria-label="Ver item anterior"></button>',
    nextArrow:
      '<button class="slick-next" title="Ver item siguiente" aria-label="Ver item siguiente"></button>',
    dots: true,
    dotsClass: "carousel-counter",
    infinite: false,
    customPaging: function(slider, i) {
      return (
        '<span class="carousel-counter__visible">' +
        (i + 3) +
        "</span> " +
        " / " +
        ' <span class="carousel-counter__total">' +
        slider.slideCount +
        "</span> "
      );
    },
    speed: 300,
    responsive: [
      {
        breakpoint: 993,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 561,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: "20px",
        },
      },
    ],
  });
  $(".four-items-carousel").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    prevArrow:
      '<button class="slick-prev" title="Ver item anterior" aria-label="Ver item anterior"></button>',
    nextArrow:
      '<button class="slick-next" title="Ver item siguiente" aria-label="Ver item siguiente"></button>',
    dots: true,
    infinite: false,
    dotsClass: "carousel-counter",
    customPaging: function(slider, i) {
      return (
        '<span class="carousel-counter__visible">' +
        (i + 4) +
        "</span> " +
        " / " +
        ' <span class="carousel-counter__total">' +
        slider.slideCount +
        "</span> "
      );
    },
    speed: 300,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 993,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 561,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });

  $(".five-items-carousel").slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: true,
    prevArrow:
      '<button class="slick-prev" title="Ver item anterior" aria-label="Ver item anterior"></button>',
    nextArrow:
      '<button class="slick-next" title="Ver item siguiente" aria-label="Ver item siguiente"></button>',
    dots: true,
    infinite: false,
    dotsClass: "carousel-counter",
    customPaging: function(slider, i) {
      return (
        '<span class="carousel-counter__visible">' +
        (i + 4) +
        "</span> " +
        " / " +
        ' <span class="carousel-counter__total">' +
        slider.slideCount +
        "</span> "
      );
    },
    speed: 300,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 561,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
    ],
  });

  $(".uc-navbar_mobile-button").click(function() {
    $(this).toggleClass("open");
    if ($(this).hasClass("open")) {
      $(this).attr("aria-expanded", "true");
      $(this).attr("aria-label", "Cerrar menú principal");
      $("#menu-mobile").attr("aria-hidden", "false");
    } else {
      $(this).attr("aria-expanded", "false");
      $(this).attr("aria-expanded", "Abrir menú principal");
      $("#menu-mobile").attr("aria-hidden", "true");
    }
  });

  //Popover
  $("[data-toggle=popover]").popover({
    html: true,
    sanitize: false,
    content: function() {
      var content = $(this).attr("data-popover-content");
      return $(content)
        .children(".popover-body")
        .html();
    },
  });

  //Fancybox
  $("[data-fancybox]").fancybox({
    animationEffect: "fade",
    buttons: ["close"],
    lang: "es",
    i18n: {
      es: {
        CLOSE: "Cerrar",
        NEXT: "Siguiente",
        PREV: "Anterior",
        ERROR:
          "El contenido requerido no pudo ser cargado. <br/> Por favor intenta más tarde.",
        PLAY_START: "Comenzar presentación",
        PLAY_STOP: "Pausar presentación",
        FULL_SCREEN: "Pantalla completa",
        THUMBS: "Imágenes miniaturas",
        DOWNLOAD: "Descargar",
        SHARE: "Compartir",
        ZOOM: "Zoom",
      },
    },
    afterShow: function() {
      $(".site").attr("aria-hidden", "true");
      $(".fancybox-container").attr("aria-hidden", "false");
      $(".fancybox-container").attr("aria-describedby", "modalDescription");
      $(".fancybox-container").prepend(
        '<div id="modalDescription" class="hide">Este es un modal que contiene y muestra las imagenes de la galería a mayor tamaño, puedes navegar entre las imágenes.</div>'
      );
      $(".fancybox-container").attr("role", "dialog");
    },
    afterClose: function() {
      $(".site").attr("aria-hidden", "false");
      $(".fancybox-container").attr("aria-hidden", "true");
    },
  });

  // ACCORDION
  $("#accordion-uc")
    .find(".accordion-toggle")
    .click(function() {
      //Expand or collapse this panel
      $(this)
        .next()
        .slideToggle("fast");

      //Hide the other panels
      $(".accordion-content")
        .not($(this).next())
        .slideUp("fast");
    });

  //Sameheight
  $(function() {
    $(".sameheight").matchHeight();
  });

  //Funciones
  handleFormSubmission();
  responsive();
  iframeResize();
  tableResize();
});

$(window).on("resize", function() {
  responsive();
});
