@extends('layouts/main')

@section('content')
  <?php
  $video = isset($fields['video_lsch']) ? $fields['video_lsch'] : null;
  $accordeon = isset($fields['acordeon']) ? $fields['acordeon'] : null;
  $image_content = get_post(get_post_thumbnail_id($post));

  ?>
@section('breadcrumbs')
  @include('parts.breadcrumbs',['post'=>$post])
@endsection

<main class="container" role="main" id="main-content">
  <div class="row">
    <div class="col-md-7">
      <h1 class="uc-h1  pr-5 mb-32">{!! $post->post_title !!}</h1>
      @if($video)
        @include('parts.video_lsch', ['video' => $video])
      @endif
      <div class="paragraph">
        {!! wpautop($post->post_content) !!}

      </div>
    </div>
    @if($image_content)
      @php
        $image=fly_get_attachment_image_src($image_content->ID, [500, 280]);
      @endphp
      @if($image)
        <div class="col-md-5 wp-content">
          <img src="{{$image['src']}}" class="img-fluid" alt="{{$post->post_title}}">
        </div>
      @endif
    @endif
  </div>
  {{-- <div class="mb-4 mb-lg-5">
      @foreach($teamsCategories as $teamCategory)
          <hr class="uc-hr mt-1 mb-2">
          <div class="d-flex align-items-center mb-32">
              <h2 class="uc-h2">{{$teamCategory['name']}}</h2>
              <span class="uc-heading-decoration"></span>
          </div>
          @if(isset($fields['categorias']))
              <div class="row five-columns">
                  @foreach($teamCategory['content'] as $key => $item)
                      <div class="col-sm-6 col-md-4 col-lg-3 col-xl mb-32">
                          @include('parts/entry_card_team_single',['item'=>$item])
                      </div>
                  @endforeach
              </div>
          @endif
      @endforeach
  </div> --}}
  @if($accordeon)
    <div id="accordion-uc">
      @foreach($accordeon as $accordeonItem)
        <div class="accordion-toggle">
          <h4>{{$accordeonItem['titulo']}}</h4>
          <div class="more">
            Ver más
            <i class="uc-icon icon-shape--rounded">
              expand_more
            </i>
          </div>
        </div>
        @if(isset($accordeonItem['equipo']))
          <div class="accordion-content">
            <div class="content-grid">
              @foreach($accordeonItem['equipo'] as $key => $item)
                @include('parts/entry_card_team_single',['item'=>$item])
              @endforeach
            </div>
          </div>
        @endif
      @endforeach
    </div>
  @endif

  {{--  <div id="accordion-uc">--}}
  {{--        @foreach($teamsCategories as $teamCategory)--}}
  {{--            <div class="accordion-toggle">--}}
  {{--                <h4>{{$teamCategory['name']}}</h4>--}}
  {{--                <div class="more">--}}
  {{--                    Ver más--}}
  {{--                    <i class="uc-icon icon-shape--rounded">--}}
  {{--                        expand_more--}}
  {{--                    </i>--}}
  {{--                </div>--}}
  {{--            </div>--}}
  {{--            @if(isset($fields['categorias']))--}}
  {{--                <div class="accordion-content">--}}
  {{--                    <div class="content-grid">--}}
  {{--                    @foreach($teamCategory['content'] as $key => $item)--}}
  {{--                        @include('parts/entry_card_team_single',['item'=>$item])--}}
  {{--                    @endforeach--}}
  {{--                    </div>--}}
  {{--                </div>--}}
  {{--            @endif--}}
  {{--        @endforeach--}}
  {{--    </div>--}}

</main>

@endsection
