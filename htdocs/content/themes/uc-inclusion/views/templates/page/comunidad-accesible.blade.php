@extends('layouts/main')
@php
    $parent_posts = get_posts([
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'

]);


@endphp
@section('breadcrumbs')
    @include('parts.breadcrumbs',['post'=>$post])
@endsection

@section('content')
    @php
        $content = apply_filters('the_content', $post->post_content);
        $video_embed=isset($fields['video'])?$fields['video']:null;
        $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;

    @endphp
    <!--Fin Breadcrumb-->
    <main class="container" role="main" id="main-content">
        <div class="mb-4 mb-lg-5">
            <div class="row">
                <div class="col-md-8 col-lg-9 mb-32">
                    <h1 class="uc-h1 pr-5 mb-0">{{$post->post_title}}</h1>
                    @if($video)
                        @include('parts.video_lsch', ['video' => $video])
                    @endif
                </div>
                <div class="col-md-8 col-lg-9 mb-24">
                    <div class="paragraph wp-content">
                        {!! wpautop($content) !!}
                        @isset($fields['video'])
                            {{$video}}
                        @endisset
                    </div>
                </div>
                <nav class="col-md-4 col-lg-3" role="navigation" aria-label="Submenú PIANE UC">
                    <div class="box">
                        <h4 class="uc-subtitle mb-16">PIANE UC:</h4>
                        <a class="uc-btn text-weight--medium mb-16" href="">Estudiantes</a>
                        <a class="uc-btn text-weight--medium mb-16" href="">Profesores</a>
                        <a class="uc-btn text-weight--medium mb-16" href="">Comunidad accesible</a>
                    </div>
                </nav>
            </div>
        </div>
        <!--Material de interés destacado-->
        <section class="mb-80 mb-lg-8" aria-label="Material de interés destacado">
            <div class="row mb-40">
                <div class="col-md-8 col-lg-9">
                    <h2 class="uc-h2 title-decorated mb-32">Material de interés destacado</h2>
                    <div class="paragraph">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec rutrum nisl. Sed ut
                            tincidunt ligula, egestas scelerisque ante. Nullam at tellus gravida, varius ipsum quis,
                            pulvinar augue. Aenean cursus, elit laoreet sagittis euismod, est massa luctus tortor.</p>
                    </div>
                </div>
            </div>
            <div class="row mb-40">
                <?php for( $a = 0; $a < 3; $a++ ) { ?>
                <div class="col-sm-6 col-lg-4 mb-24">
                    <article class="uc-card card-height--same">
                        <img src="http://via.placeholder.com/300x190" class="img-fluid">
                        <div class="uc-card_body">
                            <h3 class="uc-h4">Lenguaje sobre discapacidad</h3>
                            <div class="uc-text-divider divider-primary my-12"></div>
                            <p class="mb-24">Un lenguaje acorde al modelo social de la discapacidad es el que se ha
                                promovido desde la campaña del SENADIS.</p>
                            <div class="text-right mt-auto">
                                <a href="#" class="uc-btn btn-inline">
                                    Descargar PDF de SENADIS
                                    <i class="uc-icon" aria-hidden="true">save_alt</i>
                                </a>
                            </div>
                            <div class="lsch lsch--hover">
                                <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                </button>
                                <div id="lsch__popover-1" style="display:none;">
                                    <div class="popover-body">
                                        <video width="150" height="200" controls>
                                            <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <?php } ?>
            </div>
            <div class="text-right  mt-24">
                <a href="#" title="" class="uc-btn btn-inline">
                    Ver otros
                    <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
                </a>
            </div>
        </section>
        <!--Fin Material de interés destacado-->
    </main>
@endsection
