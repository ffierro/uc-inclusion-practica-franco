@extends('layouts.main')
@php


    $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;


@endphp
@section('breadcrumbs')
    @include('parts.breadcrumbs',['post'=>$post])
@endsection

@section('content')

    <div class="container">
        <main class="mb-4 mb-lg-5" role="main" id="main-content">
            <div class="row mb-40">
                <div class="col-md-12 mb-32">
                    <h1 class="uc-h1 pr-5 mb-0">{{$post->post_title}}</h1>
                    @if($video)
                        @include('parts/video_lsch', ['video' => $video])

                    @endif
                </div>
                <div class="col-md-8 col-lg-9 mb-24">
                    <div class="paragraph">
                        {!! wpautop($post->post_content) !!}
                    </div>
                </div>
            </div>
            <!--Listado de documentos-->
            <section class="row justify-content-center">
                <div class="col-md-10">
                    <div class="uc-tabpanel" data-tabpanel>
                        <!-- Tabs mobile se muestran como Select -->
                        <div class="uc-card card-bg--gray card-radius--none card-border--none d-block d-lg-none mb-32">
                            <div class="uc-card_body">
                                <label for="tabSelect"><strong>Seleccione tab</strong></label>
                                <select name="tabSelect" id="tabSelect" class="uc-input-style" data-tabselect>
                                    <option value="tab-01">Proyectos y Recursos</option>
                                    
                                </select>
                            </div>
                        </div>
                        <!-- Tabs en desktop se muestran como botones -->
                        <ul class="uc-tabs mb-32 d-none d-lg-flex">
                            <li class="uc-tabs_item">
                                <a href="javascript:void(0);" class="uc-tabs_item-link" data-tabtarget="tab-01"
                                   data-tabactive>Proyectos y Recursos</a>
                            </li>
                            
                        </ul>
                        <form class="uc-filters mb-40" id="projects_filter" aria-label="Filtro de proyectos" role="form">
                            <fieldset class="uc-form-group mb-0">
                                <label class="hide" for="buscador">Buscar</label>
                                <input id="buscador_proyectos_recursos" type="text" class="uc-input-style w-icon search"
                                       placeholder="Buscar por">
                                <span class="w-icon search" aria-hidden="true"></span>
                                <section id="resultado">
</section>
                            </fieldset>
                        </form>

                        <div class="uc-tab-body">
                            <div class="custom_tab" data-tab="tab-01">
                                @if($downloads)

                                    @include('parts/projects_resources_downloads_projects',
                                                ['resources' => $downloads])
                                    {{--                                    @include('parts/paginator', [--}}
                                    {{--                             'pagination_post_count' => $pagination_post_count,--}}
                                    {{--                             'pagination_post_per_page' => $pagination_post_per_page,--}}
                                    {{--                             'pagination_paged' => $pagination_paged,--}}
                                    {{--                             'pagination_num_pages' => $pagination_num_pages,--}}
                                    {{--                             ])--}}
                                @endif

                            

                            
                              
                        </div>
                    </div>
                </div>
            </section>
            <!--Fin Listado de documentos-->
        </main>
        <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
        <!--Videos-->
        <section class="mb-80 mb-lg-8" role="complementary" aria-label="Videos">
            <h2 class="uc-h2 title-decorated mb-32">@if(isset($fields['titulo_videos'])){{$fields['titulo_videos']}}@endif</h2>
            <div class="row">
                @php
                    $counter=0;
                @endphp
                @foreach($videos as $video)
                    @php
                        $fields_videos=get_fields($video->ID);
                        $video_title=$video->post_title;
                        $video_embed=isset($fields_videos['video_embed'])?$fields_videos['video_embed']:null;
                        if(!$video_embed){
                            continue;
                        }


                    @endphp
                    <div class="col-sm-6 col-lg-4 mb-32 item_video" @if($counter>=6) style="display: none" @endif>
                        <article class="uc-card card-height--same">
                            <div class="iframe-responsive">
                                {!! wpautop($video_embed) !!}
                            </div>
                            <div class="uc-card_body">
                                <h3 class="uc-h4">{{$video_title}}</h3>
                            </div>
                        </article>
                    </div>
                    @php $counter ++;@endphp
                @endforeach
            </div>
            @if($counter >6)
                <div class="text-center">
                    <a href="#" title="" class="uc-btn btn-inline show_hide_elements">
                        Ver más videos
                        <i class="uc-icon icon-shape--rounded" aria-hidden="true">add</i>
                    </a>
                </div>
            @endif
        </section>
        <!--Fin Videos-->
    </div>

@endsection
