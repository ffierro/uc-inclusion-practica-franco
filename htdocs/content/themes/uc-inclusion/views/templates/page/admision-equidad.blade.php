@extends('layouts/main')
@section('breadcrumbs')
    @include('parts.breadcrumbs',['post'=>$post])
@endsection
@section('content')
    @php
        $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;

    @endphp
    <!--Content-->
    <main class="container" role="main" id="main-content">
        <div class="row">
            <div class="col-md-12 mb-32">
                <h1 class="uc-h1 pr-5 mb-0">{{$post->post_title}}</h1>
                @if($video)
                    @include('parts.video_lsch', ['video' => $video])
                @endif
            </div>
            <div class="col-md-8 col-lg-9 mb-24">
                <div class="paragraph">
                    {!! wpautop($post->post_content) !!}
                </div>
            </div>
        </div>
        <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
        @isset($fields['contenidos'])

            <section class="boxes-reverse mb-80 mb-lg-8" aria-label="Vías de admisión">
                @foreach($fields['contenidos'] as $key => $content)
                    @php
                        $image=SiteFunction::getImageDataFromAttachment($content['imagen']['ID'],[1050,1050],true);
                        $video=isset($content['video_lsch'])?$content['video_lsch']:null;
                    @endphp
                    <article class="uc-card card-type--horizontal card-type--horizontal--xl boxes-reverse__item">
                        @isset($image)
                            <div class="uc-card_img">
                                <img src="{{$image['src']}}" class="img-fluid" alt="{{$content['imagen']['alt']}}">
                            </div>
                        @endisset
                        <div class="uc-card_body">
                            <h2 class="uc-h3 mb-16">{{$content['titulo']}}</h2>
                            <div class="paragraph mb-16">
                                {!! $content['descripcion'] !!}
                            </div>
                            <div class="text-right mt-auto">
                                <a href=" {{$content['link']}}" title="{{$content['titulo_link']}}"
                                   class="uc-btn btn-inline">
                                    {{$content['titulo_link']}}
                                    <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                </a>
                            </div>
                            @if($video)
                                @if($video)
                                    @include('parts.video_lsch', ['video' => $video])
                                @endif
                            @endif
                        </div>
                    </article>
                @endforeach
            </section>
        @endisset

    </main>
    <!--Fin Content-->

@endsection
