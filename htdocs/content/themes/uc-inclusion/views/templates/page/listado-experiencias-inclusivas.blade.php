@extends('layouts/main')

@section('content')
    @php
        $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;
        $image_content = get_post(get_post_thumbnail_id($post));

    @endphp
@section('breadcrumbs')
    @include('parts.breadcrumbs',['post'=>$post])
@endsection

    <main class="container" role="main" id="main-content">
        <div class="row">
            <div class="col-md-7">
                <h1 class="uc-h1  pr-5 mb-32">{!! $post->post_title !!}</h1>
                @if($video)
                    @include('parts/video_lsch', ['video' => $video])
                @endif
                <div class="paragraph">
                    {!! wpautop($post->post_content) !!}

                </div>
            </div>
        </div>
        <!--Experiencias inclusivas-->
        @foreach($items as $experiencia)
            @php
                $activar=isset($experiencia['activar'])?$experiencia['activar']:null;
                $titulo=isset($experiencia['experiencias_inclusivas_titulo_pagina_landing'])?$experiencia['experiencias_inclusivas_titulo_pagina_landing']:null;
                $descripcion=$experiencia['experiencias_inclusivas_descripcion'];
                $nombre=$experiencia['experiencias_inclusivas_nombre'];
                $rol=$experiencia['experiencias_inclusivas_rol'];
                $video=isset($experiencia['video_lsch'])?$experiencia['video_lsch']:null;
                $imagen=isset($experiencia['experiencias_inclusivas_imagen'])?$experiencia['experiencias_inclusivas_imagen']:null;
            @endphp
            <section class="container mb-4 mb-lg-5 pb-lg-4" aria-label="{{$titulo}}">
                <h2 class="uc-h2 title-decorated mb-32">{{$titulo}}</h2>
                <div class="row">
                    <div class="col-lg-5">
                        <blockquote class="blockquote">
                            <div class="blockquote__text">
                                {!! $descripcion !!}
                            </div>
                            <h5 class="blockquote__name">{{$nombre}}</h5>
                            <p class="blockquote__profile">{{$rol}}</p>
                            @if($video)
                                @include('parts.video_lsch', ['video' => $video])
                            @endif
                        </blockquote>
                    </div>
                    <div class="col-lg-7">
                        @if($imagen)
                            @php $image=SiteFunction::getImageDataFromAttachment($imagen['ID'],[690,600],true) @endphp
                            @if($image)

                                <img src="{{$image['src']}}" alt="{{$image['alt']}}"
                                     class="img-fluid">
                            @endif
                        @endif
                    </div>
                </div>
            </section>
    @endforeach
    </main>

@endsection