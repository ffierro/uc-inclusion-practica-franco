@extends('layouts/main')
@section('content')
  <?php
  $news = get_posts([
    'post_type' => 'noticias',
    'posts_per_page' => 3,
    'tax_query' =>
      [
        ['taxonomy' => 'categorias_noticias', 'field' => 'slug', 'terms' => 'inclusion-uc']
      ]

  ]);
  ?>
  <main role="main" id="main-content">
    <!--Hero-->
    @if($fields['sliders_items'] && $fields['sliders_items'] >=1)
      <section class="hero">
        <ul class="hero__carousel" aria-label="Carrusel de destacados">
          @foreach($fields['sliders_items'] as $key => $slider)
            <?php
            if ($slider['contenido_interno']) {
              if (!isset($slider['contenido_interno_relacion']->ID)) {
                continue;
              }
              $article_fields = get_fields($slider['contenido_interno_relacion']->ID);

              if ($slider['contenido_interno_relacion']->post_type == 'post') {
                if (!isset($article_fields['imagen_banner'])):
                  continue;
                endif;
                $image = SiteFunction::getImageDataFromAttachment($article_fields['imagen_banner']['ID'], [1440, 510]);
                $tag = $article_fields['tags'] ? $article_fields['tags'] : null;
                $tag_name = $article_fields['tags'] ? $tag->name : null;


              }
              if ($slider['contenido_interno_relacion']->post_type == 'noticias') {
                if (!isset($article_fields['imagen_fondo'])):
                  continue;
                endif;
                $image = SiteFunction::getImageDataFromAttachment($article_fields['imagen_fondo']['ID'], [1440, 510]);
              }
              $video = isset($article_fields['video_lsch']) ? $article_fields['video_lsch'] : null;

              $title = $slider['contenido_interno_relacion']->post_title;
              $url = get_permalink($slider['contenido_interno_relacion']->ID);
              $link_title = $slider['contenido_enlace_link'];
            } else {
              $video = isset($slider['video_lsch']) ? $slider['video_lsch'] : null;
              $tag_name = $slider['contenido_externo_tag'];
              $title = $slider['contenido_externo_titulo'];
              $image = SiteFunction::getImageDataFromAttachment($slider['imagen']['ID'], [1440, 510]);
              $url = $slider['contenido_externo_url'];
              $link_title = $slider['contenido_enlace_link'];

            }

            ?>


            @isset($image['src'])

              <li class="hero__carousel__item" style="background-image: url('{{$image['src']}}')">
                <div class="container">
                  <div class="row">
                    <div class="col-md-10">
                      @isset($tag)
                        <a href="#" title="Categoría - {{$tag_name}}"
                           class="uc-tag mb-24">{{$tag_name}}</a>
                      @endisset
                      <h2 class="mb-24">{{$title}}</h2>
                      <img class="hide" src="{{$image['src']}}" @if($image['alt'])alt="{{$image['alt']}}"@endif>
                      <a href="{{$url}}"
                         title="Ir a el artículo - {{$title}}"
                         class="hero__link uc-btn btn-inline">
                        {{$link_title}}
                        <i class="uc-icon icon-shape--rounded"
                           aria-hidden="true">arrow_forward</i>
                      </a>
                    </div>
                  </div>
                </div>

                @if($video)
                  @include('parts.video_lsch', ['video' => $video])
                @endif
              </li>
            @endisset

          @endforeach
        </ul>
        <div class="hero__controls">
          <button style="display: none" id="pause" aria-label="Pausar carrusel"><i
              class="uc-icon icon-shape--rounded" aria-hidden="true">pause</i></button>
          <button id="play" aria-label="Reanudar carrusel"><i class="uc-icon icon-shape--rounded"
                                                              aria-hidden="true">play_arrow</i></button>
        </div>
      </section>
    @endif
  <!--Fin hero-->
    <!--Mensaje-->
    @isset($fields['top_texto'])
      <section class="msg bg-lightgray py-20 mb-4 mb-md-80" aria-label="Mensaje informativo">
        <div class="container">
          <div class="msg__text paragraph"><span class="msg__icon"><img
                src="{{$theme->getUrl()}}/assets/images/icon-hands.svg"
                role="image" alt=""></span>{!! $fields['top_texto'] !!}</div>
        </div>
      </section>
    @endisset
  <!--Fin mensaje-->
    <!--Programas-->
    @isset($fields['programa'])
      <section class="container mb-4 mb-lg-5" aria-label="{{$fields['titulo_programa']}}">
        <h2 class="uc-h2 title-decorated mb-32">{{$fields['titulo_programa']}}</h2>
        <div class="row">
          @foreach($fields['programa'] as $key=>$item)
            @php $fields_prgrama=get_fields($item->ID) @endphp
            <div class="col-sm-6 col-xl-3 mb-24">
              @include('parts/entry_card',['item'=>$item,'section'=>'programs-home'])
            </div>
          @endforeach
        </div>
      </section>
    @endisset
  <!--Fin programas-->
    <!--Noticias-->
    @isset ($news)
      <section class="container mb-4 mb-lg-5" aria-label="@isset($data['title']){{$data['title']}}@endif">
        @include('parts/news_card_3_items',
        ['data' =>
        [
          'title'=>"Noticias",
          'link_title'=>"Ver todas las noticias",
          'link_title_url'=>"/noticias",
        ]
        ,'items'=>$news, 'section'=>'news-home'])
      </section>

    @endisset
  <!--Fin Noticias-->
    <!--Experiencias inclusivas-->
    @if(array_key_exists('experiencias_inclusivas',$fields))
      @foreach($fields['experiencias_inclusivas'] as $experiencia)
        @php
          $activar=isset($experiencia['activar'])?$experiencia['activar']:null;
          if(!$activar){
            continue;
          }
          $titulo=$experiencia['experiencias_inclusivas_titulo'];
          $cta=key_exists('experiencias_inclusivas_cta',$fields)?$fields['experiencias_inclusivas_cta']:null;
          $descripcion=$experiencia['experiencias_inclusivas_descripcion'];
          $nombre=$experiencia['experiencias_inclusivas_nombre'];
          $rol=$experiencia['experiencias_inclusivas_rol'];
          $video=isset($experiencia['video_lsch'])?$experiencia['video_lsch']:null;

          $imagen=$experiencia['experiencias_inclusivas_imagen'];
        @endphp

        <section class="container mb-4 mb-lg-5 pb-lg-4" aria-label="{{$titulo}}">
          <div class="d-sm-flex justify-content-between align-items-center mb-32">
            <h2 class="uc-h2 title-decorated">{{$titulo}}</h2>
            @if($cta)
              <a href="{{$cta['url']}}" title="{{$cta['title']}}"
                 class="uc-btn btn-inline d-none d-sm-block">
                {{$cta['title']}}
                <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
              </a>
            @endif
          </div>
          <div class="row">
            <div class="col-lg-5">
              <blockquote class="blockquote">
                <div class="blockquote__text">
                  {!! $descripcion !!}
                </div>
                <h5 class="blockquote__name">{{$nombre}}</h5>
                <p class="blockquote__profile">{{$rol}}</p>
                @if($video)
                  @include('parts.video_lsch', ['video' => $video])
                @endif
              </blockquote>
            </div>
            <div class="col-lg-7">
              @isset($imagen)
                @php $image=SiteFunction::getImageDataFromAttachment($imagen['ID'],[690,600],true) @endphp
                <img src="{{$image['src']}}" @if($image['alt'])alt="{{$image['alt']}}" @endif
                class="img-fluid">
              @endisset
            </div>
          </div>
          @if($cta)

            <div class="text-right d-sm-none">
              <a href="{{$cta['url']}}" title="{{$cta['title']}}" class="uc-btn btn-inline">
                {{$cta['title']}}
                <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
              </a>
            </div>
          @endif
        </section>
    @endforeach
    <!--Fin Experiencias inclusivas-->
  @endif
  @php

    $video=isset($fields['equipo_video_lsch'])?$fields['equipo_video_lsch']:null;
    $equipo_titulo=isset($fields['equipo_titulo'])?$fields['equipo_titulo']:null;
    $equipo_descripcion=isset($fields['equipo_descripcion'])?$fields['equipo_descripcion']:null;
    $equipo_link_title=isset($fields['equipo_titulo_link'])?$fields['equipo_titulo_link']:null;
    $equipo_link=isset($fields['equipo_url'])?$fields['equipo_url']:null;
    $equipo_imagen=isset($fields['equipo_imagen'])?$fields['equipo_imagen']:null;
  @endphp
  <!--Equipo-->
    <section class="container mb-80 mb-lg-8" aria-label="Equipo">
      <article class="uc-card card-type--horizontal card-type--horizontal--xl boxes-reverse__item uc-card-equipo-home">
        @if($equipo_imagen)
          @php

            $image=SiteFunction::getImageDataFromAttachment($equipo_imagen['ID'],[720,420]);
          @endphp
          @if($image)
            <div class="uc-card_img">
              <img src="{{$image['src']}}" class="img-fluid" alt="{{$equipo_imagen['alt']}}">
            </div>
          @endif
        @endif
        <div class="uc-card_body">
          <h2 class="uc-h2 mb-16">{{$equipo_titulo}}</h2>
          <div class="paragraph mb-16">
            {!! wpautop($equipo_descripcion) !!}
          </div>
          @if($equipo_link_title)
            <div class="text-right mt-auto">
              <a href="{{$equipo_link}}" title="{{$equipo_link_title}}" class="uc-btn btn-inline">
                {{$equipo_link_title}}
                <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
              </a>
            </div>
          @endif
          @if($video)
            @include('parts.video_lsch', ['video' => $video])
          @endif
        </div>
      </article>
    </section>
    <!--Fin equipo-->
  </main>
@endsection
