@extends('layouts/main')
@php
    $current_slug=get_permalink($post->ID);
    $parent_posts = get_posts([
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => wp_get_post_parent_id($post->ID),
    'order'          => 'ASC',
    'orderby'        => 'menu_order'

    ]);


    $post_parent=get_post( wp_get_post_parent_id($post->ID));
    $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;
    $content = apply_filters('the_content', $post->post_content);
    $video_embed=isset($fields['video'])?$fields['video']:null;


@endphp

@section('breadcrumbs')
    @include('parts.breadcrumbs',['post'=>$post])
@endsection


@section('content')
    <main class="container" role="main" id="main-content">
        <div class="mb-4 mb-lg-5">
            <div class="row">
                <div class="col-md-8 col-lg-9 mb-32">
                    <h1 class="uc-h1 pr-5 mb-0">{{$post->post_title}}</h1>
                    @if($video)
                        @include('parts/video_lsch', ['video' => $video])

                    @endif
                </div>
                <div class="col-md-8 col-lg-9 mb-24">
                    <div class="paragraph wp-content">
                        {!! wpautop($content) !!}

                        @isset($video_embed)
                            {!! $video_embed !!}
                        @endisset
                    </div>
                </div>
                <nav class="col-md-4 col-lg-3" role="navigation" aria-label="Submenú PIANE UC">
                    <div class="box">
                        <h4 class="uc-subtitle mb-16">PIANE UC:</h4>
                        @foreach($parent_posts as $key => $item)
                            @php
                                $active='';
                                $slug=get_permalink($item->ID);

                            if($slug==$current_slug){
                                $active='current';
                            }
                            @endphp
                            <a class="uc-btn text-weight--medium mb-16 {{$active}}" title="{{$item->post_title}}"
                               href="{{ get_permalink($item->ID)}}">{{$item->post_title}}</a>
                        @endforeach
                    </div>
                </nav>
            </div>
        </div>
        <!--Material de interés destacado-->
        <section class="mb-80 mb-lg-8" aria-label="{{$fields['material_de_interes_titulo']}}">
            <div class="row mb-40">
                <div class="col-md-8 col-lg-9">
                    <h2 class="uc-h2 title-decorated mb-32">{{$fields['material_de_interes_titulo']}}</h2>
                    <div class="paragraph">
                        <p>{{$fields['material_de_interes_descripcion']}}</p>
                    </div>
                </div>
            </div>
            @isset($fields['material_de_interes_contenidos'])
                <div class="row mb-40">
                    @foreach($fields['material_de_interes_contenidos'] as $key => $item)
                        <div class="col-sm-6 col-lg-4 mb-24">

                            @include('parts/entry_card',['item'=>$item])

                        </div>
                    @endforeach
                </div>
            @endisset

            @isset($fields['material_de_interes_contenidos_links'])

                <div class="text-right mt-24">
                    <a href="{{$fields['material_de_interes_contenidos_links']['url']}}" title="{{$fields['material_de_interes_contenidos_links']['title']}}"
                       class="uc-btn btn-inline">
                        {{$fields['material_de_interes_contenidos_links']['title']}}
                        <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
                    </a>
                </div>
            @endif
        </section>
        <!--Fin Material de interés destacado-->
    </main>
@endsection
