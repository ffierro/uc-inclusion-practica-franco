@extends('layouts/main')

@section('content')
	@php
		$current_slug=get_permalink($post->ID);
		$parent_posts = get_posts([
			'post_type'      => 'page',
			'posts_per_page' => -1,
			'post_parent'    => 295,
			'order'          => 'ASC',
			'orderby'        => 'menu_order'

		]);

		$video=isset($fields['video_lsch'])?$fields['video_lsch']:null;

	@endphp
@section('breadcrumbs')
	@include('parts.breadcrumbs',['post'=>$post])
@endsection

<main role="main" id="main-content">
	<div class="container mb-4 mb-lg-5">
		<div class="row">
			<div class="col-md-8 col-lg-9 mb-32">
				<h1 class="uc-h1 pr-5 mb-0">{{$post->post_title}}</h1>
				@if($video)
					@include('parts.video_lsch', ['video' => $video])
				@endif
			</div>
			<div class="col-md-8 col-lg-9 mb-24">
				<div class="paragraph">
					{!! wpautop($post->post_content) !!}
				</div>
			</div>
			<nav class="col-md-4 col-lg-3" role="navigation" aria-label="Submenú PIANE UC">
				<div class="box">
					<h4 class="uc-subtitle mb-16">PIANE UC:</h4>
					@foreach($parent_posts as $key => $item)
						@php
							$active='';
							$slug=get_permalink($item->ID);

						if($slug==$current_slug){
							$active='current';
						}
						@endphp
						<a class="uc-btn text-weight--medium mb-16 {{$active}}" title="{{$item->post_title}}"
						   href="{{ get_permalink($item->ID)}}">{{$item->post_title}}</a>
					@endforeach
				</div>
			</nav>
		</div>
	</div>
@php   $image=SiteFunction::getImageDataFromAttachment($fields['imagen_caja']['ID'],[700,420]);
        $video=isset($fields['apoyo_academico_video_lsch'])?$fields['apoyo_academico_video_lsch']:null;
        $video=isset($fields['docencia_inclusiva_video'])?$fields['docencia_inclusiva_video']:null;

@endphp
@isset($image)

	<!--Docencia inclusiva-->
		<section class="mb-4 mb-lg-5" aria-label="{{$fields['titulo_caja']}}">
			<div class="bg-lightgray pt-4 pt-lg-5">
				<div class="container">
					<div class="uc-card card-type--horizontal card-type--horizontal--xl mb-40">
						<div class="uc-card_img">
							@isset($image)
								<img src="{{$image['src']}}" class="img-fluid"
									 alt="{{$fields['imagen_caja']['alt']}}">
							@endisset
						</div>
						<div class="uc-card_body">
							<h2 class="uc-h2 mb-16">{{$fields['titulo_caja']}}</h2>
							<div class="paragraph">
								<p>{{$fields['descripcion_caja']}}</p>
							</div>
							@if($video)
								@include('parts.video_lsch', ['video' => $video])
							@endif
						</div>
					</div>
					<div class="paragraph">
						{!! $fields['descripcion_inferior_caja'] !!}
					</div>
				</div>
			</div>
			@isset($fields['contenido_multimedia'])
				<div class="bg-lightgray--xs pt-4 pt-lg-5">
					<div class="container">
						<div class="cards-carousel three-items-carousel">
							@foreach($fields['contenido_multimedia'] as $key => $item )
								<div class="cards-carousel__item sameheight">
									@include('parts/entry_card',['item'=>$item])
								</div>
							@endforeach
						</div>
					</div>
				</div>
			@endisset
		</section>
@endisset

<!--Fin Docencia inclusiva-->
@isset($fields['titulo_informacion_acompanamiento'])
	@php
		$image=isset($fields['imagen_informacion_acompanamiento'])?SiteFunction::getImageDataFromAttachment($fields['imagen_informacion_acompanamiento']['ID'],[700,420]):null;
		$video_aco=isset($fields['video_lsch_informacion_acompanamiento'])?$fields['video_lsch_informacion_acompanamiento']:null;

	@endphp
	<!--Información y acompañamiento-->
		<section class="bg-lightgray py-4 py-lg-5" aria-label="Información y acompañamiento">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 mb-24">
						<h2 class="uc-h2 title-decorated mb-32">{{$fields['titulo_informacion_acompanamiento']}}</h2>
						<div class="paragraph wp-content">
							{!! wpautop($fields['descripcion_informacion_acompanamiento']) !!}
						</div>
						@if(key_exists('cta_acompanamiento',$fields) and isset($fields['cta_acompanamiento']))
							@if(isset($fields['cta_acompanamiento']['url']))
								<a href="{{$fields['cta_acompanamiento']['url']}}"
								   title="{{$fields['cta_acompanamiento']['title']}}" class="uc-btn btn-inline mt-32">
									{{$fields['cta_acompanamiento']['title']}}
									<i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
								</a>
							@endif
						@endif
					</div>
					<div class="col-lg-6 d-lg-flex align-items-start">
						@if($video)
							<div class="lsch mb-24 mr-32 ml-16">
								<button tabindex="0" type="button" class="lsch__button"
										data-popover-content="#lsch__popover" data-toggle="popover"
										data-placement="bottom">
								</button>
								<div id="lsch__popover" style="display:none;">
									<div class="popover-body">
										<video width="150" height="200" controls>
											<source src="{{$video['url']}}"
													type="video/mp4">
										</video>
									</div>
								</div>
							</div>
						@endif
						@if($image)
							<img src="{{$image['src']}}" class="img-fluid"
								 alt="{{$fields['imagen_informacion_acompanamiento']['alt']}}">
						@endif
					</div>
				</div>
			</div>
		</section>
		<!--Fin Información y acompañamiento-->
	@endisset
</main>
@endsection
