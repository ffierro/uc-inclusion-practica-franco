@extends('layouts/main')
@section('breadcrumbs')
    @include('parts.breadcrumbs',['post'=>$post])
@endsection
@section('content')
    @php
        $title=isset($fields['titulo_formulario'])?$fields['titulo_formulario']:null;
        $subtitle=isset($fields['bajada_formulario'])?$fields['bajada_formulario']:null;
        $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;

    @endphp

    <!--Content-->
    <main class="container" role="main" id="main-content">
        <h1 class="uc-h1 mb-32">{{$post->post_title}}</h1>
        <div class="row mb-4 mb-lg-5">
            <div class="col-lg-5 mb-32">
                {!! wpautop($post->post_content) !!}
                @isset($fields['mapa'])
                    <div class="iframe-responsive mt-32">
                        <iframe aria-hidden="true"
                                src="{{$fields['mapa']}}"
                                width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                @endisset

            </div>
            <div class="col-lg-7">
                <div class="uc-card">
                    @if($video)
                        @include('parts.video_lsch', ['video' => $video])

                    @endif
                    <div class="p-44">
                        <h2 class="h4 mb-32">{{$title}}</h2>
                        <div class="paragraph">
                            <p>{{$subtitle}}</p>
                        </div>
                    </div>
                    <hr class="uc-hr">
                    <?php  echo gravity_form(1, false, false, false, '', true); ?>

                </div>
            </div>
        </div>
    </main>
    <!--Fin Content-->
@endsection
