@extends('layouts/main')

@section('content')
    @php
        $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;
        $image_content = get_post(get_post_thumbnail_id($post));

    @endphp
@section('breadcrumbs')
    @include('parts.breadcrumbs',['post'=>$post])
@endsection

<main class="container" role="main" id="main-content">
    <div class="row">
        <div class="col-md-7">
            <h1 class="uc-h1 pr-5 mb-32">{!! $post->post_title !!}</h1>
            @if($video)
                @include('parts/video_lsch', ['video' => $video])
            @endif
            <div class="paragraph">
                {!! wpautop($post->post_content) !!}

            </div>
        </div>
        @if($image_content)
            @php
                $image=fly_get_attachment_image_src($image_content->ID, [500, 280]);
            @endphp
            @if($image)
                <div class="col-md-5 wp-content">
                    <img src="{{$image['src']}}" class="img-fluid" alt="{{$post->post_title}}">
                </div>
            @endif
        @endif
    </div>
    <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
    @if($teams)
        <div class="row five-columns mb-80 mb-lg-8">
            @foreach($teams as $key => $item)
                <div class="col-sm-6 col-md-4 col-lg-3 col-xl mb-32">
                    @include('parts/entry_card_team_single',['item'=>$item])
                </div>
            @endforeach

        </div>
    @endif
</main>

@endsection