@extends('layouts/main')
@section('breadcrumbs')
    @include('parts.breadcrumbs',['post'=>$post])
@endsection


@section('content')
    @php
        $content = apply_filters('the_content', $post->post_content);
        $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;
    @endphp
    <main role="main" class="container" id="main-content">
        <div class="row">
            <div class="col-md-7 mb-24">
                <h1 class="uc-h1 pr-5 mb-32">{{$post->post_title}}</h1>
                @if($video)

                    @include('parts/video_lsch', ['video' => $video])

                @endif
                <div class="paragraph">
                    {!! wpautop($content) !!}
                </div>
            </div>
            @php
                $image_content = get_post(get_post_thumbnail_id($post));
                $desplegar_video = isset($fields['desplegar_video']) ? $fields['desplegar_video'] : null;
                $video = isset($fields['video']) ? $fields['video'] : null;


                //$alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
            @endphp
            @if($desplegar_video && $video)
                <div class="col-md-5 wp-content">
                    {!! wpautop($video) !!}
                </div>
            @else
                @isset($image_content->ID)
                    @php
                        $image=fly_get_attachment_image_src($image_content->ID, [490, 270]);
                        $image_alt=$image_content->post_title;
                    @endphp
                    @isset($image['src'])
                        <div class="col-md-5">
                            <img src="{{$image['src']}}" class="img-fluid" alt="{{$image_alt}}">
                        </div>
                    @endisset
                @endisset
            @endif
        </div>
        <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
        <!--Talleres y capacitaciones-->
        <section class="mb-4 mb-lg-5" aria-label="{{$fields['talleres_y_capacitaciones_titulo']}}">
            <h2 class="uc-h2 title-decorated mb-32">{{$fields['talleres_y_capacitaciones_titulo']}}</h2>
            @if($fields['talleres_y_capacitaciones_contenidos'])

                <div class="cards-carousel three-items-carousel" aria-label="Slider de {{$fields['talleres_y_capacitaciones_titulo']}}">

                    @foreach($fields['talleres_y_capacitaciones_contenidos'] as $key => $item)
                        <div class="cards-carousel__item sameheight">
                            @include('parts/entry_card',['item'=>$item])
                        </div>
                    @endforeach
                </div>
            @endif
        </section>
        <!--Fin Talleres y capacitaciones-->
        <!--Boxes-->
        @isset($fields['cajas'])
            <div class="boxes-reverse mb-80 mb-lg-8">
                @foreach($fields['cajas'] as $key => $content)
                    @php
                        $image=SiteFunction::getImageDataFromAttachment($content['imagen_cajas']['ID'],[1050,1050],true);
                        $video=isset($content['video_lsch'])?$content['video_lsch']:null;
                        $link=isset($content['link'])?$content['link']:null;
                    @endphp
                    <section class="uc-card card-type--horizontal card-type--horizontal--xl boxes-reverse__item"
                             aria-label="{{$content['titulo_caja']}}">
                        @isset($image)
                            <div class="uc-card_img">
                                <img src="{{$image['src']}}" class="img-fluid"
                                     alt="{{$content['imagen_cajas']['alt']}}">
                            </div>
                        @endisset
                        <div class="uc-card_body">
                            <h2 class="uc-h2 mb-16">{{$content['titulo_caja']}}</h2>
                            <div class="paragraph">
                                {!! wpautop($content['cuerpo_caja']) !!}
                            </div>
                            @if($link)
                                <div class="text-right mt-auto">
                                    <a href=" {{$link['url']}}" title="{{$link['title']}}" class="uc-btn btn-inline">
                                        {{$link['title']}}
                                        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                    </a>
                                </div>
                            @endif
                            @if($video)
                                @include('parts.video_lsch', ['video' => $video])

                            @endif
                        </div>
                    </section>
                @endforeach
            </div>
    @endisset
    <!--Fin Boxes-->
    </main>
@endsection
