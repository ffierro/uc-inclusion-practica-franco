@extends('layouts/main')
@section('breadcrumbs')
    @include('parts.breadcrumbs',['post'=>$post])
@endsection
@section('content')
    @php
        $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;
        $content = apply_filters('the_content', $post->post_content);
        $media_type = isset($fields['tipo_de_media'])?$fields['tipo_de_media']:null;

    @endphp
    <main class="container" role="main" id="main-content">
        <div class="row">
            <div class="col-md-7 mb-24">
                <h1 class="uc-h1 pr-5 mb-32">{{$post->post_title}}</h1>
                @if($video)
                    @include('parts/video_lsch', ['video' => $video])

                @endif
                <div class="paragraph">
                    {!! wpautop($content) !!}
                </div>
            </div>
            <div class="col-md-5 wp-content">
                @if($media_type=='image')
                    @php $image=SiteFunction::getImageData($post->ID,[490,270],true)@endphp

                    @isset($image)
                        <img src="{{$image['src'] }}" class="img-fluid" alt="{{$image['alt']}}">
                    @endisset
                @endif
                @if($media_type=='video')
                    @php $video=isset($fields['video'])?$fields['video']:null;@endphp

                    @isset($video)
                        {!! $video!!}
                    @endisset
                @endif
            </div>
        </div>
        <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">

    @if($fields['programas'])
        <!--Conoce más sobre el programa PACE UC-->
            <section class="mb-4 mb-lg-5" aria-label="Conoce más sobre el programa PIANE UC">
                <h2 class="uc-h2 title-decorated mb-32">{{$fields['programa_titulo']}}</h2>
                <div class="row">
                    @foreach($fields['programas'] as $key => $item)
                        <div class="col-sm-6 col-lg-4 mb-24">
                            @include('parts/entry_card',['item'=>$item])
                        </div>
                    @endforeach
                </div>
            </section>
            <!--Fin Conoce más sobre el programa PACE UC-->
    @endif

    @if(isset($fields['equipo']))

        <!--Equipo-->
            <section class="mb-4 mb-lg-5" aria-label="Equipo">
                <div class="row mb-40">
                    <div class="col-md-8 col-lg-9">
                        <h2 class="uc-h2 title-decorated mb-32">Equipo</h2>
                        <div class="paragraph">
                            <p>{{$fields['descripcion_equipo']}}</p>
                        </div>
                    </div>
                </div>
                <div class="cards-carousel five-items-carousel">
                    @foreach($fields['equipo'] as $item)
                        <div class="cards-carousel__item sameheight">
                            @include('parts/entry_card_team_single',['item'=>$item])
                        </div>
                    @endforeach
                </div>
            </section>
            <!--Fin Equipo-->
    @endisset()


    <?php
    $news = get_posts([
        'post_type' => 'noticias',
        'posts_per_page' => 3,
        'tax_query' => [
            [
                'taxonomy' => 'categorias_noticias',
                'field' => 'slug',
                'terms' => 'piane-uc'
            ]
        ]
    ]);

    ?>
    @if(!empty($news))
        <!--Noticias PIANE UC-->
            <section class="mb-80 mb-lg-8" role="complementary">
            @include('parts/news_card_3_items',
            ['data' =>
            [
            'title'=>"Noticias PIANE UC",
            'link_title'=>"Ver todas las noticias PIANE UC",
            'link_title_url'=>"/noticias?section=piane-uc",
            ]
            ,'items'=>$news])
            <!--Fin Noticias PIANE UC-->
            </section>

        @endif
    </main>
@endsection
