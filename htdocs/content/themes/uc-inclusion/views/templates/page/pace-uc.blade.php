@extends('layouts.main')
@section('breadcrumbs')
  @include('parts.breadcrumbs',['post'=>$post])
@endsection

@section('content')
  @php
    $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;

  @endphp
  <main class="container" role="main" id="main-content">
    <div class="row">
      <div class="col-md-7 mb-24">
        <h1 class="uc-h1 pr-5 mb-32">{{$post->post_title}}</h1>
        @if($video)
          @include('parts.video_lsch', ['video' => $video])

        @endif
        <div class="paragraph">
          {!! wpautop($post->post_content) !!}

        </div>
      </div>
      @php
        $tipo_media=$fields['tipo_de_media']?$fields['tipo_de_media']:null;
        if($tipo_media && $tipo_media=='embed'){
            $media=$fields['embed'];
        }
        if($tipo_media && $tipo_media=='imagen'){
          $image=SiteFunction::getImageData($post->ID,[490,270],true);
        }
      @endphp

      @if($tipo_media)
        <div class="col-md-5 wp-content">
          @if($tipo_media=='imagen' && $imagen)
            <img src="{{$image['src'] }}"
                 class="img-fluid" alt="{{$image['alt']}}">
          @endif
          @if($tipo_media=='embed' && $media)
            <div class="iframe-responsive">
              {!! wpautop($media) !!}
            </div>
          @endif
        </div>
      @endif
    </div>
    <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">

  @isset($fields['programas'])
    <!--Conoce más sobre el programa PACE UC-->
      <section class="mb-4 mb-lg-5" aria-label="Conoce más sobre el programa PACE UC">
        <h2 class="uc-h2 title-decorated mb-32">{{$fields['programa_titulo']}}</h2>
        <div class="row">
          @foreach($fields['programas'] as $key => $item)
            <div class="col-md-4 mb-32">
              @include('parts/entry_card',['item'=>$item])
            </div>
          @endforeach
        </div>
      </section>
      <!--Fin Conoce más sobre el programa PACE UC-->
  @endisset
  @if(array_key_exists('liceos_asociados',$fields) && $fields['liceos_asociados'])

    <!--Liceos Asociados-->
      <section class="mb-4 mb-lg-5" aria-label="Liceos Asociados">
        <div class="row mb-40">
          <div class="col-md-8 col-lg-9">
            <h2 class="uc-h2 title-decorated mb-32">Liceos Asociados</h2>
          </div>
        </div>
        <div class="logos-carousel five-items-carousel">
          @foreach($fields['liceos_asociados'] as $key => $item)
            @php
              $image = SiteFunction::getImageDataFromAttachment($item['imagen']['ID'], [300, 190], true);
              $url=$item['enlace']?$item['enlace']:'#';
            @endphp
            @if($image && $url )
              <div class="cards-carousel__item sameheight">
                <a target="_blank"
                   class="uc-card sameheight d-flex align-items-center justify-content-center"
                   href="{{$url}}">
                  @isset($image['src'])
                    <img src="{{$image['src']}}" alt="{{$item['imagen']['alt']}}"
                         class="p-24 img-fluid">
                  @endisset
                </a>
              </div>
            @endif
          @endforeach
        </div>
      </section>
      <!--Fin Liceos Asociados-->
  @endisset
  @if(array_key_exists('equipo',$fields) && $fields['equipo'])
    <!--Equipo-->
      <section class="mb-4 mb-lg-5" aria-label="Equipo">

        <div class="row mb-40">
          <div class="col-md-12">
            <div class="d-sm-flex justify-content-between align-items-center mb-32">
              <h2 class="uc-h2 title-decorated">Equipo</h2>
              @php
                $url=isset($fields['enlace_equipo'])?$fields['enlace_equipo']:null;
              @endphp
              @if($url)
                <a href="{{$url['url']}}" title="{{$url['title']}}"
                   class="uc-btn btn-inline d-none d-sm-block">{{$url['title']}} <i
                    class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i></a>
              @endif
            </div>
          </div>
          <div class="col-md-8 col-lg-9">
            <div class="paragraph">
              <p>{{$fields['descripcion_equipo']}}</p>
            </div>
          </div>
        </div>
        <div class="cards-carousel five-items-carousel">
          @foreach($fields['equipo'] as $item)
            <div class="cards-carousel__item sameheight">
              @include('parts/entry_card_team_single',['item'=>$item])
            </div>
          @endforeach
        </div>
      </section>
      <!--Fin Equipo-->
  @endisset()
  <?php
  $news = get_posts(
    ['post_type' => 'noticias', 'posts_per_page' => 3,
     'tax_query' =>
       [
         ['taxonomy' => 'categorias_noticias', 'field' => 'slug', 'terms' => 'pace-uc']
       ]
    ]);
  ?>
  @isset($news)
    <!--Noticias PACE UC-->
      <section class="mb-80 mb-lg-8" role="complementary">
        @include('parts.news_card_3_items',
        ['data' =>
        [
            'title'=>"Noticias PACE UC",
            'link_title'=>"Ver todas las noticias PACE UC",
            'link_title_url'=>"/noticias?section=pace-uc",
        ]
        ,'items'=>$news,'section'=>'news-pace-uc'])
      </section>
      <!--Fin Noticias PACE UC-->
    @endisset
  </main>

@endsection
