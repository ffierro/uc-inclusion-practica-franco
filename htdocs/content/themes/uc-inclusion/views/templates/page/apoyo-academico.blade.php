@extends('layouts/main')


@section('breadcrumbs')
    @include('parts.breadcrumbs',['post'=>$post])
@endsection

@section('content')
    @php
        $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;
        $content = apply_filters('the_content', $post->post_content);


    @endphp


    <main class="container mb-4 mb-lg-5" role="main" id="main-content">
        <div class="row">
            <div class="col-md-12 mb-32">
                <h1 class="uc-h1 pr-5 mb-0">{{$post->post_title}}</h1>
                @if($video)
                    @include('parts/video_lsch', ['video' => $video])

                @endif
            </div>
            @php $image=SiteFunction::getImageData($post->ID,[384,234])@endphp
            @isset($image)
                <div class="col-sm-6 col-lg-4 mb-24">
                    <img src="{{$image['src']}}" class="img-fluid" alt="{{$image['alt']}}">
                </div>
            @endisset
            <div class="col-md-8">
                <div class="paragraph">
                    {!! wpautop($content) !!}
                </div>
            </div>
        </div>
        <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
        @if($fields['cajas'])

            <ul class="mb-80 mb-lg-8">
                @foreach($fields['cajas'] as $key => $box)
                    @php
                        $image=SiteFunction::getImageDataFromAttachment($box['imagen']['ID'],[280,216]);
                        $video=isset($box['video_lsch'])?$box['video_lsch']:null;

                    @endphp
                    <li class="uc-card card-type--horizontal align-items-start mb-32 p-32">
                        @if(isset($image['src']))
                            <div class="uc-card_img mb-32 mb-md-0 mr-md-2">
                                <img src="{{$image['src']}}" class="img-fluid" alt="{{$box['imagen']['alt']}}">
                            </div>
                        @endif
                        <div class="uc-card_body p-0">
                            <h3 class="uc-h4">{{$box['titulo']}}</h3>
                            <div class="uc-text-divider divider-primary my-12"></div>
                            <div class="paragraph">
                                {!! wpautop($box['descripcion'])!!}
                            </div>
                            @if($video)
                                @include('parts/video_lsch', ['video' => $video])
                            @endif
                        </div>
                    </li>
                @endforeach
            </ul>
        @endif

    </main>

@endsection

