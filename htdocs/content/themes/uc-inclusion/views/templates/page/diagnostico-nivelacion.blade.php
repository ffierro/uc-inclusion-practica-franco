@extends('layouts/main')

@php
  $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;
  $content = apply_filters('the_content', $post->post_content);
  $video_embed=isset($fields['video'])?$fields['video']:null;
  $parents = get_post_ancestors( $post->ID );

@endphp



@section('breadcrumbs')
  @include('parts.breadcrumbs',['post'=>$post])
@endsection


@section('content')
  <!--Content-->
  <main class="container" role="main" id="main-content">
    <div class="row">
      <div class="col-md-12 mb-32">
        <h1 class="uc-h1 pr-5 mb-0">{{$post->post_title}}</h1>
        @if($video)
          @include('parts/video_lsch', ['video' => $video])
        @endif
      </div>
      <div class="col-md-8 col-lg-9 mb-24">
        <div class="paragraph">
          {!! wpautop($post->post_content) !!}
        </div>
      </div>
    </div>

    <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
    @if(isset($fields['slider_contenidos']))
      <section class="mt-4 mt-lg-5 mb-80 mb-lg-8" aria-label="{{$fields['slider_titulo']}}">
        <div class="container">
          <div class="row mb-40">
            <div class="col-md-8 col-lg-9">
              <h2 class="uc-h2 title-decorated mb-32">{{$fields['slider_titulo']}}</h2>
              <div class="paragraph">
                <p>{!! $fields['slider_descripcion'] !!}</p>
              </div>
            </div>
          </div>
          @if(is_array($fields['slider_contenidos']))

            <div class="cards-carousel three-items-carousel" aria-label="Slider de actividades de apoyo académico">
              @foreach($fields['slider_contenidos'] as $key => $item)

                <div class="cards-carousel__item sameheight">
                  @include('parts/entry_card',['item'=>$item])

                </div>
              @endforeach
            </div>
          @endisset
        </div>
      </section>
    @endif


    {{--    @if($fields['items'])--}}
    {{--      <section class="mb-80 mb-lg-8" aria-label="Vías de admisión">--}}

    {{--        @foreach($fields['items'] as $key => $content)--}}
    {{--          @php--}}
    {{--            $image=SiteFunction::getImageDataFromAttachment($content['imagen']['ID'],[700,420]);--}}
    {{--            $title=isset($content['titulo'])?$content['titulo']:null;--}}
    {{--            $body=isset($content['descripcion'])?$content['descripcion']:null;--}}
    {{--            $video=isset($content['video_lsch'])?$content['video_lsch']:null;--}}
    {{--            $link=isset($content['link'])?$content['link']:null;--}}
    {{--          @endphp--}}
    {{--          <article class="uc-card card-type--horizontal align-items-start mb-32 p-32">--}}
    {{--            <div class="uc-card_img mb-32 mb-md-0 mr-md-2">--}}
    {{--              <img src="{{$image['src']}}" class="img-fluid" alt="{{$title}}">--}}
    {{--            </div>--}}
    {{--            <div class="uc-card_body p-0">--}}
    {{--              <h2 class="uc-h2 mb-16">{{$title}}</h2>--}}
    {{--              <div class="paragraph mb-16">--}}
    {{--                {!! wpautop($body) !!}--}}
    {{--              </div>--}}
    {{--              @if($video)--}}
    {{--                @include('parts/video_lsch', ['video' => $video])--}}

    {{--              @endif--}}
    {{--            </div>--}}
    {{--          </article>--}}
    {{--        @endforeach--}}
    {{--      </section>--}}
    {{--    @endif--}}
    <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">

    @isset($fields['items_titulo'])
      <section class="mt-4 mt-lg-5 mb-80 mb-lg-8" aria-label="{{$fields['items_titulo']}}">
        <div class="container">
          <div class="row mb-40">
            <div class="col-md-8 col-lg-9">
              <h2 class="uc-h2 title-decorated mb-32">{{$fields['items_titulo']}}</h2>
              <div class="paragraph">
                <p>{!! $fields['items_caja'] !!}</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    @endif


    @if($fields['items'])

      <div class="boxes-reverse mb-80 mb-lg-8">
        @foreach($fields['items'] as $key => $content)
          @php
            $image=SiteFunction::getImageDataFromAttachment($content['imagen']['ID'],[700,420]);
            $title=isset($content['titulo'])?$content['titulo']:null;
            $body=isset($content['descripcion'])?$content['descripcion']:null;
            $video=isset($content['video_lsch'])?$content['video_lsch']:null;
            $link=isset($content['link'])?$content['link']:null;
          @endphp
          <section class="uc-card card-type--horizontal card-type--horizontal--xl boxes-reverse__item"
                   aria-label="{{$title}}">
            @isset($image)
              <div class="uc-card_img">
                <img src="{{$image['src']}}" class="img-fluid"
                     alt="{{$image['alt']}}">
              </div>
            @endisset
            <div class="uc-card_body">
              <h2 class="uc-h2 mb-16">{{$title}}</h2>
              <div class="paragraph">
                {!! wpautop($body) !!}
              </div>
              @if($link)
                <div class="text-right mt-auto">
                  <a href=" {{$link['url']}}" title="{{$link['title']}}" class="uc-btn btn-inline">
                    {{$link['title']}}
                    <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                  </a>
                </div>
              @endif
              @if($video)
                @include('parts.video_lsch', ['video' => $video])

              @endif
            </div>
          </section>
        @endforeach
      </div>
    @endif

  </main>
  <!--Fin Content-->
@endsection
