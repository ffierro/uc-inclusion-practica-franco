@extends('layouts/main')
@section('breadcrumbs')
  @include('parts.breadcrumbs',['post'=>$post])
@endsection
@section('content')
  @php
    $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;
    $content = apply_filters('the_content', $post->post_content);
  @endphp
  <main role="main" id="main-content">
    <div class="container">
      <div class="row">
        <div class="col-md-12 mb-32">
          <h1 class="uc-h1 pr-5 mb-0">{{$post->post_title}}</h1>
          @if($video)
            @if($video)
              @include('parts/video_lsch', ['video' => $video])
            @endif
          @endif
        </div>
        <div class="col-md-8 col-lg-9 mb-24">
          <div class="paragraph">
            {!! wpautop($content) !!}
          </div>
        </div>
      </div>
      <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">

    @isset($fields['contenidos'])
      @foreach($fields['contenidos'] as $key => $content)
        @php $image=SiteFunction::getImageDataFromAttachment($content['imagen']['ID'],[768,430],true)@endphp
        @php $video=isset($content['video_lsch'])?$content['video_lsch']:null; @endphp
        @php $link=isset($content['titulo_link'])?$content['titulo_link']:null; @endphp

        <!--Actividades de inserción-->
          <section class="uc-card card-type--horizontal card-type--horizontal--xl mb-4 mb-lg-5">
            @isset($image)
              <div class="uc-card_img">
                <img src="{{$image['src']}}" class="img-fluid" alt="{{$content['imagen']['alt']}}">
              </div>
            @endisset
            <div class="uc-card_body">
              <h2 class="uc-h3 mb-16">{{$content['titulo']}}</h2>
              <div class="paragraph">
                <p>{!!  $content['descripcion']!!}</p>

              </div>
              @if($video)
                @include('parts/video_lsch', ['video' => $video])
              @endif
            </div>
          </section>
      @endforeach
      <!--Fin Actividades de inserción-->
    @endisset
{{--    <!--Actividades de inserción-->--}}
{{--      @isset($fields['contenidos'])--}}

{{--        <div class="cards-carousel three-items-carousel mb-4" aria-label="Slider de Apoyo a estudiantes">--}}
{{--          @foreach($fields['contenidos'] as $key => $content)--}}
{{--            @php $image=SiteFunction::getImageDataFromAttachment($content['imagen']['ID'],[768,430],true)@endphp--}}

{{--            <div class="cards-carousel__item sameheight">--}}
{{--              <section class="uc-card uc-card card-height--same">--}}
{{--                @isset($image)--}}
{{--                  <div class="uc-card_img">--}}
{{--                    <img src="{{$image['src']}}" class="img-card-max" alt="{{$content['imagen']['alt']}}">--}}
{{--                  </div>--}}
{{--                @endisset--}}

{{--                <div class="uc-card_body">--}}
{{--                  <div>--}}
{{--                    <h2 class="uc-h3 mb-12">{{$content['titulo']}}</h2>--}}
{{--                    <div class="uc-text-divider divider-primary"></div>--}}

{{--                    <div class="paragraph">--}}
{{--                      <p>{!!  $content['descripcion']!!}</p>--}}
{{--                    </div>--}}
{{--                  </div>--}}

{{--                  <div class="text-right mt-auto">--}}
{{--                    @php $link=isset($content['titulo_link'])?$content['titulo_link']:null; @endphp--}}
{{--                    @if($link)--}}
{{--                      <a href="{{$content['link']}}" title="{{$content['titulo']}}"--}}
{{--                         aria-label="Ver más {{$content['titulo']}}" target="_blank" class="uc-btn">--}}
{{--                        {{$content['titulo_link']}}--}}
{{--                        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>--}}
{{--                      </a>--}}
{{--                    @endif--}}
{{--                  </div>--}}

{{--                  @php $video=isset($content['video_lsch'])?$content['video_lsch']:null; @endphp--}}
{{--                  @if($video)--}}
{{--                    @include('parts/video_lsch', ['video' => $video])--}}
{{--                  @endif--}}
{{--                </div>--}}
{{--              </section>--}}
{{--            </div>--}}
{{--          @endforeach--}}
{{--        </div>--}}

{{--    @endisset--}}
{{--    <!--Fin Actividades de inserción-->--}}
    </div>

    <!--Acompañamiento socioafectivo-->
    <section class="bg-lightgray--xs py-4 py-lg-5" aria-label="{{$fields['titulo_acompanamiento_socioafectivo']}}">
      <div class="container">
        <div class="row mb-40">
          <div class="col-md-8 col-lg-9">
            <h2 class="uc-h2 title-decorated mb-32">{{$fields['titulo_acompanamiento_socioafectivo']}}</h2>
            <div class="paragraph">
              <p>{!! $fields['descripcion_acompanamiento_socioafectivo'] !!} </p>
            </div>
          </div>
        </div>
        @isset($fields['contenidos_acompanamiento_socioafectivo'])
          <div class="cards-carousel three-items-carousel"
               aria-label="Slider de {{$fields['titulo_acompanamiento_socioafectivo']}}">
            @foreach($fields['contenidos_acompanamiento_socioafectivo'] as $key => $content)
              @php $image=SiteFunction::getImageData($content->ID,[768,430],true)@endphp
              @php $content_fields=get_fields($content->ID)@endphp

              <div class="cards-carousel__item sameheight">
                @include('parts/entry_card',['item'=>$content])
              </div>
            @endforeach
          </div>
        @endisset
      </div>
    </section>
    <!--Fin Acompañamiento socioafectivo-->
    <!--Diagnósticos y nivelación-->
    @php
      $video=isset($fields['video_lsch_diagnostico_nivelacion'])?$fields['video_lsch_diagnostico_nivelacion']:null;
      $url=isset($fields['diagnosticos_y_nivelacion_enlace'])?$fields['diagnosticos_y_nivelacion_enlace']:null;
    @endphp
    <section class="bg-lightgray py-4 py-lg-5" aria-label="{{$fields['diagnosticos_y_nivelacion_titulo']}}">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-24">
            <h2 class="uc-h2 title-decorated mb-32">{{$fields['diagnosticos_y_nivelacion_titulo']}}</h2>
            <div class="paragraph wp-content">
              <p>{!!  $fields['diagnosticos_y_nivelacion_descripcion'] !!}</p>
            </div>
            @if($url)
              <a href="{{$url['url']}}" title="Más sobre Diagnóstico y nivelación" class="uc-btn btn-inline mt-32">
                {{$url['title']}}
                <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
              </a>
            @endif
          </div>
          <div class="col-md-6 d-flex align-items-start">
            @if($video)
              @include('parts/video_lsch_special_class', ['video' => $video])
            @endif
            @php $image=SiteFunction::getImageDataFromAttachment($fields['diagnosticos_y_nivelacion_imagen']['ID'],[980,840]) @endphp
            @isset($image)
              <img src="{{$image['src']}}" class="img-fluid"
                   alt="{{$fields['diagnosticos_y_nivelacion_imagen']['alt']}}">
            @endisset
          </div>
        </div>
      </div>
    </section>
    <!--Fin Diagnósticos y nivelación-->
    <!--Apoyos académicos-->
    <section class="mt-4 mt-lg-5 mb-80 mb-lg-8" aria-label="{{$fields['apoyos_academicos_titulo']}}">
      <div class="container">
        <div class="row mb-40">
          <div class="col-md-8 col-lg-9">
            <h2 class="uc-h2 title-decorated mb-32">{{$fields['apoyos_academicos_titulo']}}</h2>
            <div class="paragraph">
              <p>{!! $fields['apoyos_academicos_descripcion'] !!}</p>
            </div>
          </div>
        </div>
        @isset($fields['apoyos_academicos_contenidos'])

          <div class="cards-carousel three-items-carousel" aria-label="Slider de actividades de apoyo académico">
            @foreach($fields['apoyos_academicos_contenidos'] as $key => $item)

              <div class="cards-carousel__item sameheight">
                @include('parts/entry_card',['item'=>$item])

              </div>
            @endforeach
          </div>
        @endisset
      </div>
    </section>
    <!--Fin Apoyos académicos-->
  </main>
@endsection
