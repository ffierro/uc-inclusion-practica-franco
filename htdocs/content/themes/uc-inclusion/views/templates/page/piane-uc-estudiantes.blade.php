@extends('layouts/main')

@section('breadcrumbs')
    @include('parts.breadcrumbs',['post'=>$post])
@endsection



@section('content')
    @php
        $current_slug=get_permalink($post->ID);
        $parent_posts = get_posts([
            'post_type'      => 'page',
            'posts_per_page' => -1,
            'post_parent'    => 295,
            'order'          => 'ASC',
            'orderby'        => 'menu_order'

        ]);
        $content = apply_filters('the_content', $post->post_content);
        $video = isset($fields['video_lsch']) ? $fields['video_lsch'] : null;


    @endphp

    <main role="main" id="main-content">
        <div class="container mb-4 mb-lg-5">
            <div class="row">
                <div class="col-md-8 col-lg-9 mb-24">
                    <h1 class="uc-h1 mb-32">{{$post->post_title}}</h1>
                    @if($video)
                        @include('parts/video_lsch', ['video' => $video])

                    @endif
                </div>
                <div class="col-md-8 col-lg-9 mb-24">
                    <div class="paragraph">
                        {!! wpautop($content) !!}
                    </div>
                </div>
                <nav class="col-md-4 col-lg-3" role="navigation" aria-label="Submenú PIANE UC">
                    <div class="box">
                        <h4 class="uc-subtitle mb-16">PIANE UC:</h4>
                        @foreach($parent_posts as $key => $item)
                            @php
                                $active='';
                                $slug=get_permalink($item->ID);

                            if($slug==$current_slug){
                                $active='current';
                            }
                            @endphp
                            <a class="uc-btn text-weight--medium mb-16 {{$active}}" title="{{$item->post_title}}"
                               href="{{ get_permalink($item->ID)}}">{{$item->post_title}}</a>
                        @endforeach
                    </div>
                </nav>
            </div>
        </div>
    @php
        $image=SiteFunction::getImageDataFromAttachment($fields['imagen_caja']['ID'],[700,420]);
        $video=isset($fields['docencia_inclusiva_video'])?$fields['docencia_inclusiva_video']:null;

    @endphp
    @isset($image)

        <!--Actividades de inserción-->
            <section class="mb-4 mb-lg-5">
                <div class="bg-lightgray pt-4 pt-lg-5">
                    <div class="container">
                        <div class="uc-card card-type--horizontal card-type--horizontal--xl mb-40">
                            <div class="uc-card_img">
                                @isset($image)
                                    <img src="{{$image['src']}}" class="img-fluid"
                                         alt="{{$fields['imagen_caja']['alt']}}">
                                @endisset
                            </div>
                            <div class="uc-card_body">
                                <h2 class="uc-h2 mb-16">{{$fields['titulo_caja']}}</h2>
                                <div class="paragraph">
                                    <p>{{$fields['descripcion_caja']}}</p>
                                </div>
                                @if($video)
                                    @include('parts.video_lsch', ['video' => $video])

                                @endif
                            </div>
                        </div>
                        <div class="paragraph">
                            {!! $fields['descripcion_inferior_caja'] !!}
                        </div>
                    </div>
                </div>
                @isset($fields['contenido_multimedia'])
                    <div class="bg-lightgray--xs pt-4 pt-lg-5">
                        <div class="container">
                            <div class="cards-carousel three-items-carousel">
                                @foreach($fields['contenido_multimedia'] as $key => $item )
                                    <div class="cards-carousel__item sameheight">
                                        @include('parts/entry_card',['item'=>$item])
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endisset
            </section>
    @endisset

    <!--Fin Actividades de inserción-->
        <!--Apoyos académicos-->
        <section class="mb-80 mb-lg-8" aria-label="{{$fields['apoyos_academicos_titulo']}}">
            <div class="container">
                <div class="row mb-40">
                    <div class="col-md-8 col-lg-9">
                        <h2 class="uc-h2 title-decorated mb-32">{{$fields['apoyos_academicos_titulo']}}</h2>
                        <div class="paragraph">
                            {!! wpautop($fields['apoyos_academicos_descripcion']) !!}
                        </div>
                    </div>
                </div>
                @isset($fields['apoyos_academicos_contenidos'])
                    <div class="cards-carousel three-items-carousel">
                        @foreach($fields['apoyos_academicos_contenidos'] as $key => $item)
                            <div class="cards-carousel__item sameheight">
                                @include('parts/entry_card',['item'=>$item])

                            </div>
                        @endforeach

                    </div>
                @endisset
            </div>
        </section>
        <!--Fin Apoyos académicos-->
    </main>

@endsection
