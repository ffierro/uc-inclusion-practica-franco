@extends('layouts/main')

@section('content')
    @php
        $video=isset($fields['video_lsch'])?$fields['video_lsch']:null;
        $image_content = get_post(get_post_thumbnail_id($post));

    @endphp
@section('breadcrumbs')
    @include('parts.breadcrumbs',['post'=>$post])
@endsection

    <main class="container" role="main" id="main-content">
        <div class="row">
            <div class="col-md-7">
                <h1 class="uc-h1  pr-5 mb-32">{!! $post->post_title !!}</h1>
                @if($video)
                    @include('parts/video_lsch', ['video' => $video])
                @endif
                <div class="paragraph">
                    {!! wpautop($post->post_content) !!}

                </div>
            </div>
        </div>
        <div class="mb-4 mb-lg-5">
            <div class="row">
                @foreach($items as $key=>$item)
                    <div class="col-md-6 col-lg-3 mb-32">
                        @include('parts.entry_card',['item'=>$item,'key'=>$key])
                    </div>
                @endforeach
            </div>
        </div>
    </main>

@endsection