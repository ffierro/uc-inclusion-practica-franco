@extends('layouts/main')

<?php

$parent_posts = get_posts([
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => wp_get_post_parent_id($post->ID),
    'order'          => 'ASC',
    'orderby'        => 'menu_order'

]);

$post_parent = get_post(wp_get_post_parent_id($post->ID));
$video = isset($fields['video_lsch']) ? $fields['video_lsch'] : null;

?>


@section('breadcrumbs')
    @include('parts.breadcrumbs',['post'=>$post])
@endsection

@section('content')
    <main class="container" role="main" id="main-content">
        <div class="row">
            <div class="col-md-8 col-lg-9 mb-32">
                <h1 class="uc-h1 pr-5 mb-0">{{$post->post_title}}</h1>
                @if($video)
                    @include('parts/video_lsch', ['video' => $video])

                @endif
            </div>
            <div class="col-md-8 col-lg-9 mb-24">
                <div class="row">
                    @php $image=SiteFunction::getImageData($post->ID,[768,480],true)@endphp
                    @isset($image)
                        <div class="col-sm-6 col-lg-4 mb-24">
                            <img src="{{$image['src']}}" height="{{$image['height']}}" width="{{$image['width']}}"
                                 class="img-fluid" alt="{{$image['alt']}}">
                        </div>
                    @endisset
                    <div class="col-md-8">
                        <div class="paragraph">
                            {!! wpautop($post->post_content) !!}
                        </div>
                    </div>

                </div>
            </div>
            <nav class="col-md-4 col-lg-3" role="navigation" aria-label="Submenú PACE UC">
                <div class="box">
                    <h4 class="uc-subtitle mb-16">PACE UC:</h4>
                    @php
                        $current_slug=get_permalink($post->ID);
                    @endphp

                    @foreach($parent_posts as $key => $item)
                        @php
                            $active='';
                            $slug=get_permalink($item->ID);

                        if($slug==$current_slug){
                            $active='current';
                        }
                        @endphp
                        <a class="uc-btn text-weight--medium mb-16 {{$active}}"
                           href="{{ get_permalink($item->ID)}}" title="{{$item->post_title}}">{{$item->post_title}}</a>
                    @endforeach
                </div>
            </nav>
        </div>
        <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
    @isset($fields['cajas'])
        <!--Boxes-->
            <div class="boxes-reverse mb-4 mb-lg-5">
                @foreach($fields['cajas'] as $key => $box)
                    @include('parts/boxes_reverse', ['item' => $box,'image_size'=>[1050,1050]])
                @endforeach

            </div>
            <!--Fin Boxes-->
    @endisset
    <!--Tutores
        <section class="py-4 py-lg-5 mb-4 mb-lg-5">
            <div class="row mb-60">
                <div class="col-md-8 col-lg-9">
                    <h2 class="uc-h2 title-decorated mb-40">Tutores</h2>
                    <div class="paragraph">
                        @isset($fields['tutores_descripcion'])
        <p>{{$fields['tutores_descripcion']}}</p>
                        @endisset
            </div>
        </div>
    </div>
@isset($fields['tutores'])
        <div class="row">
@foreach($fields['tutores'] as $key => $tutor)
            <div class="col-md-4 col-lg-3">
@include('parts/card_tutor', ['item' => $tutor])
                    </div>
@endforeach
                </div>
            @endisset
            </section>
            Fin Tutores-->
        <!--Tutores-->
        <section class="mb-80 mb-lg-8" aria-label="Tutores">
            <div class="row mb-40">
                <div class="col-md-12">
                    <div class="d-sm-flex justify-content-between align-items-center mb-32">
                        @isset($fields['titulo_tutores'])
                            <h2 class="uc-h2 title-decorated">{{$fields['titulo_tutores']}}</h2>
                        @endisset
                        @if(key_exists('enlace_tutores',$fields) && is_array($fields['enlace_tutores']) && key_exists('url',$fields['enlace_tutores']))
                            <a href="{{$fields['enlace_tutores']['url']}}"
                               title="{{$fields['enlace_tutores']['title']}}"
                               class="uc-btn btn-inline d-none d-sm-block">{{$fields['enlace_tutores']['title']}} <i
                                        class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i></a>
                        @endisset
                    </div>
                </div>
                <div class="col-md-8 col-lg-9">
                    <div class="paragraph">
                        @isset($fields['tutores_descripcion'])
                            {!! wpautop($fields['tutores_descripcion']) !!}
                        @endisset
                    </div>
                </div>
            </div>
            @isset($fields['equipo'])
                <div class="img-carousel five-items-carousel">
                    @foreach($fields['equipo'] as $key => $tutor)
                        <div class="cards-carousel__item sameheight">
                            @include('parts/entry_card_team_single',['item'=>$tutor])
                        </div>
                    @endforeach
                </div>
            @endisset

        </section>
        <!--Fin Tutores-->
    @php

        $news = get_posts([
              'post_type'      => 'noticias',
              'posts_per_page' => 3,
              'tax_query' => [
                  ['taxonomy' => 'categorias_noticias',
                   'field' => 'slug',
                    'terms' => 'educacion-superior'
                    ]
                 ]

          ]);
    @endphp
    @if($news)
        <!--Sigue explorando-->
            <section class="container mb-80 mb-lg-8" role="complementary">
                @include('parts/news_card_3_items',
            ['data' =>
            [
                'title'=>"Noticias",
                'link_title'=>"Ver todas las noticias Educación Superior",
                'link_title_url'=>"/noticias?filter=educacion-superior",
            ]
            ,'items'=>$news, 'section'=>'news-home'])
            </section>
            <!--Fin Sigue explorando-->
        @endif
    </main>

@endsection
