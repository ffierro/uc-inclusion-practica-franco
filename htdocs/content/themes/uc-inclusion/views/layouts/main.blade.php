@php
    $top_menu_items = SiteFunction::getItemsMenu('Top Menú');
    $menu_items = SiteFunction::getItemsMenu('Menú Superior');
    $global_options = SiteFunction::getGlobalsOptions();
    $header_title=isset($global_options['theme_config']['titulo_cabecera'])?$global_options['theme_config']['titulo_cabecera']:'';
    $header_sub_title=isset($global_options['theme_config']['bajada_cabecera'])?$global_options['theme_config']['bajada_cabecera']:'';

@endphp
        <!doctype html>
<html {!! get_language_attributes() !!}>
<head>
    <meta charset="{{ get_bloginfo('charset') }}">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://kit-digital-uc-prod.s3.amazonaws.com/uc-kitdigital/css/uc-kitdigital.css">
    <link rel="icon" type="image/png" href="{{get_theme_file_uri()}}/dist/images/theme.png">
    <script src="https://kit.fontawesome.com/c57dddd08c.js" crossorigin="anonymous"></script>
    @head
@if(isset($global_options['theme_config']['codigo_analytics']))

    <!-- Google Analytics -->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', "{{$global_options['theme_config']['codigo_analytics']}}", 'auto');
            ga('send', 'pageview');
        </script>
        <!-- End Google Analytics -->
@endif

@if(isset($global_options['theme_config']['codigo_gtm']))

    <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', '{{$global_options['theme_config']['codigo_gtm']}}');</script>
        <!-- End Google Tag Manager -->
    @endif

</head>
<body @php(body_class())>
@if(isset($global_options['theme_config']['codigo_gtm']))

    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id={{$global_options['theme_config']['codigo_gtm']}}"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
@endif
<div id="page" class="site">
    <header class="uc-header" id="masthead" role="banner">
        <div class="uc-top-bar">
            <div class="container">
                <div class="top-bar_mobile-logo d-block d-lg-none">
                    <a href="" title="UC">
                        <img src="https://kit-digital-uc-prod.s3.amazonaws.com/assets/logo-uc-comprimido.svg"
                             role="image" alt="Logo UC - Ir al Home" class="img-fluid">
                    </a>
                </div>
                <nav class="top-bar_links justify-content-between d-none d-lg-flex" role="navigation"
                     aria-label="Menú secundario UC">
                    @isset($top_menu_items[0])
                        <div class="top-bar_links">
                            <a href="{{$top_menu_items[0]->url}}" target="_blank" class="text-size--sm mr-12"
                               title="{{$top_menu_items[0]->post_title}}">
                                {{$top_menu_items[0]->post_title}} <i class="uc-icon external"
                                                                      aria-hidden="true">launch</i>
                                <span class="hide">(El enlace se abre en una nueva pestaña)</span>
                            </a>
                        </div>
                    @endif
                    @isset($top_menu_items[1])
                        <div class="top-bar_links">
                            @foreach($top_menu_items as $key=>$item)
                                @if($key==0) @continue; @endif
                                <a href="{{$top_menu_items[0]->url}}" target="_blank"
                                   class="text-size--sm  ml-12" title="{{$item->post_title}}">
                                    <span class="hide">(El enlace se abre en una nueva pestaña)</span>
                                    {{$item->post_title}} <i class="uc-icon external" aria-hidden="true">launch</i>
                                </a>
                            @endforeach
                        </div>
                    @endif
                </nav>
            </div>
        </div>
        <div class="uc-navbar">
            <!-- Menú para versión Escritorio -->
            <div class="container d-none d-lg-block">
                <div class="d-flex align-items-center justify-content-between mb-2">
                    <a href="/" title="UC Inclusión" class="d-flex align-items-center">
                        @if(is_home())
                            <h1 class="logo h2 d-flex align-items-center m-0">
                                <img src="https://kit-digital-uc-prod.s3.amazonaws.com/assets/logo-uc-color.svg"
                                     role="image" alt="Logo UC - Ir al Home" class="logo__img img-fluid">
                                <div class="ml-2">
                                    <span class="text-font--serif text-color--blue logo__main">Inclusión</span>
                                    <span class="text-color--gray p-size--sm logo__text">Buscamos ampliar oportunidades de acceso y aprendizaje.</span>
                                </div>
                            </h1>
                        @else
                            <h2 class="logo h2 d-flex align-items-center m-0">
                                <img src="https://kit-digital-uc-prod.s3.amazonaws.com/assets/logo-uc-color.svg"
                                     role="image" alt="Logo UC - Ir al Home" class="logo__img img-fluid">
                                <div class="ml-2">
                                    <span class="text-font--serif text-color--blue logo__main">{{$header_title}}</span>
                                    <span class="text-color--gray p-size--sm logo__text">{{$header_sub_title}}</span>
                                </div>
                            </h2>
                        @endif
                    </a>
                    <a href="/contacto" title="Contacto">Contacto <i class="uc-icon" aria-hidden="true">email</i></a>
                </div>
                @isset($menu_items)
                    <nav class="uc-navbar_nav" role="navigation" aria-label="Menú principal">
                        @foreach($menu_items as $key=>$item)
                            <div class="nav-item">
                                <a href="{{$item->url}}" class="uc-btn btn-inline"
                                   title="{{$item->title}}">{{$item->title}}</a>
                            </div>
                        @endforeach
                    </nav>
                @endisset
            </div>
            <!-- Menú para versión Móvil -->
            <div class="uc-navbar_mobile d-block d-lg-none">
                <div class="uc-navbar_mobile-bar navbar-brand">
                    <div class="uc-navbar_mobile-logo navbar-light">
                        @if(is_home())
                            <h1 class="h2 text-font--serif text-color--blue m-0">Inclusión</h1>
                        @else
                            <h2 class="h2 text-font--serif text-color--blue m-0">Inclusión</h2>
                        @endif
                    </div>
                    <button title="" class="uc-navbar_mobile-button" aria-controls="menu-mobile" aria-expansion="false"
                            data-collapse="collapseMobileNav" aria-label="Abrir menú principal" id="menu">
                        <span class="uc-icon"></span>
                        Menú
                    </button>
                </div>
                <div aria-labelledby="menu" aria-hidden="true" id="menu-mobile" class="uc-navbar_mobile-content"
                     data-toggle="collapseMobileNav" data-open="false"
                     style="height: 0;">
                    @isset($menu_items)

                        <nav class="uc-navbar_mobile-list" role="navigation" aria-label="Menú principal">
                            @foreach($menu_items as $key=>$item)
                            <a href="{{$item->url}}" title="{{$item->title}}" class="list-item">{{$item->title}}</a>
                            @endforeach
                        </nav>
                    @endif
                </div>
            </div>
        </div>
    </header><!-- #masthead -->


    <div id="content" class="site-content">
        @yield('breadcrumbs')
        @yield('content')
    </div><!-- #content -->


    <footer class="uc-footer" role="contentinfo">
        <div class="container pb-md-5">
            <div class="row no-gutters justify-content-between align-items-center" data-accordion>
                <div class="logo col-md mb-24 mb-lg-0 d-flex align-items-end">
                    <a href="/" class="logo__img mr-20 mr-md-40 nabvar-brand" title="Logo UC">
                        <img src="{{get_theme_file_uri()}}/assets/images/logo-blanco.svg" role="image" alt=""
                             class="img-fluid">
                    </a>
                    <div class="ml-md-80">
                        <h4 class="h2 text-font--serif text-color--white mb-1">Inclusión</h4>
                        @isset($global_options['theme_config']['rrss'])
                            <ul class="d-flex">
                                @foreach($global_options['theme_config']['rrss'] as $rss)

                                    <li><a href="{{$rss['url']}}" title="{{$rss['red_social']}}"><i
                                                    class="uc-icon mr-1 fab {{$rss['red_social']}}"></i></a>
                                    </li>
                                @endforeach
                            </ul>
                        @endisset
                    </div>
                </div>
                <div class="col-md-4 col-lg-3 text-md-right">
                    @isset($global_options['theme_config']['direccion'])
                        <p class="mb-1">{{$global_options['theme_config']['direccion']}}</p>
                    @endisset
                    @if(isset($global_options['theme_config']['como_llegar']) && isset($global_options['theme_config']['como_llegar']['url']))
                        <a class="mb-1" href="{{$global_options['theme_config']['como_llegar']['url']}}"
                           title="{{$global_options['theme_config']['como_llegar']['title']}}">{{$global_options['theme_config']['como_llegar']['title']}}</a>
                    @endif
                    @isset($global_options['theme_config']['telefono'])
                        <p class="mb-0"><i class="uc-icon">phone</i>
                            <a href="#" title=">{{$global_options['theme_config']['telefono']}}">
                                {{$global_options['theme_config']['telefono']}}
                            </a>
                            @endisset
                        </p>
                </div>
            </div>
        </div>
        <div class="uc-footer_help">
            <div class="container">
                <div class="row">
                    @isset($global_options['theme_config']['cajas'])
                        @foreach($global_options['theme_config']['cajas'] as $item)
                            <div class="col-md-4 col-lg-3">
                                <h4 class="uc-footer_list-title mb-16">
                                    {{$item['titulo']}}
                                </h4>
                                <p>  {{$item['descripcion']}}</p>
                                @isset($item['enlaces'])
                                    @foreach($item['enlaces'] as $link)
                                        @if($link['tipo']=='url')
                                            <p><a href="{{$link['url']}}"
                                                  title="{{$link['titulo_link']}}">{{$link['titulo_link']}}</a></p>

                                        @else
                                            <p><i class="uc-icon">phone</i> <a
                                                        href="tel:{{$link['url']}}"
                                                        title="{{$link['titulo_link']}}">{{$link['titulo_link']}}</a>
                                            </p>

                                        @endif
                                    @endforeach
                                @endisset
                            </div>
                        @endforeach
                    @endisset
                </div>
            </div>
        </div>
    </footer><!-- #footer -->
</div><!-- #page -->

@footer

</body>
<script src="https://kit-digital-uc-prod.s3.amazonaws.com/uc-kitdigital/js/uc-kitdigital.js"></script>
@yield('scripts')
</html>
