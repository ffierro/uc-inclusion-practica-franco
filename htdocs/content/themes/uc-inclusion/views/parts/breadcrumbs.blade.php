<?php
$parents = get_post_ancestors($post->ID);

?>
<!--Breadcrumb-->
<ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
    <li class="uc-breadcrumb_item">
        <a href="/" title="Portada">Portada</a>
        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
    </li>
    @if(is_array($parents))
        @foreach($parents as $item)
            @php
                $content=get_post($item)
            @endphp
            <li class="uc-breadcrumb_item">
                <a href="{{get_post_permalink($content->ID)}}" title="{{$content->post_title}}">{{$content->post_title}}</a>
            </li>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        @endforeach
    @endif
    <li class="uc-breadcrumb_item current">
        {{$post->post_title}}
    </li>
</ol>
<!--Fin Breadcrumb-->