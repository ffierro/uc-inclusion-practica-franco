<div class="lsch mb-24 mr-32 ml-16">
    <button tabindex="0" type="button" class="lsch__button"
            data-popover-content="#lsch__popover"
            data-toggle="popover" data-placement="bottom">
    </button>
    <div id="lsch__popover" style="display:none;">
        <div class="popover-body">
            <video width="150" height="200" controls>
                <source src="{{$video['url']}}"
                        type="video/mp4">
            </video>
        </div>
    </div>
</div>