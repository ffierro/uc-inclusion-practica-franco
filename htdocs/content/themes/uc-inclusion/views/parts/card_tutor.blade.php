<?php
$image=SiteFunction::getImageDataFromAttachment($item['imagen']['ID'],[700,420]);
$video=isset($item['video_lsch'])?$item['video_lsch']:null;
?>
<article class="uc-card card-height--same">
    @isset($image)
        <img src="{{$image['src']}}" alt="{{$item['imagen']['alt']}}" class="img-fluid">
    @endisset
    <div class="uc-card_body">
        <h3 class="uc-h4">{{$item['nombre']}}<span
                    class="uc-card-subtitle">{{$item['bajada']}}</span>
        </h3>
        <div class="uc-text-divider divider-primary my-12"></div>
        <p class="no-margin">{{$item['descripcion']}}</p>
        <a href=" {{$item['email']}}" class="uc-btn btn-inline mt-24 mb-24">
            {{$item['email']}}
        </a>
    </div>
    <div class="text-right mt-auto">
        <a href="#" title="" class="uc-btn btn-inline">
            Ver más
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </a>
    </div>
        @if($video)
            @include('parts.video_lsch', ['video' => $video])
        @endif
</article>