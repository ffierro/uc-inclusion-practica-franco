<?php

$image = SiteFunction::getImageData($item->ID, [768, 430], true);
$content_fields = get_fields($item->ID);
if (!isset($content_fields['descarga_item'])) {
  return;
}
$related_content = $content_fields['descarga_item'];
if (!isset($related_content[0])) {
  $title = '';
} else {
  $title = $related_content[0]->post_title;
}
$title = isset($content_fields['descarga_sobrescribir_titulo']) ? $content_fields['titulo_enlace_cards'] : $title;
$video = isset($content_fields['video_lsch']) ? $content_fields['video_lsch'] : null;
$related_content_fields = isset($content_fields['descarga_item'][0]) ? get_fields($content_fields['descarga_item'][0]->ID) : [];
$related_content_type = '';
$related_content_url = '';
$url = '';
$content = isset($content_fields['descripcion_externa']) ? $content_fields['descripcion_externa'] : wp_trim_words($item->post_content, 50);

if (isset($related_content_fields['es_descarga_interna']) && !empty($related_content_fields['es_descarga_interna'])) {
  $related_content_type = $related_content_fields['es_descarga_interna'] ? 1 : null;
} else {
  $related_content_type = 0;

}
if ($related_content_type && isset($related_content_fields['archivo'])) {
  $url = $related_content_fields['archivo']['url'];
} elseif ($related_content_type == 0 && key_exists('url', $related_content_fields)) {
  $url = $related_content_fields['url'];

}
?>
<article class="uc-card card-height--same">
  @isset($image)
    <img src="{{$image['src']}}" @if($image['alt']) alt="{{$image['alt']}}" @endif class="img-fluid">
  @endisset
  <div class="uc-card_body">
    <h3 class="uc-h4" id="{{$section}}-{{$key}}">{{$item->post_title}}</h3>
    <div class="uc-text-divider divider-primary my-12"></div>
    {!!  wpautop($content) !!}
    <div class="text-right mt-auto">
      <a href="{{$url}}" target="_blank" title="{{$title}}" class="uc-btn"
         aria-labelledby="{{$section}}-{{$key}}">
        {{$title}}
        <i class="uc-icon" aria-hidden="true">save_alt</i>
      </a>
    </div>
    @if($video)
      @include('parts.video_lsch', ['video' => $video])
    @endif
  </div>
</article>
