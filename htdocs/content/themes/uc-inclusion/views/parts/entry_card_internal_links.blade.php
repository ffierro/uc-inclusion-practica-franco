<?php
$image = SiteFunction::getImageData($item->ID, [768, 430], true);
$content_fields = get_fields($item->ID);
$title = $content_fields['titulo_enlace_cards'];
$url = get_permalink($item->ID);
$video = isset($content_fields['video_lsch']) ? $content_fields['video_lsch'] : null;

$content = isset($content_fields['descripcion_externa']) ? $content_fields['descripcion_externa'] : wp_trim_words($item->post_content, 50);

?>


<article class="uc-card card-height--same">
  @if($video)
    @include('parts/video_lsch', ['video' => $video])

  @endif
  @isset($image)

    <img src="{{$image['src']}}" @if($image['alt']) alt="{{$image['alt']}}" @endif class="img-fluid">
  @endisset

  <div class="uc-card_body">
    <h3 class="uc-h4" id="{{$section}}-{{$key}}">{{$item->post_title}}</h3>
    <div class="uc-text-divider divider-primary my-12"></div>
    @if($content)
      {!!  wpautop($content) !!}
    @endif
    <div class="text-right mt-auto">
      <a href="{{$url}}" title="{{$title}}" class="uc-btn"
         aria-labelledby="{{$section}}-{{$key}}">
        {{$title}}
        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
      </a>
    </div>
  </div>
</article>
