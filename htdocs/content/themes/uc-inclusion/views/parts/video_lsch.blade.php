<?php
$unique = uniqid();

?>
<div class="lsch lsch--hover">
	<button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button"
			data-popover-content="#lsch__popover-{{$unique}}" data-toggle="popover"
			data-placement="bottom">
	</button>
	<div id="lsch__popover-{{$unique}}" style="display:none;">
		<div class="popover-body">
			@isset($video['url'])
				<video width="150" height="200" controls>
					<source src="{{$video['url']}}"
							type="video/mp4">
				</video>
			@endisset
		</div>
	</div>
</div>