@php
    $content_fields=get_fields($item->ID);
    $section=isset($section)?$section:'no-section';
@endphp

@isset($content_fields['tipo_entrada'])
  <!--Tipo de entrada: <?php echo $content_fields['tipo_entrada']?>-->
    @if($content_fields['tipo_entrada']=='descargable')
        @include('parts.entry_card_download',['item'=>$item,'fields'=>$content_fields])
    @elseif($content_fields['tipo_entrada']=='link')
        @include('parts.entry_card_links',['item'=>$item,'fields'=>$content_fields])
    @elseif($content_fields['tipo_entrada']=='link_interno')
        @include('parts.entry_card_internal_links',['item'=>$item,'fields'=>$content_fields])
    @elseif($content_fields['tipo_entrada']=='video')
        @include('parts.entry_card_video',['item'=>$item,'fields'=>$content_fields])
    @elseif($content_fields['tipo_entrada']=='equipo')
        @include('parts.entry_card_team',['item'=>$item,'fields'=>$content_fields])
    @endif
@endisset

