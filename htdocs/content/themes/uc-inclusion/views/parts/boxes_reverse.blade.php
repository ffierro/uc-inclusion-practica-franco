@php

    $image=[];
        if(isset($image_size)){
            $image=SiteFunction::getImageDataFromAttachment($item['imagen']['ID'],$image_size,true);
        }else{
            $image=SiteFunction::getImageDataFromAttachment($item['imagen']['ID'],[560,430],true);
        }
      //      $image=SiteFunction::getImageDataFromAttachment($item['imagen']['ID'],[560,430],true);
            $video=isset($item['video_lsch'])?$item['video_lsch']:null;
@endphp

<article class="uc-card card-type--horizontal card-type--horizontal--xl boxes-reverse__item">
    @isset($image['src'])
        <div class="uc-card_img">
            <img src="{{$image['src']}}" class="img-fluid" alt="{{$item['imagen']['alt']}}">
        </div>
    @endisset
    <div class="uc-card_body">
        <h2 class="uc-h2 mb-16">{{$item['titulo']}}</h2>
        <div class="paragraph">
            <p>{{$item['descripcion']}}</p>
        </div>
        <div class="text-right mt-auto">
            <a href="{{$item['url']}}" title="{{$item['titulo_enlace']}}" class="uc-btn btn-inline">
                {{$item['titulo_enlace']}}
                <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
            </a>
        </div>
        @if($video)
            @include('parts.video_lsch', ['video' => $video])
        @endif
    </div>
</article>