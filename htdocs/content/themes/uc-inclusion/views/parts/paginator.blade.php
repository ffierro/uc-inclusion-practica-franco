@if($pagination_post_count > $pagination_post_per_page)
    <div class="col-12">
        <!--Paginador-->
        <ol class="paginator mt-1">
            @if($pagination_paged > 1)
                <li class="paginator__arrow">
                    <a href="?pag={{$pagination_paged-1}}">
                        <i class="uc-icon" aria-hidden="true">keyboard_arrow_left</i>
                    </a>
                </li>
            @else
                <li class="paginator__item first"><a href="#"></a></li>
            @endif
            @for($p = 1; $p <= $pagination_num_pages; $p++)
                @if($pagination_paged == $p)
                    <li class="paginator__item paginator__item--active"><a href="#">{{$p}}</a></li>
                @else
                    <li class="paginator__item"><a href="?pag={{$p}}">{{$p}}</a></li>
                @endif
            @endfor

            @if($pagination_paged < $pagination_num_pages)
                <li class="paginator__arrow">
                    <a href="?pag={{$pagination_paged+1}}">
                        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                    </a>
                </li>
            @else
                <li class="paginator__item"><a href="#"></a></li>

            @endif
        </ol>
        <!--Fin Paginador-->
    </div>
@endif