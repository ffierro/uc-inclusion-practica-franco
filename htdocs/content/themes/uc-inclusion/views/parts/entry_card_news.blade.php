@php
    $image=SiteFunction::getImageData($item->ID,[300,190],true);
    $taxonomies=SiteFunction::getPostTermsFilterByTaxonomies($item->ID,'categorias_noticias');
    $section=isset($section)?$section:'no-section';
    $video_content= get_field('video_lsch',$item->ID);
    $video=isset($video_content)?$video_content:null;

@endphp

<article class="uc-card card-height--same">
    @isset($image)
        <img src="{{$image['src']}}" class="img-fluid" alt="{{$image['alt']}}">
    @endisset
    @if(is_array($taxonomies))
{{--        <div class="tags tag-fixed">--}}
{{--            @foreach($taxonomies as $taxonomy)--}}
{{--                <a href="/noticias?filter={{$taxonomy->slug}}" title="{{$taxonomy->name}} - Categoría de artículo"--}}
{{--                aria-label="{{$taxonomy->name}} - Categoría de artículo"--}}
{{--                class="uc-tag">{{$taxonomy->name}}</a>--}}
{{--            @endforeach--}}
{{--        </div>--}}
    @endif
    <div class="uc-card_body">
        <p class="small gray condensed">{{get_the_date('d F Y', $item->ID)}}</p>
        <h3 class="uc-h4 mb-24" id="{{$section}}-{{$key}}">{{$item->post_title}}</h3>
        <div class="text-right mt-auto">
            <a href="{{ get_permalink($item->ID)}}" title="Ver {{$item->post_title}}" class="uc-btn"
               aria-labelledby="{{$section}}-{{$key}}">
                Ver más
                <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
            </a>
        </div>
        @if($video)
            @include('parts.video_lsch', ['video' => $video])
        @endif
    </div>
</article>
