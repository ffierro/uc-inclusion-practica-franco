@php
    $fields=get_fields($item->ID);
    $show_single= get_field('mostrar_detalle',$item->ID);
    $video_content= get_field('video_lsch',$item->ID);
    $video=isset($video_content)?$video_content:null;
@endphp

<article class="uc-card card-height--same sameheight">
    @php $image=SiteFunction::getImageData($item->ID,[330,350],false)@endphp
    @php
        $imageData=SiteFunction::getPostImageByName($item->ID,'equipo-card');

    @endphp
    @if($image)

        <img src="{{$imageData['src']}}" class="img-fluid p-20 pb-0" alt="{{$imageData['alt']}}">
    @endif
    <div class="uc-card_body">
        <h3 class="uc-h4">{{$item->post_title}}
            @if(isset($fields['bajada']))
                <span class="uc-card-subtitle">{{$fields['bajada']}}</span>
            @endif
        </h3>
        <div class="uc-text-divider divider-primary my-12"></div>
        {{$item->post_content}}
        <a href="{{get_permalink($item->ID)}}" title="profesor@uc.cl" class="uc-btn btn-inline mt-24">
            {{get_field('email',$item->ID)}}
        </a>
        @if($show_single)
            <div class="text-right mt-auto">
                <a href="{{get_permalink($item->ID)}}" title="" class="uc-btn">
                    Ver más
                    <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                </a>
            </div>
        @endif
        @if($video)
            @include('parts.video_lsch', ['video' => $video])
        @endif
    </div>
</article>