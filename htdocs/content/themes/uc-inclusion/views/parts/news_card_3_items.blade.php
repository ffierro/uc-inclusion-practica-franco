@php $section=isset($section)?$section:'no-section'@endphp

<div class="d-sm-flex justify-content-between align-items-center mb-32">
	<h2 class="uc-h2 title-decorated">{{$data['title']}}</h2>
	@if($data['link_title_url'])
		<a href="{{$data['link_title_url']}}" title="{{$data['link_title']}}"
		   class="uc-btn btn-inline d-none d-sm-block">
			{{$data['link_title']}}
			<i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
		</a>
	@endif
</div>
<div class="grid row no-gutters">
	<div class="col-lg-7 grid__column">
		<?php
		$image = SiteFunction::getImageData($items[0]->ID, [768, 1024], true);
		$taxonomies = SiteFunction::getPostTermsFilterByTaxonomies($items[0]->ID, 'categorias_noticias');
		$bajada = get_field('bajada_noticia', $items[0]->ID);
		$video = get_field('video_lsch', $items[0]->ID);
		$video = isset($video) ? $video : null;
		?>
		<article class="uc-card card-bg--image card-gradient--bottom-blue"
				 @if($image)style="background-image:url('{{$image['src']}}');"@endif>
			@isset($taxonomies[0])
{{--				<div class="tags tag-fixed">--}}
{{--					@foreach($taxonomies as $taxonomy)--}}
{{--						<a href="/noticias/?filter={{$taxonomy->slug}}"--}}
{{--						   title="{{$taxonomy->name}} - Categoría de artículo"--}}
{{--						   aria-label="{{$taxonomy->name}} - Categoría de artículo" class="uc-tag">--}}
{{--							{{$taxonomy->name}}--}}
{{--						</a>--}}
{{--					@endforeach--}}
{{--				</div>--}}
			@endisset
			<div class="uc-card_body justify-content-end">
				<div class="relative">
					<a href="{{ get_permalink($news[0]->ID)}}" title="{{$news[0]->post_title}}">
						<h3>{{$news[0]->post_title}}</h3>
					</a>
					@if($bajada)
						<p class="ellipsis">{{$bajada}}</p>
					@endif
					@if($video)
						@include('parts/video_lsch', ['video' => $video])

					@endif
				</div>
			</div>
		</article>
	</div>
	@isset($items[1])
		<div class="col-lg-5 grid__column grid__column--items">
			@foreach($items as $key=>$item)
				@if ($key==0)@continue @endif
				@php $image=SiteFunction::getImageData($item->ID,[200,255],true)@endphp
				@php
					$taxonomies=SiteFunction::getPostTermsFilterByTaxonomies($item->ID,'categorias');
					$video=get_field('video_lsch',$item->ID);
					$video=isset($video)?$video:null;
				@endphp

				<article class="uc-card card-type--horizontal">
					@if($video)
						@include('parts/video_lsch', ['video' => $video])
					@endif
					@if($image)
						<img src="{{$image['src']}}" class="img-fluid" alt="{{$image['alt']}}">
					@endif
					<div class="uc-card_body">
						@if($taxonomies)
{{--							<div class="tags">--}}
{{--								@foreach($taxonomies as $taxonomy)--}}
{{--									<a href="/noticias/?filter={{$taxonomy->slug}}"--}}
{{--									   title="{{$taxonomy->name}} - Categoría de artículo"--}}
{{--									   aria-label="{{$taxonomy->name}} - Categoría de artículo" class="uc-tag">--}}
{{--										{{$taxonomy->name}}--}}
{{--									</a>--}}
{{--								@endforeach--}}

{{--							</div>--}}
						@endif
						<h3 class="uc-h4 mt-16" id="{{$section}}-{{$key}}">{{$item->post_title}}</h3>
						<div class="text-right mt-auto">
							<a href="{{ get_permalink($item->ID)}}" title="Ver {{$item->post_title}}"
							   class="uc-btn btn-inline" aria-labelledby="{{$section}}-{{$key}}">
								Ver más
								<i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
							</a>
						</div>
					</div>
				</article>
			@endforeach
		</div>
	@endisset
</div>
<div class="text-right d-sm-none">
	<a href="{{$data['link_title_url']}}" title="{{$data['link_title']}}" class="uc-btn btn-inline">
		{{$data['link_title']}}
		<i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
	</a>
</div>
