<div class="cards-carousel__item sameheight">
    <article class="uc-card card-height--same">
        <img src="http://via.placeholder.com/300x190" class="img-fluid">
        <div class="uc-card_body">
            <h3 class="uc-h4">{{$item['nombre']}}<span
                        class="uc-card-subtitle">{{$item['bajada']}}</span>
            </h3>
            <div class="uc-text-divider divider-primary my-12"></div>
            <p class="mb-24">{{$item['descripcion']}}</p>
            <a href="{{$item['email']}}" class="uc-btn btn-inline mt-auto">
                {{$item['email']}}
            </a>
        </div>
    </article>
</div>