<?php
$image = SiteFunction::getImageData($item->ID, [768, 430], true);
$content_fields=get_fields($item->ID);
$title=$content_fields['titulo_enlace_cards'];
$embed=isset($content_fields['video_embed'])?$content_fields['video_embed']:null;

?>
<article class="uc-card card-height--same sameheight">
    <div class="iframe-responsive">
        @if($embed)
            {!! $embed !!}
        @endif
    </div>
    <div class="uc-card_body">
        <h3 class="uc-h4">{{$title}}</h3>
    </div>
</article>
