<?php
$image = SiteFunction::getImageData($item->ID, [300, 190], true);
$content_fields = get_fields($item->ID);
$video_content = get_field('video_lsch', $item->ID);
$video = isset($video_content) ? $video_content : null;
$current_post = get_post();
$is_team_template = get_page_template_slug($current_post->id) === 'equipo';
?>

<div class="@if($is_team_template) col-sm-6 col-md-3 col-lg-3 col-xl mb-32 @else cards-carousel__item @endif">
  <article class="uc-card card-height--same">
    @isset($image)
      <img src="{{$image['src']}}" @if($image['alt']) alt="{{$image['alt']}}" @endif class="img-fluid p-20">
    @endisset
    <div class="uc-card_body">
      <h3 class="uc-h4">{!! $item->post_title !!}
        <span class="uc-card-subtitle">{!! $content_fields['cargo'] ?? ''!!}</span></h3>
      <div class="uc-text-divider divider-primary my-12"></div>
      {!! $item->post_excerpt !!}
      <a href="{{ get_permalink($item->ID)}}" title="Ver {{$item->post_title}}" class="uc-btn btn-inline">
        {!! $content_fields['correo'] ?? ''!!}
      </a>
      <div class="text-right mt-auto">
        <a href="#" title="" class="uc-btn">
          Ver más
          <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </a>
      </div>
      @if($video)
        @include('parts.video_lsch', ['video' => $video])
      @endif
    </div>
  </article>
</div>
