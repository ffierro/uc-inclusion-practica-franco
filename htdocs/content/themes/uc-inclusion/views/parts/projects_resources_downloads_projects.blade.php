@php
    $theme = app('wp.theme');
@endphp
<ul class="table-list" aria-label="Listado de documentos">
    @foreach($resources as $item)
        @php
            $fields_downloads=get_fields($item->ID);
            $type=$fields_downloads['es_descarga_interna']?1:null;

            $icon=isset($fields_downloads['icono'])?$fields_downloads['icono']:null;
            if($icon){
                $icon = $theme->getUrl('assets/images/'.$icon.'.svg');
            }else{
                   $icon = $theme->getUrl('assets/images/archive.svg');
            }
             $url='#';
            $url_download='#';
            $file_size='';


             if($type==1){
                 $file_size=$fields_downloads['es_descarga_interna']?SiteFunction::filesize_formatted($fields_downloads['archivo']['filesize']):null;
                 $url_download=$fields_downloads['es_descarga_interna']?$fields_downloads['archivo']['url']:$fields_downloads['url'];
             }else{
                 $url_download=$fields_downloads['url']?$fields_downloads['url']:'#';

             }
        @endphp
        <li class="table-list__item table-list__item--download">
            <a href="{{$url_download}}">
                <span class="hide">(El enlace descarga un documento)</span>
                @if($icon)
                    <img class="table-list__item__icon"
                         src="{{$icon}}"
                         alt="{{$item->post_title}}">
                @endif
                <h4 id="titulo-item-1">{{$item->post_title}}</h4>
                <div class="d-flex align-items-center">
                    @if($type==1 && $file_size)
                        <p>{{$file_size}}</p>
                    @endif
                    <i class="uc-icon icon-shape--rounded ml-1"
                    aria-hidden="true">arrow_downward</i>
                </div>
            </a>
        </li>
    @endforeach

</ul>