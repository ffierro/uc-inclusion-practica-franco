@php $image=SiteFunction::getImageData($item->ID,[300,190])@endphp
@php $content_fields=get_fields($item->ID)@endphp

<?php
$is_download = false;
if (isset($content_fields['es_descargable']) && $content_fields['es_descargable'] == 1) {
    $url = $content_fields['archivo']['url'];
    $is_download = true;

} else {
    if (isset($content_fields['enlace_cards'])) {
        $url = $content_fields['enlace_cards'];
    } else {
        $url = '#';
    }

}
?>

<div class="cards-carousel__item sameheight">
    <article class="uc-card card-height--same">
        @isset($image)
            <img src="{{$image['src']}}" alt="{{$image['alt']}}" class="img-fluid">
        @endisset
        <div class="uc-card_body">
            <h3 class="uc-h4">{{$item->post_title}}</h3>
            <div class="uc-text-divider divider-primary my-12"></div>
            <p class="mb-24">{!! $item->post_content !!}</p>
            <div class="text-right mt-auto">
                <a target="_blank" href="{{$url}}" class="uc-btn btn-inline">
                    {{$content_fields['titulo_enlace_cards']}}
                    @if($is_download)
                        <i class="uc-icon">save_alt</i>
                    @else
                        <i class="uc-icon">keyboard_arrow_right</i>

                    @endif
                </a>
            </div>
        </div>
    </article>
</div>