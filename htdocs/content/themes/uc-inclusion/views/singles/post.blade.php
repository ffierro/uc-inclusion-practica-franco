@extends('layouts/main')

@section('content')
	<!--Content-->
	@php
		$image=SiteFunction::getImageData($post->ID,[768,430],true);
		$video=isset($fields['video_lsch'])?$fields['video_lsch']:null;
		$breadcrumb=isset($fields['breadcrumb_link'])?$fields['breadcrumb_link']:null;
		$hiddeDate=isset($fields['ocultar_fecha'])?$fields['ocultar_fecha']:null;


	@endphp
	<div class="container offset-white">
		<div class="row justify-content-center">
			<div class="col-md-10">
				<!--Breadcrumb-->
				<ol class="uc-breadcrumb my-40" aria-label="Breadcrumb">
					<li class="uc-breadcrumb_item">
						<a href="/" title="Portada">Portada</a>
						<i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
					</li>
					@if($breadcrumb)
						<li class="uc-breadcrumb_item">
							<a href="{{$breadcrumb['url']}}"
							   title="{{$breadcrumb['title']}}">{{$breadcrumb['title']}}</a>
							<i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
						</li>
					@endif
					<li class="uc-breadcrumb_item current">
						{{$post->post_title}}
					</li>
				</ol>
				<!--Fin Breadcrumb-->
			</div>
		</div>
		<main role="main" id="main-content" class="row">
			<div class="col-md-10 offset-md-1">
				@if($video)
					@include('parts/video_lsch', ['video' => $video])

				@endif
				<h1 class="uc-h1 mb-40" id="articulo">{{$post->post_title}}</h1>
			</div>
			<div class="col-md-7 offset-md-1">
				<!--Main-->
				<article class="mb-4 mb-lg-5" role="contentinfo" aria-label="Artículo" aria-labelledby="Artículo">
					<div class="row align-items-center mb-40">
						@if(!$hiddeDate)
							<div class="col-md-auto paragraph mb-24">
								<p>{{get_the_date('d F Y', $post->ID)}}</p>
							</div>
						@endif
						<div class="col">
							<span class="uc-hr"></span>
						</div>
					</div>
					@if($image)
						<div class="">
							<img src="{{$image['src']}}" alt="{{$image['alt']}}"/>
						</div>
					@endif
					@isset($fields['bajada'])
						<div class="p-size--xl text-weight--bold">
							{!! wpautop($fields['bajada']) !!}

						</div>
					@endisset
					<span class="uc-text-divider divider-secondary my-32"></span>
					<div class="body-article">
						{!! wpautop($post->post_content) !!}
					</div>
				</article>
				<!--Fin Main-->
			</div>
		</main>
	</div>
	<!--Fin Content-->
@endsection