@extends('layouts/main')

@section('content')
@section('breadcrumbs')

	<?php
	$parent_posts = get_posts([
		'post_type'      => 'page',
		'posts_per_page' => -1,
		'post_parent'    => $post->post_parent,
		'order'          => 'ASC',
		'orderby'        => 'menu_order'

	]);


	?>
	<!--Breadcrumb-->
	<ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
		<li class="uc-breadcrumb_item">
			<a href="#" title="Portada">Portada</a>
			<i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
		</li>
		<li class="uc-breadcrumb_item">
			<a href="/equipo" title="Equipo">Equipo</a>
			<i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
		</li>
		<li class="uc-breadcrumb_item current">
			{{$post->post_title}}
		</li>
	</ol>
	<!--Fin Breadcrumb-->
@endsection
@php
	$video = isset($fields['video_lsch']) ? $fields['video_lsch'] : null;

@endphp
<div class="container">
	<main class="mb-4 mb-lg-5" role="main" id="main-content">
		<div class="row">
			<div class="col-md-8  mb-24">
				@if($video)
					@include('parts.video_lsch', ['video' => $video])

				@endif
				<h1 class="uc-h1"> {{$post->post_title}}</h1>
				@if($fields['bajada'])
					<h2 class="h4 uc-h4 mb-32">{{$fields['bajada']}}</h2>
				@endif
				@if($fields['funciones'])

					<h3>Sus funciones</h3>
					<div class="paragraph mb-32">
						{!! wpautop($fields['funciones']) !!}
					</div>
				@endif

				@if($fields['servicios'])

					<h3>Sus servicios</h3>
					<div class="paragraph">
						{!! wpautop($fields['servicios']) !!}

					</div>
				@endif

			</div>
			@php $image=SiteFunction::getImageData($post->ID,[768,920],true)@endphp
			@if($image)
				<div class="col-md-4">
					<img src="{{$image['src']}}" class="img-fluid" alt="{{$image['alt']}}">
				</div>
			@endif
		</div>
	</main>
	<hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">


	<!--Tutores-->
	@if($teams)
		<section class="mb-80 mb-lg-8" role="complementary" aria-label="Otros tutores">
			<h2 class="uc-h2 title-decorated mb-32">Otros Tutores</h2>
			<div class="row five-columns">

				@foreach($teams as $key => $item)
					<div class="col-sm-6 col-md-4 col-lg-3 col-xl mb-24">
						@include('parts/entry_card_team_single',['item'=>$item])
					</div>

				@endforeach
			</div>
		</section>
@endif
<!--Fin Tutores-->
</div>

@endsection
