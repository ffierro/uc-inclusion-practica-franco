@extends('layouts/main')


@section('content')
	<?php
	$video = isset($fields['video_lsch']) ? $fields['video_lsch'] : null;
	$categories = isset($fields['categoria_noticias']) ? $fields['categoria_noticias'] : null;
	$contact_data = isset($fields['contacto_titulo']) ? $fields['contacto_titulo'] : null;
	?>

	<!--Content-->
	@isset($fields['imagen_fondo']['url'])
		@php $image=SiteFunction::getImageDataFromAttachment($fields['imagen_fondo']['ID'],[1500,650],true)@endphp

		<div class="img-single">
			<img src="{{$image['src']}}" alt="{{$fields['imagen_fondo']['alt']}}">
		</div>
	@endisset
	<div class="container offset-white">
		<div class="row justify-content-center">
			<div class="col-md-10">

				<!--Breadcrumb-->
				<ol class="uc-breadcrumb my-40" aria-label="Breadcrumb">
					<li class="uc-breadcrumb_item">
						<a href="#" title="Portada">Portada</a>
						<i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
					</li>
					<li class="uc-breadcrumb_item">
						<a href="/noticias" title="Noticias">Noticias</a>
						<i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
					</li>
					<li class="uc-breadcrumb_item current">
						{{$post->post_title}}
					</li>
				</ol>
				<!--Fin Breadcrumb-->
			</div>
		</div>
		<main role="main" id="main-content" class="row justify-content-center">
			<div class="col-md-10">
				@if($video)
					@include('parts.video_lsch', ['video' => $video])
				@endif
{{--				@if($categories)--}}
{{--					@foreach($categories as $category)--}}
{{--						<a href="#" title="{{$category->name}}" class="uc-tag tag-fixed mb-16">{{$category->name}}</a>--}}
{{--					@endforeach--}}
{{--				@endif--}}
				<h1 class="uc-h1 mb-40" id="articulo">{{$post->post_title}}</h1>
			</div>
			<div class="col-md-7">
				<article class="mb-4 mb-lg-5" role="contentinfo" aria-label="Artículo" aria-labelledby="Artículo">
					<div class="row align-items-center mb-40">
						<div class="col-md-auto paragraph mb-24">
							<p> {{get_the_date('d F Y', $post->ID)}}</p>
						</div>
						<div class="col">
							<span class="uc-hr"></span>
						</div>
					</div>

					@isset($fields['bajada_noticia'])
						<div class="p-size--xl text-weight--bold">
							{!! wpautop($fields['bajada_noticia']) !!}

						</div>
					@endisset
					@if(is_array($fields['imagen_contenido']))

						<div class="row">
							<img src="{{$fields['imagen_contenido']['url']}}">
						</div>
					@endisset
					<span class="uc-text-divider divider-secondary my-32"></span>
					<div class="body-article">
						@php
							$content = apply_filters('the_content', $post->post_content);

						@endphp
						{!! wpautop($content) !!}
					</div>
				@if(array_key_exists('galeria',$fields) && $fields['galeria'])

					<!--Galería-->
						<section class="my-4 my-lg-5">
							<h2 class="uc-h3 title-decorated mb-16">Galería de imágenes</h2>
							<div class="gallery-carousel three-items-carousel mb-4 mb-lg-5">
								@foreach($fields['galeria'] as $item)
									@php
										$image=SiteFunction::getImageDataFromAttachment($item['ID'],[300,190],true)
									@endphp
									<div class="cards-carousel__item sameheight">
										<a class="uc-card" data-fancybox="gallery"
										   href="{{$item['url']}}" title="{{$item['alt']}}">
											<img src="{{$image['src']}}" class="img-fluid" alt="{{$item['alt']}}">
										</a>
									</div>
								@endforeach
							</div>
						</section>
						<!--Fin Galería-->
				@endif


				@if($contact_data)
					<!--Box contacto-->
						<section class="uc-card p-48 mb-4 mb-lg-5">
							<h4 class="h6 mb-24">{{$fields['contacto_titulo']}}</h4>
							<div class="paragraph">
								{!! wpautop($fields['contacto_descripcion']) !!}

							</div>
						</section>
						<!--Fin box contacto-->
				@endif

				@php
					$title=isset($fields['titulo_informacion_relacionada'])?$fields['titulo_informacion_relacionada']:null;
					$items=isset($fields['items_informacion_relacionada'])?$fields['items_informacion_relacionada']:null;
				@endphp
				<!--Info relacionada-->
					<section class="body-article mb-4 mb-lg-5">
						@if($title)<h2>{{$title}}</h2>@endif
						@if($items)
							<ul>
								@foreach($items as $key=>$item)

									<li>
										{!! wpautop($item['contenido']) !!}
										@if(isset($item['link']['url']) && isset($item['link']['title']))
											<a href="{{$item['link']['url']}}">{{$item['link']['title']}}</a>
										@endif
									</li>
								@endforeach

							</ul>
						@endif
					</section>
					<!--Fin Info relacionada-->
					<!--Share-->
					<span class="uc-hr mb-40"></span>
					<section class="row mb-40">
						@if(!array_key_exists('contadores',$fields) )
							@php  $counter=0 @endphp
						@else
							@php $counter=$fields['contadores'] @endphp
						@endif


						<div class="col-lg-6 mb-24">
							<div class="uc-subtitle mb-12">¿Te gusta esta publicación?</div>
							<div class="uc-like-container">
								<button class="uc-like-btn" data-id="{{$post->ID}}">
									<img src="{{$theme->getUrl()}}/assets/images/icon-like.svg"
										 alt="Me gusta">
								</button>
								<div class="h2 ml-16">{{$counter}}</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="uc-subtitle mb-12">Comparte esta publicación</div>
							@php $share_links=SiteFunction::generateSocialShareLink(get_permalink($post->ID),$post->post_title)@endphp
							<div>
								<a href="{{$share_links['facebook_url']}}" title="" target="_blank"><img
											src="{{$theme->getUrl()}}/assets/images/icon-facebook.svg"
											alt=""></a>
								<a href="{{$share_links['twitter_url']}}" title="" class="ml-24"><img
											src="{{$theme->getUrl()}}/assets/images/icon-twitter.svg"
											alt=""></a>
								<a href="mailto:?subject={{$post->post_title}}" title="" class="ml-24"><img
											src="{{$theme->getUrl()}}/assets/images/icon-mail.svg"
											alt=""></a>
							</div>
						</div>
					</section>
					<span class="uc-hr mb-4 mb-lg-5"></span>
					<!--Fin Share-->
				</article>
			</div>

			<div class="col-md-4 col-lg-3">
				@if(is_array($fields['etiquetas']) || is_array($fields['ejes_estrategicos']) || is_array($fields['unidad_academica']))
					<aside id="sidebar" role="complementary">
						<div class="uc-card">
							<div class="uc-card_body">
								<h3 class="h6 mb-12">Información</h3>
								<span class="uc-hr mb-24"></span>
								@if(array_key_exists('etiquetas',$fields) && $fields['etiquetas'])

									<div class="mb-24">
										<h4 class="uc-subtitle mb-16"><i
													class="uc-icon icon-gray icon-small">local_offer</i>
											Etiquetas</h4>
										<div class="uc-tags">
											@foreach($fields['etiquetas'] as $key=>$item)

												<a title="" href="#/" class="uc-tag">{{$item->name}}</a>
											@endforeach
										</div>
									</div>
								@endif
								@if(array_key_exists('ejes_estrategicos',$fields) && $fields['ejes_estrategicos'])

									<div class="mb-24">
										<h4 class="uc-subtitle mb-16"><i
													class="uc-icon icon-gray icon-small">local_offer</i>
											Eje estratégico</h4>
										<div class="uc-tags">
											@foreach($fields['ejes_estrategicos'] as $key=>$item)
												<a title="" href="#" class="uc-tag">{{$item->name}}</a>
											@endforeach
										</div>
									</div>
								@endif
								@if(array_key_exists('unidad_academica',$fields) && $fields['unidad_academica'])

									<div class="mb-24">
										<h4 class="uc-subtitle mb-16"><i
													class="uc-icon icon-gray icon-small">local_offer</i>
											Unidad académica</h4>
										<div class="uc-tags">
											@foreach($fields['unidad_academica'] as $key=>$item)

												<a title="" href="#" class="uc-tag">{{$item->name}}</a>
											@endforeach
										</div>
									</div>
								@endif

							</div>
						</div>
					</aside>
				@endif
			</div>
		</main>
	</div>
	<!--Fin Content-->
	<!--Sigue explorando-->
	<section class="container mb-80 mb-lg-8" role="complementary">
		<?php
		$taxonomies = SiteFunction::getPostTermsFilterByTaxonomies($post->ID, 'categorias_noticias');

		if ($taxonomies) {
			$categories = array_column($taxonomies, 'slug');

			$news = get_posts([
				'post_type'      => 'noticias',
				'post__not_in'   => array($post->ID),
				'posts_per_page' => 3,
				'tax_query'      => array(
					'taxonomy' => 'categorias',
					'field'    => 'slug',
					'terms'    => $categories,
					'operator' => 'IN'
				)

			]);
		} else {
			$news = get_posts([
				'post_type'      => 'noticias',
				'post__not_in'   => array($post->ID),
				'posts_per_page' => 3,

			]);
		}
		?>
		@include('parts/news_card_3_items',
	['data' =>
	[
		'title'=>"Sigue Explorando",
		'link_title'=>"",
		'link_title_url'=>"",
	]
	,'items'=>$news, 'section'=>'news-home'])
	</section>
	<!--Fin Sigue explorando-->
@endsection

@section('scripts')

	<script>
        $('body').on('click', '.uc-like-btn', function (e) {
            e.preventDefault();
            var value = $(this).data('id');
            if (!value) {
                return;
            }


            $.ajax({
                url: themosis.ajaxurl,
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'likeNew',
                    security: themosis.nonce, // A nonce value defined by the user with the "add-posts" action
                    post_id: value
                },
                beforeSend: function (xhr, opts) {


                },
            }).done(function (data) {
                console.log(data);
                if (data.status == 'ok') {

                    $('.uc-like-container .h2.ml-16').html(data.counter);

                }
            });
            return;
        });

	</script>

@endsection
