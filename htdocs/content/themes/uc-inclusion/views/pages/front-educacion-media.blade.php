@extends('layouts/main')

@section('content')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="#" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item">
            <a href="#" title="">PACE UC</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            Educación Media
        </li>
    </ol>
    <!--Fin Breadcrumb-->
    <main class="container" role="main" id="main-content">
        <div class="row">
            <div class="col-md-8 col-lg-9 mb-32">
                <h1 class="uc-h1 pr-5 mb-0">Educación Media</h1>
                <div class="lsch lsch--hover">
                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                    </button>
                    <div id="lsch__popover-1" style="display:none;">
                        <div class="popover-body">
                            <video width="150" height="200" controls>
                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                            </video>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-lg-9 mb-24">
                <div class="row">
                    <div class="col-sm-6 col-lg-4 mb-24">
                        <img src="http://via.placeholder.com/280x280" class="img-fluid" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="paragraph">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fringilla eget dui vel vestibulum. In feugiat commodo purus, a semper purus eleifend et. Integer tortor sem, semper nec blandit a, ultricies ac leo.</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="col-md-4 col-lg-3" role="navigation" aria-label="Submenú PACE UC">
                <div class="box">
                    <h4 class="uc-subtitle mb-16">PACE UC:</h4>
                    <a class="uc-btn text-weight--medium mb-16" href="" title="">Educación media</a>
                    <a class="uc-btn text-weight--medium mb-16" href="" title="">Educación superior</a>
                </div>
            </nav>
        </div>
        <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
        <!--Actividades-->
        <section class="mb-4 mb-lg-5" aria-label="Actividades">
            <div class="row mb-40">
                <div class="col-md-8 col-lg-9">
                    <h2 class="uc-h2 title-decorated mb-32">Actividades</h2>
                    <div class="paragraph">
                        <p>Los estudiantes pueden participar en actividades grupales de apoyo a los cursos de primer año en el área de matemáticas y química. Cada grupo está liderado por un tutor que apoya semanalmente la nivelación de contenidos y la adquisición de habilidades en estas áreas.</p>
                    </div>
                </div>
            </div>
            <div class="cards-carousel three-items-carousel">
                <?php for( $a = 0; $a < 10; $a++ ) { ?>
                    <div class="cards-carousel__item sameheight">
                        <article class="uc-card card-height--same">
                            <img src="http://via.placeholder.com/300x190" class="img-fluid" alt="">
                            <div class="uc-card_body">
                                <h3 class="uc-h4">Talleres en la UC</h3>
                                <div class="uc-text-divider divider-primary my-12"></div>
                                <div class="paragraph mb-24">
                                    <p>Durante la educación media, la UC trabaja con estudiantes de 5 liceos de la RM, ofreciendo talleres académicos y de desarrollo personal/vocacional a sus estudiantes, aportando a su desarrollo y buscando mejorar su proceso post secundario.</p>
                                </div>
                                <div class="text-right mt-auto">
                                    <a href="#" title="" class="uc-btn btn-inline">
                                        Descargar calendario UC
                                        <i class="uc-icon" aria-hidden="true">save_alt</i>
                                    </a>
                                </div>
                                <div class="lsch lsch--hover">
                                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                    </button>
                                    <div id="lsch__popover-1" style="display:none;">
                                        <div class="popover-body">
                                            <video width="150" height="200" controls>
                                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php } ?>
            </div>
        </section>
        <!--Fin Actividades-->
        <!--Tutores-->
        <section class="mb-4 mb-lg-5" aria-label="Tutores">
            <div class="row mb-40">
                <div class="col-md-12">
                    <div class="d-sm-flex justify-content-between align-items-center mb-32">
                        <h2 class="uc-h2 title-decorated">Tutores</h2>
                        <a href="" title="" class="uc-btn btn-inline d-none d-sm-block">Ver tutores <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i></a>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9">
                    <div class="paragraph">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec rutrum nisl. Sed ut tincidunt ligula, egestas scelerisque ante. Nullam at tellus gravida, varius ipsum quis, pulvinar augue. Aenean cursus, elit laoreet sagittis euismod, est massa luctus tortor.</p>
                    </div>
                </div>
            </div>
            <div class="img-carousel five-items-carousel">
                <?php for( $a = 0; $a < 10; $a++ ) { ?>
                    <div class="cards-carousel__item sameheight">
                        <article class="uc-card card-height--same">
                            <img src="http://via.placeholder.com/300x190" class="img-fluid" alt="">
                            <div class="uc-card_body">
                                <h3 class="uc-h4">Alejandro Gallardo <span class="uc-card-subtitle">Profesor pregrado</span></h3>
                                <div class="uc-text-divider divider-primary my-12"></div>
                                <div class="paragraph">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam porta pellentesque finibus. In porttitor est augue, at aliquet massa tristique in. Donec viverra enim eget lacus consectetur, eu scelerisque metus congue. </p>
                                </div>
                                <a href="#" title="" class="uc-btn btn-inline mt-24 mb-24">
                                    profesor@uc.cl
                                </a>
                                <div class="text-right mt-auto">
                                    <a href="#" title="" class="uc-btn btn-inline">
                                        Ver más
                                        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                    </a>
                                </div>
                                <div class="lsch lsch--hover">
                                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                    </button>
                                    <div id="lsch__popover-1" style="display:none;">
                                        <div class="popover-body">
                                            <video width="150" height="200" controls>
                                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </article>
                    </div>
                <?php } ?>
            </div>
        </section>
        <!--Fin Tutores-->
        <!--Comunidad-->
        <section class="mb-80 mb-lg-8" aria-label="Comunidad">
            <div class="row mb-40">
                <div class="col-md-8 col-lg-9">
                    <h2 class="uc-h2 title-decorated mb-32">Comunidad</h2>
                    <div class="paragraph">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fringilla eget dui vel vestibulum. In feugiat commodo purus, a semper purus eleifend et. Integer tortor sem.</p>
                    </div>
                </div>
            </div>
            <h3 class="uc-h3 title-decorated mb-32">Galería de imágenes</h3>
            <div class="img-carousel four-items-carousel">
                <?php for( $a = 0; $a < 10; $a++ ) { ?>
                    <div class="cards-carousel__item sameheight">
                        <a class="uc-card" data-fancybox="gallery" href="http://via.placeholder.com/1200x768" title="">
                            <img src="http://via.placeholder.com/300x190" class="img-fluid" alt="">
                        </a>
                    </div>
                <?php } ?>
            </div>
        </section>
        <!--Fin Comunidad-->
        
    </main>
    
@endsection
