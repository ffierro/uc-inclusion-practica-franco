@extends('layouts/main')

@section('content')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="#" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            Apoyo a estudiantes
        </li>
    </ol>
    <!--Fin Breadcrumb-->
    <main role="main" id="main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-32">
                    <h1 class="uc-h1 pr-5 mb-0">Apoyo a estudiantes</h1>
                    <div class="lsch lsch--hover">
                        <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                        </button>
                        <div id="lsch__popover-1" style="display:none;">
                            <div class="popover-body">
                                <video width="150" height="200" controls>
                                    <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9 mb-24">
                    <div class="paragraph">
                        <p>Buscamos promover que una vez que los estudiantes ingresen a la UC, éstos logren graduarse de la Universidad Católica, participando de la vida universitaria y del proceso académico en condiciones de equidad, trabajando de forma articulada con las unidades académicas y unidades de apoyo de la UC.</p>
                    </div>
                </div>
            </div>
            <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
            <!--Actividades de inserción-->
            <section class="uc-card card-type--horizontal card-type--horizontal--xl mb-4 mb-lg-5">
                <div class="uc-card_img">
                    <img src="http://via.placeholder.com/700x420" class="img-fluid" alt="">
                </div>
                <div class="uc-card_body">
                    <h2 class="uc-h3 mb-16">Actividades de inserción</h2>
                    <div class="paragraph">
                        <p>Los estudiantes que ingresan por las vías de admisión de equidad Talento e Inclusión, cupo supernumerario Beca de Excelencia Académica y vías interculturales, tendrán una jornada de inserción a la UC durante el mes de enero, además de la bienvenida institucional.</p>
                    </div>
                    <div class="lsch lsch--hover">
                        <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                        </button>
                        <div id="lsch__popover-1" style="display:none;">
                            <div class="popover-body">
                                <video width="150" height="200" controls>
                                    <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Fin Actividades de inserción-->
        </div>
        <!--Acompañamiento socioafectivo-->
        <section class="bg-lightgray--xs py-4 py-lg-5" aria-label="Acompañamiento socioafectivo">
            <div class="container">
                <div class="row mb-40">
                    <div class="col-md-8 col-lg-9">
                        <h2 class="uc-h2 title-decorated mb-32">Acompañamiento socioafectivo</h2>
                        <div class="paragraph">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec rutrum nisl. Sed ut tincidunt ligula, egestas scelerisque ante. Nullam at tellus gravida, varius ipsum quis, pulvinar augue. Aenean cursus, elit laoreet sagittis euismod, est massa luctus tortor.</p>
                        </div>
                    </div>
                </div>
                <div class="cards-carousel three-items-carousel" aria-label="Slider de acompañamiento socioafectivo">
                    <?php for( $a = 0; $a < 10; $a++ ) { ?>
                        <div class="cards-carousel__item" >
                            <article class="uc-card card-height--same">
                                <img src="http://via.placeholder.com/300x190" class="img-fluid" alt="">
                                <div class="uc-card_body">
                                    <h3 class="uc-h4 mb-24">Actividades de integración y deportivas</h3>
                                    <div class="text-right mt-auto">
                                        <a href="#" title="Más sobre Actividades de integración" class="uc-btn btn-inline">
                                            Más sobre Actividades de integración
                                            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                        </a>
                                    </div>
                                    <div class="lsch lsch--hover">
                                        <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                        </button>
                                        <div id="lsch__popover-1" style="display:none;">
                                            <div class="popover-body">
                                                <video width="150" height="200" controls>
                                                    <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <!--Fin Acompañamiento socioafectivo-->
        <!--Diagnósticos y nivelación-->
        <section class="bg-lightgray py-4 py-lg-5" aria-label="Diagnósticos y nivelación">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 mb-24">
                        <h2 class="uc-h2 title-decorated mb-32">Diagnósticos y nivelación</h2>
                        <div class="paragraph wp-content">
                            <p>Con el objetivo de apoyar tempranamente a los estudiantes que ingresan a la UC, y en conjunto con las Unidades Académicas de Matemática, Letras y Química junto al Centro de Apoyo al Rendimiento Académico y la exploración vocacional (CARA); durante los meses de enero y febrero se realizan diagnósticos y nivelaciones:</p>
                            <ul>
                                <li>Diagnósticos de Pre-cálculo, Razonamiento Cuantitativo e Introducción a la Matemática</li>
                                <li>Programa Introducción a la Matemática Universitaria (PIMU)</li>
                                <li>Taller de Química</li>
                                <li>Curso de Introducción a la lectura y escritura académica</li>
                                <li>Taller CARA: Preparándonos para rendir mejor en la UC</li>
                                <li>Actividades 1er y 2do Semestre</li>
                            </ul>
                        </div>
                        <a href="#" title="Más sobre Diagnóstico y nivelación" class="uc-btn btn-inline mt-32">
                            Más sobre Diagnóstico y nivelación
                            <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
                        </a>
                    </div>
                    <div class="col-lg-6 d-lg-flex align-items-start">
                        <div class="lsch mb-24 mr-32 ml-16">
                            <button tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover" data-toggle="popover" data-placement="bottom">
                            </button>
                            <div id="lsch__popover" style="display:none;">
                                <div class="popover-body">
                                    <video width="150" height="200" controls>
                                        <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                        <img src="http://via.placeholder.com/490x420" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!--Fin Diagnósticos y nivelación-->
        <!--Apoyos académicos-->
        <section class="container mt-4 mt-lg-5 mb-80 mb-lg-8" aria-label="Apoyos académicos">
            <div class="row mb-40">
                <div class="col-md-8 col-lg-9">
                    <h2 class="uc-h2 title-decorated mb-32">Apoyos académicos</h2>
                    <div class="paragraph">
                        <p>Los estudiantes pueden participar en actividades grupales de apoyo a los cursos de primer año en el área de matemáticas y química. Cada grupo está liderado por un tutor que apoya semanalmente la nivelación de contenidos y la adquisición de habilidades en estas áreas.</p>
                    </div>
                </div>
            </div>
            <div class="cards-carousel three-items-carousel" aria-label="Slider de actividades de apoyo académico">
                <?php for( $a = 0; $a < 10; $a++ ) { ?>
                    <div class="cards-carousel__item" >
                        <article class="uc-card card-height--same">
                            <img src="http://via.placeholder.com/300x190" class="img-fluid" alt="">
                            <div class="uc-card_body">
                                <h3 class="uc-h4">Programa de Apoyo a la Comunicación Académica PRAC</h3>
                                <div class="uc-text-divider divider-primary my-12"></div>
                                <div class="paragraph mb-24">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam porta pellentesque finibus. In porttitor est augue, at aliquet massa tristique in. Donec viverra enim eget lacus consectetur, eu scelerisque metus congue. </p>
                                </div>
                                <div class="text-right mt-auto">
                                    <a href="#" title="Más sobre PRAC" class="uc-btn btn-inline">
                                        Más sobre PRAC
                                        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                    </a>
                                </div>
                                <div class="lsch lsch--hover">
                                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                    </button>
                                    <div id="lsch__popover-1" style="display:none;">
                                        <div class="popover-body">
                                            <video width="150" height="200" controls>
                                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php } ?>
            </div>
        </section>
        <!--Fin Apoyos académicos-->
    </main>
@endsection
