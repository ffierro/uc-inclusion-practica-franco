@extends('layouts/main')

@section('content')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="#" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item">
            <a href="#" title="PIANE UC">PIANE UC</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            Comunidad accesible
        </li>
    </ol>
    <!--Fin Breadcrumb-->
    <main class="container" role="main" id="main-content">
        <div class="mb-4 mb-lg-5">
            <div class="row">
                <div class="col-md-8 col-lg-9 mb-32">
                    <h1 class="uc-h1 pr-5 mb-0">Comunidad accesible</h1>
                    <div class="lsch lsch--hover">
                        <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                        </button>
                        <div id="lsch__popover-1" style="display:none;">
                            <div class="popover-body">
                                <video width="150" height="200" controls>
                                    <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9 mb-24">
                    <div class="paragraph wp-content">
                        <p>¿Qué significa participar en el PIANE?</p>
                        <p>El proceso de aprendizaje implica, entre otras cosas, acceder a información, participar en las actividades académicas (clases, ayudantías, salidas a terreno, etc.), y rendir evaluaciones.</p>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/0O2xA6lGvro" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <nav class="col-md-4 col-lg-3" role="navigation" aria-label="Submenú PIANE UC">
                    <div class="box">
                        <h4 class="uc-subtitle mb-16">PIANE UC:</h4>
                        <a class="uc-btn text-weight--medium mb-16" href="">Estudiantes</a>
                        <a class="uc-btn text-weight--medium mb-16" href="">Profesores</a>
                        <a class="uc-btn text-weight--medium mb-16" href="">Comunidad accesible</a>
                    </div>
                </nav>
            </div>
        </div>
        <!--Material de interés destacado-->
        <section class="mb-80 mb-lg-8" aria-label="Material de interés destacado">
            <div class="row mb-40">
                <div class="col-md-8 col-lg-9">
                    <h2 class="uc-h2 title-decorated mb-32">Material de interés destacado</h2>
                    <div class="paragraph">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec rutrum nisl. Sed ut tincidunt ligula, egestas scelerisque ante. Nullam at tellus gravida, varius ipsum quis, pulvinar augue. Aenean cursus, elit laoreet sagittis euismod, est massa luctus tortor.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php for( $a = 0; $a < 3; $a++ ) { ?>
                    <div class="col-sm-6 col-lg-4 mb-24">
                        <article class="uc-card card-height--same">
                            <img src="http://via.placeholder.com/300x190" class="img-fluid">
                            <div class="uc-card_body">
                                <h3 class="uc-h4">Lenguaje sobre discapacidad</h3>
                                <div class="uc-text-divider divider-primary my-12"></div>
                                <p class="mb-24">Un lenguaje acorde al modelo social de la discapacidad es el que se ha promovido desde la campaña del SENADIS.</p>
                                <div class="text-right mt-auto">
                                    <a href="#" title="" class="uc-btn btn-inline">
                                        Descargar PDF de SENADIS
                                        <i class="uc-icon" aria-hidden="true">save_alt</i>
                                    </a>
                                </div>
                                <div class="lsch lsch--hover">
                                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                    </button>
                                    <div id="lsch__popover-1" style="display:none;">
                                        <div class="popover-body">
                                            <video width="150" height="200" controls>
                                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php } ?>
            </div>
            <div class="text-right mt-24">
                <a href="#" title="" class="uc-btn btn-inline">
                    Ver otros
                    <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
                </a>
            </div>
        </section>
        <!--Fin Material de interés destacado-->
    </main>
@endsection
