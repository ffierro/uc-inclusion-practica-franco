@extends('layouts/main')

@section('content')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="#" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item">
            <a href="#" title="PACE UC">PACE UC</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item">
            <a href="#" title=">Educación Superior">Educación Superior</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            Apoyos académicos
        </li>
    </ol>
    <!--Fin Breadcrumb-->
    <main class="container mb-4 mb-lg-5" role="main" id="main-content">
        <div class="row">
            <div class="col-md-12 mb-32">
                <h1 class="uc-h1 pr-5 mb-0">Apoyos académicos</h1>
                <div class="lsch lsch--hover">
                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                    </button>
                    <div id="lsch__popover-1" style="display:none;">
                        <div class="popover-body">
                            <video width="150" height="200" controls>
                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                            </video>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4 mb-24">
                <img src="http://via.placeholder.com/384x234" class="img-fluid" alt="">
            </div>
            <div class="col-md-8">
                <div class="paragraph">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fringilla eget dui vel vestibulum. In feugiat commodo purus, a semper purus eleifend et. Integer tortor sem, semper nec blandit a, ultricies ac leo.</p>
                </div>
            </div>
        </div>
        <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
        <ul class="mb-80 mb-lg-8">
            <?php for( $a = 0; $a < 4; $a++ ) { ?>
                <li class="uc-card card-type--horizontal mb-32">
                    <img src="http://via.placeholder.com/280x216" class="img-fluid" alt="">
                    <div class="uc-card_body">
                        <h3 class="uc-h4 mt-16">Lorem ipsum dolor sit amet</h3>
                        <div class="uc-text-divider divider-primary my-12"></div>
                        <div class="paragraph">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam porta pellentesque finibus. In porttitor est augue, at aliquet massa tristique in. Donec viverra enim eget lacus consectetur, eu scelerisque metus congue. </p>
                        </div>
                        <div class="lsch lsch--hover">
                            <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                            </button>
                            <div id="lsch__popover-1" style="display:none;">
                                <div class="popover-body">
                                    <video width="150" height="200" controls>
                                        <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </main>

@endsection
