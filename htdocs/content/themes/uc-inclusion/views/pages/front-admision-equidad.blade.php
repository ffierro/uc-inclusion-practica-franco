@extends('layouts/main')

@section('content')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="#" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            Admisión equidad
        </li>
    </ol>
    <!--Fin Breadcrumb-->
    <!--Content-->
    <main class="container" role="main" id="main-content">
        <div class="row">
            <div class="col-md-12 mb-32">
                <h1 class="uc-h1 pr-5 mb-0">Admisión equidad</h1>
                <div class="lsch lsch--hover">
                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                    </button>
                    <div id="lsch__popover-1" style="display:none;">
                        <div class="popover-body">
                            <video width="150" height="200" controls>
                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                            </video>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-lg-9 mb-24">
                <div class="paragraph">
                    <p>Buscamos ampliar oportunidades de acceso y aprendizaje, disminuyendo barreras para una participación plena y con equidad, reflejando a nuestra sociedad y valorando la riqueza de la diversidad en la UC.</p>
                </div>
            </div>
        </div>
        <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
        <section class="boxes-reverse mb-80 mb-lg-8" aria-label="Vías de admisión">
            <?php for( $a = 0; $a < 5; $a++ ) { ?>
                <article class="uc-card card-type--horizontal card-type--horizontal--xl boxes-reverse__item">
                    <div class="uc-card_img">
                        <img src="http://via.placeholder.com/700x420" class="img-fluid" alt="">
                    </div>
                    <div class="uc-card_body">
                        <h2 class="uc-h2 mb-16">Necesidades Educativas Especiales</h2>
                        <div class="paragraph mb-16">
                            <p>Esta vía ofrece vacantes para a postulantes que presenten alguna discapacidad del tipo motora, visual y/o auditiva. Quienes ingresen a través de ella podrán recibir el apoyo del Programa para Alumnos con Necesidades Educativas Espciales (PIANE UC).</p>
                        </div>
                        <div class="text-right mt-auto">
                            <a href="#" title="Más sobre vía intercultural" class="uc-btn btn-inline">
                                Más sobre vía intercultural
                                <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                            </a>
                        </div>
                        <div class="lsch lsch--hover">
                            <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                            </button>
                            <div id="lsch__popover-1" style="display:none;">
                                <div class="popover-body">
                                    <video width="150" height="200" controls>
                                        <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            <?php } ?>
        </section>
    </main>
    <!--Fin Content-->


@endsection
