@extends('layouts/main')

@section('content')
    <!--Content-->
    <div class="img-single">
        <img src="http://placeimg.com/1440/300/any" alt="">
    </div>
    <div class="container offset-white">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <!--Breadcrumb-->
                <ol class="uc-breadcrumb my-40" aria-label="Breadcrumb">
                    <li class="uc-breadcrumb_item">
                        <a href="#" title="Portada">Portada</a>
                        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                    </li>
                    <li class="uc-breadcrumb_item">
                        <a href="#" title="Noticias">Noticias</a>
                        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                    </li>
                    <li class="uc-breadcrumb_item current">
                        Facultad de Teología UC presenta nueva beca de equidad de género
                    </li>
                </ol>
                <!--Fin Breadcrumb-->
            </div>
        </div>
        <main role="main" id="main-content" class="row">
            <div class="col-md-10 offset-md-1">
                <div class="lsch lsch--hover">
                    <button tabindex="0" type="button" class="lsch__button"
                            data-popover-content="#lsch__popover" data-toggle="popover"
                            data-placement="bottom">
                    </button>
                    <div id="lsch__popover" style="display:none;">
                        <div class="popover-body">
                            <video width="150" height="200" controls>
                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4"
                                        type="video/mp4">
                            </video>
                        </div>
                    </div>
                </div>
                <a href="#" title="Tag" class="uc-tag tag-fixed mb-16">Tag</a>
                <h1 class="uc-h1 mb-40" id="articulo">Facultad de Teología UC presenta nueva beca de equidad de género</h1>
            </div>
            <div class="col-md-7 offset-md-1">
                <!--Main-->
                <article class="mb-4 mb-lg-5" role="contentinfo" aria-label="Artículo" aria-labelledby="Artículo">
                    <div class="row align-items-center mb-40">
                        <div class="col-md-auto paragraph mb-24">
                            <p>7 de febrero de 2020</p>
                        </div>
                        <div class="col">
                            <span class="uc-hr"></span>
                        </div>
                    </div>
                    <div class="p-size--xl text-weight--bold">
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</p>
                    </div>
                    <span class="uc-text-divider divider-secondary my-32"></span>
                    <div class="body-article">
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</p>
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</p>
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. <a href>Cras sollicitudin rutrum orci eget tincidunt.</a> Sed sed auctor est. Cras scelerisque leo sed <strong>nunc tincidunt pharetra.</strong></p>
                    </div>
                    <div class="body-article">
                        <blockquote aria-label="Blockquote">
                            <p><span class="quote-lg uc-quote">“Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra."</span></p>
                            <a href title="" class="uc-share-quote-btn" aria-labelledby="Blockquote">Compartir</a>
                        </blockquote>
                    </div>
                    <div class="body-article">
                        <h3>Una alternativa accesible para mirar el fondo marino</h3>
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</p>
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</p>
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. <a href>Cras sollicitudin rutrum orci eget tincidunt.</a> Sed sed auctor est. Cras scelerisque leo sed <strong>nunc tincidunt pharetra.</strong></p>
                    </div>
                    <!--Box contacto-->
                    <section class="uc-card p-48 mb-4 mb-lg-5">
                        <h4 class="h6 mb-24">Lorem auctor sagittis vitae eget sem.</h4>
                        <div class="paragraph">
                            <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus.</p>
                        </div>
                    </section>
                    <!--Fin box contacto-->
                    <!--Info relacionada-->
                    <section class="body-article mb-4 mb-lg-5">
                        <h2>Información Relacionada</h2>
                        <ul>
                            <li><a href title="">Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</a></li>
                            <li><a href title="">Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</a></li>
                        </ul>
                    </section>
                    <!--Fin Info relacionada-->
                </article>
            </div>
        </main>
    </div>
    <!--Fin Content-->
@endsection
