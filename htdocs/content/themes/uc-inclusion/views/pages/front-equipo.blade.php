@extends('layouts/main')

@section('content')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="#" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item">
            <a href="#" title="PACE UC">PACE UC</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            Equipo
        </li>
    </ol>
    <!--Fin Breadcrumb-->
    <main class="container" role="main" id="main-content">
        <div class="row">
            <div class="col-md-7">
                <h1 class="uc-h1 pr-5 mb-32">Equipo</h1>
                <div class="lsch lsch--hover">
                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                    </button>
                    <div id="lsch__popover-1" style="display:none;">
                        <div class="popover-body">
                            <video width="150" height="200" controls>
                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                            </video>
                        </div>
                    </div>
                </div>
                <div class="paragraph">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pulvinar, magna nec egestas bibendum, justo mauris luctus metus, vitae auctor mi leo vel mauris. Donec fermentum eleifend velit, at scelerisque sapien porttitor eu. Pellentesque sodales est eu efficitur dignissim.</p>
                </div>
            </div>
            <div class="col-md-5 wp-content">
                <img src="http://via.placeholder.com/500x280" class="img-fluid" alt="">
            </div>
        </div>
        <div class="mb-4 mb-lg-5">
            <?php for( $a = 0; $a < 4; $a++ ) { ?>
                <hr class="uc-hr mt-1 mb-2">
                <div class="d-flex align-items-center mb-32">
                    <h2 class="uc-h2">Categoría de equipo</h2>
                    <span class="uc-heading-decoration"></span>
                </div>
                <div class="row">
                    <?php for( $b = 0; $b < 4; $b++ ) { ?>
                        <div class="col-sm-6 col-md-4 col-lg-3 col-xl mb-32">
                            <article class="uc-card card-height--same">
                                <img src="http://via.placeholder.com/300x190" class="img-fluid p-20 pb-0" alt="">
                                <div class="uc-card_body">
                                    <h3 class="uc-h4">Alejandro Gallardo <span class="uc-card-subtitle">Profesor pregrado</span></h3>
                                    <div class="uc-text-divider divider-primary my-12"></div>
                                    <div class="paragraph mb-24">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam porta pellentesque finibus. In porttitor est augue, at aliquet massa tristique in. Donec viverra enim eget lacus consectetur, eu scelerisque metus congue. </p>
                                    </div>
                                    <a href="#" title="" class="uc-btn btn-inline mt-auto">
                                        profesor@uc.cl
                                    </a>
                                    <div class="text-right mt-32">
                                        <a href="#" title="" class="uc-btn btn-inline">
                                            Ver más
                                            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                        </a>
                                    </div>
                                    <div class="lsch lsch--hover">
                                        <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                        </button>
                                        <div id="lsch__popover-1" style="display:none;">
                                            <div class="popover-body">
                                                <video width="150" height="200" controls>
                                                    <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <!--Paginador-->
            <ol class="paginator mt-1">
                <li class="paginator__arrow" ><a href=""><i class="uc-icon" aria-hidden="true">keyboard_arrow_left</i></a></li>
                <li class="paginator__item paginator__item--active"><a href="">1</i></a></li>
                <li class="paginator__item"><a href="">2</i></a></li>
                <li class="paginator__item" ><a href="">3</i></a></li>
                <li class="paginator__item" ><a href="">4</i></a></li>
                <li class="paginator__item" ><a href="">5</i></a></li>
                <li class="paginator__arrow" ><a href=""><i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i></a></li>
            </ol>
            <!--Fin Paginador-->
        </div>
    </main>
    
@endsection
