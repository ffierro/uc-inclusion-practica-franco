@extends('layouts/main')

@section('content')
    <main role="main" id="main-content">
        <!--Hero-->
        <section class="hero">
            <ul class="hero__carousel" aria-label="Carrusel de destacados">
                <?php for( $a = 0; $a < 3; $a++ ) { ?>
                    <li class="hero__carousel__item" style="background-image: url('http://placeimg.com/1920/510/any')">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10">
                                    <a href="#" title="Categoría - Economía" aria-label="Categoría - Economía" class="uc-tag mb-24">Economía</a>
                                    <h2 class="mb-24">Chile: la desigualdad que persiste</h2>
                                    <img class="hide" src="http://placeimg.com/1920/510/any" alt="">
                                    <a href="#" title="Ir a Chile: la desigualdad que persiste"
                                        class="hero__link uc-btn btn-inline">
                                        Ir al artículo
                                        <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="lsch lsch--hover">
                            <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                            </button>
                            <div id="lsch__popover-1" style="display:none;">
                                <div class="popover-body">
                                    <video width="150" height="200" controls>
                                        <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
            <div class="hero__controls">
                <button style="display: none" id="pause" aria-label="Pausar carrusel"><i class="uc-icon icon-shape--rounded" aria-hidden="true">pause</i></button>
                <button id="play" aria-label="Reanudar carrusel"><i class="uc-icon icon-shape--rounded" aria-hidden="true">play_arrow</i></button>
            </div>
        </section>
        <!--Fin hero-->
        <!--Mensaje-->
        <section class="msg bg-lightgray py-20 mb-4 mb-md-80" aria-label="Mensaje informativo">
            <div class="container">
                <h4 class="msg__text"><span class="msg__icon"><img src="http://localhost/uc-inclusion/content/themes/uc-inclusion/assets/images/icon-hands.svg" role="image" alt=""></span>Lorem ipsum dolor sit amet</h4>
            </div>
        </section>
        <!--Fin mensaje-->
        <!--Programas-->
        <section class="container mb-4 mb-lg-5" aria-label="Programas">
            <h2 class="uc-h2 title-decorated mb-32">Programas</h2>
            <div class="row">
                <?php for( $a = 0; $a < 4; $a++ ) { ?>
                    <div class="col-sm-6 col-xl-3 mb-24">
                        <article class="uc-card card-height--same uc-card--lsch">
                            <img src="http://via.placeholder.com/200x255" class="img-fluid" alt="">
                            <div class="uc-card_body">
                                <h3 class="uc-h4" id="titulo-card-1">Admisión especial</h3>
                                <span class="uc-text-divider divider-primary my-12"></span>
                                <div class="paragraph">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                                </div>
                                <div class="text-right mt-auto">
                                    <a href="#" title="Ir a admisión especial" class="uc-btn btn-inline mt-24" aria-labelledby="titulo-card-1">
                                        Ir a admisión especial
                                        <i class="uc-icon"  aria-hidden="true">keyboard_arrow_right</i>
                                    </a>
                                </div>
                                <div class="lsch lsch--hover">
                                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-2" data-toggle="popover" data-placement="bottom">
                                    </button>
                                    <div id="lsch__popover-2" style="display:none;">
                                        <div class="popover-body">
                                            <video width="150" height="200" controls>
                                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php } ?>
            </div>
        </section>
        <!--Fin programas-->
        <!--Noticias-->
        <section class="container mb-4 mb-lg-5" aria-label="Noticias">
            <div class="d-sm-flex justify-content-between align-items-center mb-32">
                <h2 class="uc-h2 title-decorated">Noticias</h2>
                <a href="#" title=""  class="uc-btn btn-inline d-none d-sm-block">
                    Ver todas las noticias
                    <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
                </a>
            </div>
            <div class="grid row no-gutters">
                <div class="col-lg-7 grid__column">
                    <article class="uc-card card-bg--image card-gradient--bottom-blue" style="background-image:url('http://via.placeholder.com/768x1024');">
                        <div class="tags tag-fixed">
                            <a href="#" title="" class="uc-tag" aria-label="Crisis social - Categoría de artículo">Crisis social</a>
                        </div>
                        <img src="http://via.placeholder.com/768x1024" alt="" class="hide">
                        <div class="uc-card_body justify-content-end">
                            <div class="relative">
                                <a href="#" title="" aria-labelledby="titulo-grid-card-1">
                                    <h3 id="titulo-grid-card-1">Lorem ipsum dolor sit amet</h3>
                                </a>
                                <p class="ellipsis">Esta es una bajada que se agregó, lorem ipsum dolor sit.</p>
                                <div class="lsch lsch--hover">
                                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-2" data-toggle="popover" data-placement="bottom">
                                    </button>
                                    <div id="lsch__popover-2" style="display:none;">
                                        <div class="popover-body">
                                            <video width="150" height="200" controls>
                                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-lg-5 grid__column grid__column--items">
                    <article class="uc-card card-type--horizontal">
                        <img src="http://via.placeholder.com/200x255" class="img-fluid" alt="">
                        <div class="uc-card_body">
                            <div class="tags d-flex">
                                <a href="#" title="" class="uc-tag" aria-label="Crisis social - Categoría de artículo">
                                    Crisis social
                                </a>
                            </div>
                            <h3 class="uc-h4 mt-16" id="titulo-grid-card-2">Lorem ipsum dolor sit amet</h3>
                            <div class="text-right mt-auto">
                                <a href="#" title="" class="uc-btn btn-inline" aria-labelledby="titulo-grid-card-2">
                                    Ver más
                                    <i class="uc-icon"  aria-hidden="true">keyboard_arrow_right</i>
                                </a>
                            </div>
                            <div class="lsch lsch--hover">
                                <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                </button>
                                <div id="lsch__popover-1" style="display:none;">
                                    <div class="popover-body">
                                        <video width="150" height="200" controls>
                                            <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="uc-card card-type--horizontal">
                        <img src="http://via.placeholder.com/200x255" class="img-fluid" alt="">
                        <div class="uc-card_body">
                            <div class="tags d-flex">
                                <a href="#" title="" class="uc-tag" aria-label="Crisis social - Categoría de artículo">
                                    Crisis social
                                </a>
                            </div>
                            <h3 class="uc-h4 mt-16" id="titulo-grid-card-3">Lorem ipsum dolor sit amet</h3>
                            <div class="text-right mt-auto">
                                <a href="#" title="" class="uc-btn btn-inline" aria-labelledby="titulo-grid-card-2">
                                    Ver más
                                    <i class="uc-icon"  aria-hidden="true">keyboard_arrow_right</i>
                                </a>
                            </div>
                            <div class="lsch lsch--hover">
                                <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                </button>
                                <div id="lsch__popover-1" style="display:none;">
                                    <div class="popover-body">
                                        <video width="150" height="200" controls>
                                            <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
            <div class="text-right d-sm-none">
                <a href="#" title="" class="uc-btn btn-inline">
                    Ver todas las noticias
                    <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
                </a>
            </div>
        </section>
        <!--Fin Noticias-->
        <!--Experiencias inclusivas-->
        <section class="container mb-4 mb-lg-5 pb-lg-4" aria-label="Experiencias inclusivas">
            <div class="d-sm-flex justify-content-between align-items-center mb-32">
                <h2 class="uc-h2 title-decorated">Experiencias Inclusivas</h2>
                <a href="#" title=""  class="uc-btn btn-inline d-none d-sm-block">
                    Ver todas las Experiencias Inclusivas
                    <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
                </a>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <blockquote class="blockquote">
                        <div class="blockquote__text">
                            <p>“Crecí en la comuna de La Pintana, donde no tuve una enseñanza de mucha calidad. Donde la universidad no era un tema tan profundo, y tal vez lo posible era imaginar otras opciones para mi vida. No contaba con ningún referente en mi familia que haya estudiado en la universidad, por lo que pensé que tal vez esta opción tampoco era para mí.  Luego me cambié de colegio, al Instituto Claudio Matte donde me mostraron otras vías por las que podría ingresar a la Universidad. Una de ellas era Talento e Inclusión."</p>
                        </div>
                        <h5 class="blockquote__name">Javier Cristómo</h5>
                        <p class="blockquote__profile">Estudiante UC</p>
                        <div class="lsch lsch--hover">
                            <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover" data-toggle="popover" data-placement="bottom">
                            </button>
                            <div id="lsch__popover" style="display:none;">
                                <div class="popover-body">
                                    <video width="150" height="200" controls>
                                        <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </blockquote>
                </div>
                <div class="col-lg-7">
                    <img src="http://placeimg.com/768/500/any" alt="" class="img-fluid">
                </div>
            </div>
            <div class="text-right d-sm-none">
                <a href="#" title="" class="uc-btn btn-inline">
                    Ver todas las Experiencias Inclusivas
                    <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
                </a>
            </div>
        </section>
        <!--Fin Experiencias inclusivas-->
        <!--Equipo-->
        <section class="container mb-80 mb-lg-8" aria-label="Equipo">
            <article class="uc-card card-type--horizontal card-type--horizontal--xl boxes-reverse__item">
                <div class="uc-card_img">
                    <img src="http://via.placeholder.com/700x420" class="img-fluid" alt="">
                </div>
                <div class="uc-card_body">
                    <h2 class="uc-h2 mb-16">Equipo</h2>
                    <div class="paragraph mb-16">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lobortis eu ante sed faucibus. Mauris placerat hendrerit semper. Morbi interdum convallis est, ac porttitor massa venenatis sit amet. Phasellus tempus justo sed fringilla cursus. Aliquam a leo vel nunc aliquet luctus vel vitae lorem. Phasellus lacinia placerat enim quis viverra. Fusce dapibus in lectus sit amet rutrum.</p>
                    </div>
                    <div class="text-right mt-auto">
                        <a href="#" title="Más sobre vía intercultural" class="uc-btn btn-inline">
                            Conoce a todo el equipo
                            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                        </a>
                    </div>
                    <div class="lsch lsch--hover">
                        <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-2" data-toggle="popover" data-placement="bottom">
                        </button>
                        <div id="lsch__popover-2" style="display:none;">
                            <div class="popover-body">
                                <video width="150" height="200" controls>
                                    <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </section>
        <!--Fin equipo-->
    </main>
@endsection
