@extends('layouts/main')

@section('content')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="#" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            Comunidad inclusiva
        </li>
    </ol>
    <!--Fin Breadcrumb-->
    <main role="main" class="container" id="main-content">
        <div class="row">
            <div class="col-md-7 mb-24">
                <h1 class="uc-h1 pr-5 mb-32">Comunidad Inclusiva</h1>
                <div class="lsch lsch--hover">
                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                    </button>
                    <div id="lsch__popover-1" style="display:none;">
                        <div class="popover-body">
                            <video width="150" height="200" controls>
                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                            </video>
                        </div>
                    </div>
                </div>
                <div class="paragraph">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum dignissim consectetur semper. Donec eu vehicula velit, vel euismod ante. Proin eu venenatis odio.</p>
                </div>
            </div>
            <div class="col-md-5 wp-content">
                <img src="http://via.placeholder.com/490x270" class="img-fluid" alt="">
            </div>
        </div>  
        <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
        <!--Talleres y capacitaciones-->
        <section class="mb-4 mb-lg-5" aria-label="Talleres y capacitaciones">
            <h2 class="uc-h2 title-decorated mb-32">Talleres y capacitaciones</h2>
            <div class="cards-carousel three-items-carousel" aria-label="Slider de acompañamiento socioafectivo">
                <?php for( $a = 0; $a < 10; $a++ ) { ?>
                    <div class="cards-carousel__item" >
                        <article class="uc-card card-height--same">
                            <img src="http://via.placeholder.com/300x190" class="img-fluid" alt="">
                            <div class="uc-card_body">
                                <h3 class="uc-h4">Taller de química</h3>
                                <div class="uc-text-divider divider-primary my-12"></div>
                                <div class="paragraph mb-24">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                                <div class="text-right mt-auto">
                                    <a href="#" title="Más sobre taller de química" class="uc-btn btn-inline">
                                        Más sobre taller de química
                                        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                    </a>
                                </div>
                                <div class="lsch lsch--hover">
                                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                    </button>
                                    <div id="lsch__popover-1" style="display:none;">
                                        <div class="popover-body">
                                            <video width="150" height="200" controls>
                                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php } ?>
            </div>
        </section>
        <!--Fin Talleres y capacitaciones-->
        <!--Boxes-->
        <div class="boxes-reverse mb-80 mb-lg-8">
            <?php for( $a = 0; $a < 3; $a++ ) { ?>
                <section class="uc-card card-type--horizontal card-type--horizontal--xl boxes-reverse__item" aria-label="Comunidad transgénero">
                    <div class="uc-card_img">
                        <img src="http://via.placeholder.com/700x420" class="img-fluid" alt="">
                    </div>
                    <div class="uc-card_body">
                        <h2 class="uc-h2 mb-16">Comunidad transgénero</h2>
                        <div class="paragraph">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus mattis ut felis quis fermentum. Nullam tincidunt faucibus erat, quis accumsan.</p>
                        </div>
                        <div class="lsch lsch--hover">
                            <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                            </button>
                            <div id="lsch__popover-1" style="display:none;">
                                <div class="popover-body">
                                    <video width="150" height="200" controls>
                                        <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php } ?>
        </div>
        <!--Fin Boxes-->  
    </main>

@endsection
