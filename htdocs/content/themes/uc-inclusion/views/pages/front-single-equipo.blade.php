@extends('layouts/main')

@section('content')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="#" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item">
            <a href="#" title="PACE UC">PACE UC</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item">
            <a href="#" title="Equipo">Equipo</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            Paulina Cifuentes
        </li>
    </ol>
    <!--Fin Breadcrumb-->
    <div class="container">
        <main class="mb-4 mb-lg-5" role="main" id="main-content">
            <div class="row">
                <div class="col-md-8  mb-24">
                    <h1 class="uc-h1 pr-5">Paulina Cifuentes</h1>
                    <div class="lsch lsch--hover">
                        <button tabindex="0" type="button" class="lsch__button"
                                data-popover-content="#lsch__popover" data-toggle="popover"
                                data-placement="bottom">
                        </button>
                        <div id="lsch__popover" style="display:none;">
                            <div class="popover-body">
                                <video width="150" height="200" controls>
                                    <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4"
                                            type="video/mp4">
                                </video>
                            </div>
                        </div>
                    </div>
                    <h2 class="h4 uc-h4 mb-32">Profesor pre-grado</h2>
                    <h3>Sus funciones</h3>
                    <div class="paragraph mb-32">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fringilla eget dui vel vestibulum. In feugiat commodo purus, a semper purus eleifend et. Integer tortor sem, semper nec blandit a, ultricies ac leo.</p>
                    </div>
                    <h3>Sus servicios</h3>
                    <div class="paragraph">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fringilla eget dui vel vestibulum. In feugiat commodo purus, a semper purus eleifend et. Integer tortor sem, semper nec blandit a, ultricies ac leo.</p>
                    </div>

                </div>
                <div class="col-md-4">
                    <img src="http://via.placeholder.com/384x460" class="img-fluid" alt="">
                </div>
            </div>
        </main>
        <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
        <!--Tutores-->
        <section class="mb-80 mb-lg-8" role="complementary" aria-label="Otros tutores">
            <h2 class="uc-h2 title-decorated mb-32">Otros Tutores</h2>
            <div class="row">
                <?php for( $a = 0; $a < 3; $a++ ) { ?>
                    <div class="col-sm-6 col-xl-3 mb-24">
                        <article class="uc-card card-height--same">
                            <img src="http://via.placeholder.com/300x190" class="img-fluid" alt="">
                            <div class="uc-card_body">
                                <h3 class="uc-h4">Alejandro Gallardo <span class="uc-card-subtitle">Profesor pregrado</span></h3>
                                <div class="uc-text-divider divider-primary my-12"></div>
                                <div class="paragraph mb-24">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam porta pellentesque finibus. In porttitor est augue, at aliquet massa tristique in. Donec viverra enim eget lacus consectetur, eu scelerisque metus congue. </p>
                                </div>
                                <a href="#" title="profesor@uc.cl" class="uc-btn btn-inline mt-auto">
                                    profesor@uc.cl
                                </a>
                                <div class="lsch lsch--hover">
                                    <button tabindex="0" type="button" class="lsch__button"
                                            data-popover-content="#lsch__popover" data-toggle="popover"
                                            data-placement="bottom">
                                    </button>
                                    <div id="lsch__popover" style="display:none;">
                                        <div class="popover-body">
                                            <video width="150" height="200" controls>
                                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4"
                                                        type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php } ?>
            </div>
        </section>
        <!--Fin Tutores-->
    </div>

@endsection
