@extends('layouts/main')

@section('breadcrumbs')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="/" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            Noticias
        </li>
    </ol>
    <!--Fin Breadcrumb-->
@endsection


@section('content')

    <!--Content-->
    <main crole="main" class="container" id="main-content" aria-label="Noticias">
        <h1 class="uc-h1 mb-32">Noticias</h1>
        <!--Filter-->
        <form class="uc-filters mb-40" aria-label="Filtro de noticias" role="form">
            <fieldset class="no-margin">
                <div class="uc-form-group label-inline mb-0">
                    <label for="news_filter">Filtrar por:</label>
                    <select id="news_filter" class="uc-input-style w-icon">
                        <option value="all">Todas las categorías ({{count($news)}})</option>

                        @foreach($terms as $term)
                            @if($term->slug==$current_filter_slug)
                                <option value="{{$term->slug}}" selected="selected">{{$term->name}}</option>

                            @else
                                <option value="{{$term->slug}}">{{$term->name}}</option>

                            @endif
                        @endforeach
                    </select>
                </div>
            </fieldset>
        </form>
        <!--Fin Filter-->
    @isset($news)
        <!--List-->
            <div class="row mb-4 mb-lg-5" aria-label="Listado de noticias">
                @foreach($news as $key=>$item)
                    <div class="col-md-6 col-lg-3 mb-32">
                        @include('parts/entry_card_news',['item'=>$item,'section'=>'news-landing'])
                    </div>
                @endforeach
                <div class="col-12">
                    <!--Paginador-->
                    <ol class="paginator mt-1">
                        <li class="paginator__arrow" ><a href=""><i class="uc-icon" aria-hidden="true">keyboard_arrow_left</i></a></li>
                        <li class="paginator__item paginator__item--active"><a href="">1</i></a></li>
                        <li class="paginator__item"><a href="">2</i></a></li>
                        <li class="paginator__item" ><a href="">3</i></a></li>
                        <li class="paginator__item" ><a href="">4</i></a></li>
                        <li class="paginator__item" ><a href="">5</i></a></li>
                        <li class="paginator__arrow" ><a href=""><i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i></a></li>
                    </ol>
                    <!--Fin Paginador-->
                </div>
            </div>
            <!--Fin List-->
        @endisset
    </main>
    <!--Fin Content-->
@endsection
