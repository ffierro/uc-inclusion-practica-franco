@extends('layouts/main')

@section('content')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="#" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            Contacto
        </li>
    </ol>
    <!--Fin Breadcrumb-->
    <!--Content-->
    <main class="container" role="main" id="main-content">
        <h1 class="uc-h1 mb-32">Contacto</h1>
        <div class="row mb-4 mb-lg-5">
            <div class="col-lg-5 mb-32">
                <h4>Dirección Inclusión:</h4>
                <p>Edificio Centro de Desarrollo Docente, tercer piso, campus San Joaquín. Avenida Vicuña Mackenna 4860, Macul - Metro San Joaquín.</p>
                <p>Mail: <a href title="direccioninclusion@uc.cl">direccioninclusion@uc.cl</a></p>
                <div class="iframe-responsive mt-32">
                    <div class="hide">
                        <h5>Ubicación en mapa de UC inclusión</h5>
                        <p>UC Inclusión está ubicado en tal calle con tal calle, cerca de tal cosa.</p>
                    </div>
                    <iframe title="Mapa de ubicación" aria-hidden="true" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3327.0848308914906!2d-70.61433748479945!3d-33.49916998076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662d00dd70fe57b%3A0x8c5fb0bb1c5bfa29!2sCentro%20de%20Desarrollo%20Docente%20-%20Manuel%20Letelier!5e0!3m2!1ses!2scl!4v1582631565930!5m2!1ses!2scl" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>

            </div>
            <div class="col-lg-7">
                <div class="uc-card">
                    <div class="lsch lsch--hover">
                        <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                        </button>
                        <div id="lsch__popover-1" style="display:none;">
                            <div class="popover-body">
                                <video width="150" height="200" controls>
                                    <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    </div>
                    <div class="p-44">
                        <h2 class="h4 mb-32">Escríbanos sus consultas, sugerencias o reclamos.</h2>
                        <div class="paragraph">
                            <p>Si usted desea más información de cursos, escríbanos aquí:</p>
                        </div>
                    </div>
                    <hr class="uc-hr">
                    <form class="form py-28 px-44" role="form">
                        <fieldset class="uc-form-group mb-28">
                            <label for="input-nombre">Nombres <span aria-hidden="true">*</span></label>
                            <span class="error-message">Debes completar el campo Nombres.</span>
                            <input id="input-nombre" type="text" required="" class="uc-input-style" placeholder="Nombres">
                        </fieldset>
                        <fieldset class="uc-form-group mb-28">
                            <label for="input-apellidos">Apellidos <span aria-hidden="true">*</span></label>
                            <span class="error-message">Debes completar el campo Apellidos.</span>
                            <input id="input-apellidos" type="text" required="" class="uc-input-style" placeholder="Apellidos">
                        </fieldset>
                        <fieldset class="uc-form-group mb-28">
                            <label for="input-email">Email <span aria-hidden="true">*</span></label>
                            <span class="error-message">Debes completar el campo Email con un correo válido.</span>
                            <input id="input-email" type="email" required="" class="uc-input-style" placeholder="Email">
                        </fieldset>
                        <fieldset class="uc-form-group mb-28">
                            <label for="input-telefono">Teléfono <span aria-hidden="true">*</span></label>
                            <span class="error-message">Debes completar el campo Teléfono con un número válido.</span>
                            <input id="input-telefono" type="tel" required="" class="uc-input-style" placeholder="Teléfono">
                        </fieldset>
                        <fieldset class="uc-form-group mb-28">
                            <legend id="input-tipo">Tipo de contacto</legend>
                            <ul aria-labelledby="input-tipo" role="group">
                                <li class="uc-checkbox">
                                    <input id="input-contacto" type="checkbox" class="uc-checkbox__input">
                                    <label for="input-contacto" class="uc-checkbox__label">Contacto</label>
                                </li>
                                <li class="uc-checkbox">
                                    <input id="input-sugerencia" type="checkbox" class="uc-checkbox__input">
                                    <label for="input-sugerencia" class="uc-checkbox__label">Sugerencia</label>
                                </li>
                                <li class="uc-checkbox">
                                    <input id="input-reclamo" type="checkbox" class="uc-checkbox__input">
                                    <label for="input-reclamo" class="uc-checkbox__label">Reclamo</label>
                                </li>
                            </ul>
                        </fieldset>
                        <fieldset class="uc-form-group mb-28">
                            <label for="input-mensaje">Mensaje <span aria-hidden="true">*</span></label>
                            <span class="error-message">Debes completar el campo Mensaje.</span>
                            <textarea class="uc-input-style" required="" id="input-mensaje" placeholder="Mensaje"></textarea>
                        </fieldset>
                        <div class="text-right">
                            <button class="uc-btn btn-cta btn-inline" type="submit">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <!--Fin Content-->
@endsection
