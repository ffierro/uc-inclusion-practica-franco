@extends('layouts/main')

@section('content')
    <!--Content-->
    <div class="img-single">
        <img src="http://placeimg.com/1440/300/any" alt="">
    </div>
    <div class="container offset-white">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <!--Breadcrumb-->
                <ol class="uc-breadcrumb my-40" aria-label="Breadcrumb">
                    <li class="uc-breadcrumb_item">
                        <a href="#" title="Portada">Portada</a>
                        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                    </li>
                    <li class="uc-breadcrumb_item">
                    <a href="#" title="Noticias">Noticias</a>
                        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                    </li>
                    <li class="uc-breadcrumb_item current">
                        Facultad de Teología UC presenta nueva beca de equidad de género
                    </li>
                </ol>
                <!--Fin Breadcrumb-->
            </div>
        </div>
        <main role="main" id="main-content" class="row justify-content-center">
            <div class="col-md-10">
                <a href="#" title="Tag" class="uc-tag tag-fixed mb-16">Tag</a>
                <h1 class="uc-h1 mb-40" id="articulo">Facultad de Teología UC presenta nueva beca de equidad de género</h1>
                <div class="lsch lsch--hover">
                    <button tabindex="0" type="button" class="lsch__button"
                            data-popover-content="#lsch__popover" data-toggle="popover"
                            data-placement="bottom">
                    </button>
                    <div id="lsch__popover" style="display:none;">
                        <div class="popover-body">
                            <video width="150" height="200" controls>
                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4"
                                        type="video/mp4">
                            </video>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <!--Main-->
                <article class="mb-4 mb-lg-5" role="contentinfo" aria-label="Artículo" aria-labelledby="Artículo">
                    <div class="row align-items-center mb-40">
                        <div class="col-md-auto paragraph mb-24">
                            <p>7 de febrero de 2020</p>
                        </div>
                        <div class="col">
                            <span class="uc-hr"></span>
                        </div>
                    </div>
                    <div class="p-size--xl text-weight--bold">
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</p>
                    </div>
                    <span class="uc-text-divider divider-secondary my-32"></span>
                    <div class="body-article">
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</p>
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</p>
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. <a href>Cras sollicitudin rutrum orci eget tincidunt.</a> Sed sed auctor est. Cras scelerisque leo sed <strong>nunc tincidunt pharetra.</strong></p>
                    </div>
                    <div class="body-article">
                        <blockquote aria-label="Blockquote">
                            <p><span class="quote-lg uc-quote">“Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra."</span></p>
                            <a href title="" class="uc-share-quote-btn" aria-labelledby="Blockquote">Compartir</a>
                        </blockquote>
                    </div>
                    <div class="body-article">
                        <h3>Una alternativa accesible para mirar el fondo marino</h3>
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</p>
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</p>
                        <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. <a href>Cras sollicitudin rutrum orci eget tincidunt.</a> Sed sed auctor est. Cras scelerisque leo sed <strong>nunc tincidunt pharetra.</strong></p>
                    </div>
                    <!--Galería-->
                    <section class="my-4 my-lg-5">
                        <h2 class="uc-h3 title-decorated mb-16">Galería de imágenes</h2>
                        <div class="gallery-carousel three-items-carousel mb-4 mb-lg-5">
                            <?php for( $a = 0; $a < 10; $a++ ) { ?>
                                <div class="cards-carousel__item sameheight">
                                    <a class="uc-card" data-fancybox="gallery" href="http://via.placeholder.com/1200x768" title="">
                                        <img src="http://via.placeholder.com/300x190" class="img-fluid" alt="">
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </section>
                    <!--Fin Galería-->
                    <!--Box contacto-->
                    <section class="uc-card p-48 mb-4 mb-lg-5">
                        <h4 class="h6 mb-24">Lorem auctor sagittis vitae eget sem.</h4>
                        <div class="paragraph">
                            <p>Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus.</p>
                        </div>
                    </section>
                    <!--Fin box contacto-->
                    <!--Info relacionada-->
                    <section class="body-article mb-4 mb-lg-5">
                        <h2>Información Relacionada</h2>
                        <ul>
                            <li><a href title="">Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</a></li>
                            <li><a href title="">Lorem auctor sagittis vitae eget sem. Donec pellentesque orci ac urna pharetra, eu tincidunt ligula luctus. Nullam mattis consequat mauris nec auctor. Morbi gravida nisi sit amet ligula pulvinar, id euismod elit cursus. Proin non eleifend lorem. Cras sollicitudin rutrum orci eget tincidunt. Sed sed auctor est. Cras scelerisque leo sed nunc tincidunt pharetra.</a></li>
                        </ul>
                    </section>
                    <!--Fin Info relacionada--> 
                    <!--Share-->
                    <span class="uc-hr mb-40"></span>
                    <section class="row mb-40">
                        <div class="col-lg-6 mb-24">
                            <div class="uc-subtitle mb-12">¿Te gusta esta publicación?</div>
                            <div class="uc-like-container">
                                <button class="uc-like-btn">
                                    <img src="http://localhost/uc-inclusion/content/themes/uc-inclusion/assets/images/icon-like.svg" alt="" role="image">
                                </button>
                                <div class="h2 ml-16">0</div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="uc-subtitle mb-12">Comparte esta publicación</div>
                            <div>
                                <a href title="" target="_blank"><img src="http://localhost/uc-inclusion/content/themes/uc-inclusion/assets/images/icon-facebook.svg" role="image" alt=""></a>
                                <a href title="" class="ml-24"><img src="http://localhost/uc-inclusion/content/themes/uc-inclusion/assets/images/icon-twitter.svg" role="image" alt=""></a>
                                <a href title="" class="ml-24"><img src="http://localhost/uc-inclusion/content/themes/uc-inclusion/assets/images/icon-mail.svg" role="image" alt=""></a>
                            </div>
                        </div>
                    </section>
                    <span class="uc-hr mb-4 mb-lg-5"></span>
                    <!--Fin Share-->
                </article>
            </div>
            <div class="col-md-4 col-lg-3">
                <aside id="sidebar" role="complementary" aria-label="Información">
                    <div class="uc-card">
                        <div class="uc-card_body">
                            <h3 class="h6 mb-12">Información</h3>
                            <span class="uc-hr mb-24"></span>
                            <div class="mb-24">
                                <h4 class="uc-subtitle mb-16"><i class="uc-icon icon-gray icon-small" aria-hidden="true">local_offer</i> Etiquetas</h4>
                                <div class="uc-tags">
                                    <a title="" href="/temas/contingencia/" class="uc-tag">Crisis social</a>
                                    <a title="" href="/temas/revista-universitaria/" class="uc-tag">Revista universitaria</a>
                                    <a title="" href="/temas/entrevista/" class="uc-tag">Entrevista</a>
                                    <a title="" href="/temas/instituto-de-sociologia/" class="uc-tag">Instituto de Sociología</a>
                                </div>
                            </div>
                            <div class="mb-24">
                                <h4 class="uc-subtitle mb-16"><i class="uc-icon icon-gray icon-small" aria-hidden="true">local_offer</i> Eje estratégico</h4>
                                <div class="uc-tags">
                                    <a title="" href="/temas/contingencia/" class="uc-tag">Crisis social</a>
                                    <a title="" href="/temas/revista-universitaria/" class="uc-tag">Revista universitaria</a>
                                    <a title="" href="/temas/entrevista/" class="uc-tag">Entrevista</a>
                                    <a title="" href="/temas/instituto-de-sociologia/" class="uc-tag">Instituto de Sociología</a>
                                </div>
                            </div>
                            <div class="mb-24">
                                <h4 class="uc-subtitle mb-16"><i class="uc-icon icon-gray icon-small" aria-hidden="true">local_offer</i> Unidad académica</h4>
                                <div class="uc-tags">
                                    <a title="" href="/temas/contingencia/" class="uc-tag">Crisis social</a>
                                    <a title="" href="/temas/revista-universitaria/" class="uc-tag">Revista universitaria</a>
                                    <a title="" href="/temas/entrevista/" class="uc-tag">Entrevista</a>
                                    <a title="" href="/temas/instituto-de-sociologia/" class="uc-tag">Instituto de Sociología</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </main>
    </div>
    <!--Fin Content-->
    <!--Sigue explorando-->
    <section class="container mb-80 mb-lg-8" role="complementary">
        <h2 class="uc-h2 title-decorated mb-32">Sigue explorando</h2>
        <div class="grid row no-gutters">
                <div class="col-lg-7 grid__column">
                    <article class="uc-card card-bg--image card-gradient--bottom-blue" style="background-image:url('http://via.placeholder.com/768x1024');">
                        <a href="#" title="" class="uc-tag tag-fixed">Crisis social</a>
                        <div class="uc-card_body justify-content-end">
                            <div class="relative">
                                <a href="#" title="" aria-labelledby="titulo-grid-card-1">
                                    <h3 id="titulo-grid-card-1">Lorem ipsum dolor sit amet</h3>
                                </a>
                                <div class="lsch lsch--hover">
                                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-2" data-toggle="popover" data-placement="bottom">
                                    </button>
                                    <div id="lsch__popover-2" style="display:none;">
                                        <div class="popover-body">
                                            <video width="150" height="200" controls>
                                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-lg-5 grid__column grid__column--items">
                    <article class="uc-card card-type--horizontal">
                        <img src="http://via.placeholder.com/200x255" class="img-fluid" alt="">
                        <div class="uc-card_body">
                            <a href="#" title="" class="uc-tag">
                                Crisis social
                            </a>
                            <h3 class="uc-h4 mt-16">Lorem ipsum dolor sit amet</h3>
                            <div class="text-right mt-auto">
                                <a href="#" title="" class="uc-btn btn-inline">
                                    Ver más
                                    <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                </a>
                            </div>
                            <div class="lsch lsch--hover">
                                <button tabindex="0" type="button" class="lsch__button"
                                        data-popover-content="#lsch__popover" data-toggle="popover"
                                        data-placement="bottom">
                                </button>
                                <div id="lsch__popover" style="display:none;">
                                    <div class="popover-body">
                                        <video width="150" height="200" controls>
                                            <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4"
                                                    type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="uc-card card-type--horizontal">
                        <img src="http://via.placeholder.com/200x255" class="img-fluid" alt="">
                        <div class="uc-card_body">
                            <a href="#" title="" class="uc-tag">
                                Crisis social
                            </a>
                            <h3 class="uc-h4 mt-16">Lorem ipsum dolor sit amet</h3>
                            <div class="text-right mt-auto">
                                <a href="#" title="" class="uc-btn btn-inline">
                                    Ver más
                                    <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                </a>
                            </div>
                            <div class="lsch lsch--hover">
                                <button tabindex="0" type="button" class="lsch__button"
                                        data-popover-content="#lsch__popover" data-toggle="popover"
                                        data-placement="bottom">
                                </button>
                                <div id="lsch__popover" style="display:none;">
                                    <div class="popover-body">
                                        <video width="150" height="200" controls>
                                            <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4"
                                                    type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
    </section>
    <!--Fin Sigue explorando-->
@endsection
