@extends('layouts/main')

@section('content')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="#" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item">
            <a href="#" title="PIANE UC">PIANE UC</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            Estudiantes
        </li>
    </ol>
    <!--Fin Breadcrumb-->
    <main role="main" id="main-content">
        <div class="container mb-4 mb-lg-5">
            <div class="row">
                <div class="col-md-8 col-lg-9 mb-24">
                    <h1 class="uc-h1 mb-32">Estudiantes</h1>
                    <div class="lsch lsch--hover">
                        <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                        </button>
                        <div id="lsch__popover-1" style="display:none;">
                            <div class="popover-body">
                                <video width="150" height="200" controls>
                                    <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9 mb-24">
                    <div class="paragraph">
                        <p>¿Qué significa participar en el PIANE?</p>
                        <p>El proceso de aprendizaje implica, entre otras cosas, acceder a información, participar en las actividades académicas (clases, ayudantías, salidas a terreno, etc.), y rendir evaluaciones.</p>
                    </div>
                </div>
                <nav class="col-md-4 col-lg-3" role="navigation" aria-label="Submenú PIANE UC">
                    <div class="box">
                        <h4 class="uc-subtitle mb-16">PIANE UC:</h4>
                        <a class="uc-btn text-weight--medium mb-16" href="" title="">Estudiantes</a>
                        <a class="uc-btn text-weight--medium mb-16" href="" title="">Profesores</a>
                        <a class="uc-btn text-weight--medium mb-16" href="" title="">Comunidad accesible</a>
                    </div>
                </nav>
            </div>
        </div>
        <!--Actividades de inserción-->
        <section class="mb-4 mb-lg-5">
            <div class="bg-lightgray pt-4 pt-lg-5">
                <div class="container">
                    <div class="uc-card card-type--horizontal card-type--horizontal--xl mb-40">
                        <div class="uc-card_img">
                            <img src="http://via.placeholder.com/700x420" class="img-fluid" alt="">
                        </div>
                        <div class="uc-card_body">
                            <h2 class="uc-h2 mb-16">Adecuaciones curriculares</h2>
                            <div class="paragraph">
                                <p>Algunos alumnos necesitan contar con adecuaciones curriculares que no alteren  los contenidos ni objetivos de aprendizaje, pero sí puedan proponer modificar aspectos de las metodologías y/o formatos de evaluación, manteniendo los mismos niveles de exigencia que para el resto del curso. Es por esto que se las llama adecuaciones curriculares no significativas.</p>
                            </div>
                            <div class="lsch lsch--hover">
                                <button tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover" data-toggle="popover" data-placement="bottom">
                                </button>
                                <div id="lsch__popover" style="display:none;">
                                    <div class="popover-body">
                                        <video width="150" height="200" controls>
                                            <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="paragraph">
                        <p>Para definir aquellas adecuaciones que se ajusten a las necesidades de cada alumno, se realiza una entrevista con alguno de los profesionales del PIANE. A partir de esta información,  el equipo elabora una Carta que notifica a los equipos docentes sobre estas adecuaciones y realiza seguimiento a su implementación.</p>
                        <p>Además de las adecuaciones generales, hay adecuaciones específicas de acuerdo al tipo de discapacidad que presenta el estudiante.</p>
                    </div>
                </div>
            </div>
            <div class="bg-lightgray--xs pt-4 pt-lg-5">
                <div class="container">
                    <div class="cards-carousel three-items-carousel">
                        <?php for( $a = 0; $a < 10; $a++ ) { ?>
                            <div class="cards-carousel__item sameheight">
                                <article class="uc-card card-height--same">
                                    <div class="iframe-responsive">
                                        <iframe width="300" height="190" src="https://www.youtube.com/embed/0O2xA6lGvro" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                            
                                    </div>
                                    <div class="uc-card_body">
                                        <h3 class="uc-h4">Video Adecuaciones curriculares para discapacidad visual</h3>
                                    </div>
                                </article>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <!--Fin Actividades de inserción-->
        <!--Apoyos académicos-->
        <section class="mb-80 mb-lg-8" ari-label="Apoyos académicos">
            <div class="container">
                <div class="row mb-40">
                    <div class="col-md-8 col-lg-9">
                        <h2 class="uc-h2 title-decorated mb-32">Apoyos académicos</h2>
                        <div class="paragraph">
                            <p>Los estudiantes pueden participar en actividades grupales de apoyo a los cursos de primer año en el área de matemáticas y química. Cada grupo está liderado por un tutor que apoya semanalmente la nivelación de contenidos y la adquisición de habilidades en estas áreas.</p>
                        </div>
                    </div>
                </div>
                <div class="cards-carousel three-items-carousel">
                    <?php for( $a = 0; $a < 10; $a++ ) { ?>
                        <div class="cards-carousel__item sameheight">
                            <article class="uc-card card-height--same">
                                <img src="http://via.placeholder.com/300x190" class="img-fluid" alt="">
                                <div class="uc-card_body">
                                    <h3 class="uc-h4">Programa de Apoyo a la Comunicación Académica PRAC</h3>
                                    <div class="uc-text-divider divider-primary my-12"></div>
                                    <div class="paragraph mb-24">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam porta pellentesque finibus. In porttitor est augue, at aliquet massa tristique in. Donec viverra enim eget lacus consectetur, eu scelerisque metus congue. </p>
                                    </div>
                                    <div class="text-right mt-auto">
                                        <a href="#" title="" class="uc-btn btn-inline">
                                            Más sobre PRAC
                                            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                        </a>
                                    </div>
                                    <div class="lsch lsch--hover">
                                        <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                        </button>
                                        <div id="lsch__popover-1" style="display:none;">
                                            <div class="popover-body">
                                                <video width="150" height="200" controls>
                                                    <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <!--Fin Apoyos académicos-->
    </main>
@endsection
