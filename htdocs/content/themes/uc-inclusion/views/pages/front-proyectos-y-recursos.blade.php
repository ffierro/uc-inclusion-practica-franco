@extends('layouts/main')

@section('content')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="#" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            Proyectos y recursos
        </li>
    </ol>
    <!--Fin Breadcrumb-->
    <div class="container">
        <main class="mb-4 mb-lg-5" role="main" id="main-content">
            <div class="row mb-40">
                <div class="col-md-12 mb-32">
                    <h1 class="uc-h1 pr-5 mb-0">Proyectos y recursos</h1>
                    <div class="lsch lsch--hover">
                        <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                        </button>
                        <div id="lsch__popover-1" style="display:none;">
                            <div class="popover-body">
                                <video width="150" height="200" controls>
                                    <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9 mb-24">
                    <div class="paragraph">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse est enim, lobortis gravida fermentum sit amet, scelerisque ornare massa. Cras nisi ligula, dictum eu justo ac, rutrum bibendum tellus. Proin mollis sit amet nisi vel maximus. Duis finibus tristique maximus. Duis quis condimentum lacus. Etiam convallis erat sit amet sagittis cursus. Cras sodales convallis nunc, ut sodales augue sagittis vestibulum. Donec risus felis, mollis sit amet blandit vel, vehicula vitae lectus. Sed vestibulum arcu eu elit interdum, sit amet sollicitudin velit molestie.</p>
                    </div>
                </div>
            </div>
            <!--Listado de documentos-->
            <section class="row justify-content-center">
                <div class="col-md-10">
                    <div class="uc-tabpanel" data-tabpanel>
                        <!-- Tabs mobile se muestran como Select -->
                        <div class="uc-card card-bg--gray card-radius--none card-border--none d-block d-lg-none mb-32">
                            <div class="uc-card_body">
                                <label for="tabSelect"><strong>Seleccione tab</strong></label>
                                <select name="tabSelect" id="tabSelect" class="uc-input-style" data-tabselect>
                                    <option value="tab-01">Proyectos</option>
                                    <option value="tab-02">Recursos</option>
                                </select>
                            </div>
                        </div>
                        <!-- Tabs en desktop se muestran como botones -->
                        <ul class="uc-tabs mb-32 d-none d-lg-flex">
                            <li class="uc-tabs_item">
                                <a href="javascript:void(0);" class="uc-tabs_item-link" data-tabtarget="tab-01" data-tabactive>Proyectos</a>
                            </li>
                            <li class="uc-tabs_item">
                                <a href="javascript:void(0);" class="uc-tabs_item-link" data-tabtarget="tab-02">Recursos</a>
                            </li>
                        </ul>
                        <form class="uc-filters mb-40" aria-label="Filtro de proyectos" role="form">
                            <fieldset class="uc-form-group mb-0">
                                <label class="hide" for="buscador">Buscar</label>
                                <input id="buscador" type="text" class="uc-input-style w-icon search" placeholder="Buscar por">
                                <span class="w-icon search" aria-hidden="true"></span>
                            </fieldset>
                        </form>
                        <div class="uc-tab-body">
                            <div data-tab="tab-01">
                                <ul class="table-list" aria-label="Listado de documentos">
                                    <li class="table-list__item table-list__item--download">
                                        <a href="">
                                            <span class="hide">(El enlace descarga un documento)</span> 
                                            <img class="table-list__item__icon" src="http://localhost/uc-inclusion/content/themes/uc-inclusion/assets/images/icon-pdf.svg" alt="">
                                            <h4 id="titulo-item-1">1 En el camino hacia la Educación Superior inclusiva en Chile</h4>
                                            <div class="d-flex align-items-center">
                                                <p>4,5 MB</p>
                                                <i class="uc-icon icon-shape--rounded ml-1" aria-hidden="true">arrow_downward</i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="table-list__item table-list__item--download">
                                        <a href="">
                                            <span class="hide">(El enlace descarga un documento)</span> 
                                            <img class="table-list__item__icon" src="http://localhost/uc-inclusion/content/themes/uc-inclusion/assets/images/icon-word.svg" alt="">
                                            <h4 id="titulo-item-1">En el camino hacia la Educación Superior inclusiva en Chile</h4>
                                            <div class="d-flex align-items-center">
                                                <p>4,5 MB</p>
                                                <i class="uc-icon icon-shape--rounded ml-1" aria-hidden="true">arrow_downward</i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="table-list__item table-list__item--external">
                                        <a href="" target="_blank">
                                            <span class="hide">(El enlace se abre en una nueva pestaña)</span> 
                                            <img class="table-list__item__icon" src="http://localhost/uc-inclusion/content/themes/uc-inclusion/assets/images/icon-ppt.svg" alt="">
                                            <h4 id="titulo-item-2">En el camino hacia la Educación Superior inclusiva en Chile</h4>
                                            <div class="d-flex align-items-center">
                                                <p>4,5 MB</p>
                                                <i class="uc-icon icon-shape--rounded ml-1" aria-hidden="true">arrow_downward</i>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                 <!--Paginador-->
                                <ol class="paginator mt-3">
                                    <li class="paginator__arrow" ><a href=""><i class="uc-icon" aria-hidden="true">keyboard_arrow_left</i></a></li>
                                    <li class="paginator__item paginator__item--active"><a href="">1</i></a></li>
                                    <li class="paginator__item"><a href="">2</i></a></li>
                                    <li class="paginator__item" ><a href="">3</i></a></li>
                                    <li class="paginator__item" ><a href="">4</i></a></li>
                                    <li class="paginator__item" ><a href="">5</i></a></li>
                                    <li class="paginator__arrow" ><a href=""><i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i></a></li>
                                </ol>
                                <!--Fin Paginador-->
                            </div>
                            <div data-tab="tab-02">
                                <ul class="table-list" aria-label="Listado de documentos">
                                    <li class="table-list__item table-list__item--download">
                                        <a href="">
                                            <span class="hide">(El enlace descarga un documento)</span> 
                                            <img class="table-list__item__icon" src="http://localhost/uc-inclusion/content/themes/uc-inclusion/assets/images/icon-pdf.svg" alt="">
                                            <h4 id="titulo-item-1">2 En el camino hacia la Educación Superior inclusiva en Chile</h4>
                                            <div class="d-flex align-items-center">
                                                <p>4,5 MB</p>
                                                <i class="uc-icon icon-shape--rounded ml-1" aria-hidden="true">arrow_downward</i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="table-list__item table-list__item--download">
                                        <a href="">
                                            <span class="hide">(El enlace descarga un documento)</span> 
                                            <img class="table-list__item__icon" src="http://localhost/uc-inclusion/content/themes/uc-inclusion/assets/images/icon-word.svg" alt="">
                                            <h4 id="titulo-item-1">En el camino hacia la Educación Superior inclusiva en Chile</h4>
                                            <div class="d-flex align-items-center">
                                                <p>4,5 MB</p>
                                                <i class="uc-icon icon-shape--rounded ml-1" aria-hidden="true">arrow_downward</i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="table-list__item table-list__item--external">
                                        <a href="" target="_blank">
                                            <span class="hide">(El enlace se abre en una nueva pestaña)</span> 
                                            <img class="table-list__item__icon" src="http://localhost/uc-inclusion/content/themes/uc-inclusion/assets/images/icon-ppt.svg" alt="">
                                            <h4 id="titulo-item-2">En el camino hacia la Educación Superior inclusiva en Chile</h4>
                                            <div class="d-flex align-items-center">
                                                <p>4,5 MB</p>
                                                <i class="uc-icon icon-shape--rounded ml-1" aria-hidden="true">arrow_downward</i>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Fin Listado de documentos-->
        </main>
        <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
        <!--Videos-->
        <section class="mb-80 mb-lg-8" role="complementary" aria-label="Videos">
            <h2 class="uc-h2 title-decorated mb-32">Videos</h2>
            <div class="row">
                <?php for( $a = 0; $a < 6; $a++ ) { ?>
                    <div class="col-sm-6 col-lg-4 mb-32">
                        <article class="uc-card card-height--same">
                            <div class="iframe-responsive">
                                <iframe width="300" height="190" src="https://www.youtube.com/embed/0O2xA6lGvro" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                            
                            </div>
                            <div class="uc-card_body">
                                <h3 class="uc-h4">Video Adecuaciones curriculares para discapacidad visual</h3>
                            </div>
                        </article>
                    </div>
                <?php } ?>
            </div>
            <div class="text-center">
                <a href="#" title="" class="uc-btn btn-inline">
                    Ver más videos
                    <i class="uc-icon icon-shape--rounded ml-1" aria-hidden="true">add</i>
                </a>
            </div>
        </section>
        <!--Fin Videos-->
    </div>

@endsection
