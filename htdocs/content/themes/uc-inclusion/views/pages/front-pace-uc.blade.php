@extends('layouts/main')

@section('content')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="#" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            PACE UC
        </li>
    </ol>
    <!--Fin Breadcrumb-->
    <main class="container" role="main" id="main-content">
        <div class="row">
            <div class="col-md-7 mb-24">
                <h1 class="uc-h1 pr-5 mb-32">PACE UC</h1>
                <div class="lsch lsch--hover">
                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                    </button>
                    <div id="lsch__popover-1" style="display:none;">
                        <div class="popover-body">
                            <video width="150" height="200" controls>
                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                            </video>
                        </div>
                    </div>
                </div>
                <div class="paragraph">
                    <p>El Programa de Acompañamiento y Acceso Efectivo a la Educación Superior (PACE) es una iniciativa del Ministerio de Educación, implementada por instituciones de educación superior, que busca ampliar las oportunidades de aprendizaje, y de acceso, permanencia y graduación de la educación superior, de estudiantes cuyos contextos presentan barreras de desarrollo académico.</p>
                </div>
            </div>
            <div class="col-md-5 wp-content">
                <!--IMG-->
                <img src="http://via.placeholder.com/490x270" class="img-fluid" alt="">
                <!--Video
                <div class="iframe-responsive">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/eJnQBXmZ7Ek" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>-->
            </div>
        </div>
        <hr class="uc-hr mt-1 mb-3 mt-lg-4 mb-lg-80">
        <!--Conoce más sobre el programa PACE UC-->
        <section class="mb-4 mb-lg-5" aria-label="Conoce más sobre el programa PACE UC">
            <h2 class="uc-h2 title-decorated mb-32">Conoce más sobre el programa PACE UC</h2>
            <div class="row">
                <?php for( $a = 0; $a < 2; $a++ ) { ?>
                    <div class="col-md-4 mb-32">
                        <article class="uc-card card-height--same">
                            <img src="http://via.placeholder.com/300x190" class="img-fluid" alt="">
                            <div class="uc-card_body">
                                <h3 class="uc-h4">Educación media</h3>
                                <div class="uc-text-divider divider-primary my-12"></div>
                                <div class="paragraph mb-24">
                                    <p>Durante la educación media, la UC trabaja con estudiantes de 5 liceos de la RM, ofreciendo talleres académicos y de desarrollo personal/vocacional a sus estudiantes, aportando a su desarrollo y buscando mejorar su proceso post secundario.</p>
                                </div>
                                <div class="text-right mt-auto">
                                    <a href="#" title="" class="uc-btn btn-inline">
                                        Ir a Educación Media
                                        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                    </a>
                                </div>
                                <div class="lsch lsch--hover">
                                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                    </button>
                                    <div id="lsch__popover-1" style="display:none;">
                                        <div class="popover-body">
                                            <video width="150" height="200" controls>
                                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php } ?>
            </div>
        </section>
        <!--Fin Conoce más sobre el programa PACE UC-->
        <!--Liceos Asociados-->
        <section class="mb-4 mb-lg-5" aria-label="Liceos Asociados">
            <h2 class="uc-h2 title-decorated mb-32">Liceos Asociados</h2>
            <div class="logos-carousel five-items-carousel">
                <?php for( $a = 0; $a < 10; $a++ ) { ?>
                    <div class="cards-carousel__item sameheight">
                        <a href="" title="" class="uc-card sameheight d-flex align-items-center justify-content-center">
                            <img src="http://via.placeholder.com/300x190" class="p-24 img-fluid" alt="">
                            <div class="lsch lsch--hover">
                                <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                </button>
                                <div id="lsch__popover-1" style="display:none;">
                                    <div class="popover-body">
                                        <video width="150" height="200" controls>
                                            <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </section>
        <!--Fin Liceos Asociados-->
        <!--Equipo-->
        <section class="mb-4 mb-lg-5" aria-label="Equipo">
            <div class="row mb-40">
                <div class="col-md-12">
                    <div class="d-sm-flex justify-content-between align-items-center mb-32">
                        <h2 class="uc-h2 title-decorated">Equipo</h2>
                        <a href="" title="" class="uc-btn btn-inline d-none d-sm-block">Ver equipo completo <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i></a>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9">
                    <div class="paragraph">
                        <p>Los estudiantes pueden participar en actividades grupales de apoyo a los cursos de primer año en el área de matemáticas y química. Cada grupo está liderado por un tutor que apoya semanalmente la nivelación de contenidos y la adquisición de habilidades en estas áreas.</p>
                    </div>
                </div>
            </div>
            <div class="cards-carousel five-items-carousel">
                <?php for( $a = 0; $a < 10; $a++ ) { ?>
                    <div class="cards-carousel__item sameheight">
                        <article class="uc-card card-height--same">
                            <img src="http://via.placeholder.com/300x190" class="img-fluid" alt="">
                            <div class="uc-card_body">
                                <h3 class="uc-h4">Alejandro Gallardo <span class="uc-card-subtitle">Profesor pregrado</span></h3>
                                <div class="uc-text-divider divider-primary my-12"></div>
                                <div class="paragraph">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam porta pellentesque finibus. In porttitor est augue, at aliquet massa tristique in. Donec viverra enim eget lacus consectetur, eu scelerisque metus congue. </p>
                                </div>
                                <a href="#" title="" class="uc-btn btn-inline mt-24 mb-24">
                                    profesor@uc.cl
                                </a>
                                <div class="text-right mt-auto">
                                    <a href="#" title="" class="uc-btn btn-inline">
                                        Ver más
                                        <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                    </a>
                                </div>
                                <div class="lsch lsch--hover">
                                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                    </button>
                                    <div id="lsch__popover-1" style="display:none;">
                                        <div class="popover-body">
                                            <video width="150" height="200" controls>
                                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php } ?>
            </div>
        </section>
        <!--Fin Equipo-->
        <!--Noticias PACE UC-->
        <section class="mb-80 mb-lg-8" aria-label="Noticias PACE UC">
            <div class="d-sm-flex justify-content-between align-items-center mb-32">
                <h2 class="uc-h2 title-decorated">Noticias PACE UC</h2>
                <a href="#" title="" class="uc-btn btn-inline d-none d-sm-block">
                    Ver todas las noticias PACE UC
                    <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
                </a>
            </div>
            <div class="grid row no-gutters">
                <div class="col-lg-7 grid__column">
                    <article class="uc-card card-bg--image card-gradient--bottom-blue" style="background-image:url('http://via.placeholder.com/768x1024');">
                        <a href="#" title="" class="uc-tag tag-fixed">PACE UC</a>
                        <div class="uc-card_body justify-content-end">
                            <div class="relative">
                                <a href="#" title="" aria-labelledby="titulo-grid-card-1">
                                    <h3 id="titulo-grid-card-1">Lorem ipsum dolor sit amet</h3>
                                </a>
                                <div class="lsch lsch--hover">
                                    <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-2" data-toggle="popover" data-placement="bottom">
                                    </button>
                                    <div id="lsch__popover-2" style="display:none;">
                                        <div class="popover-body">
                                            <video width="150" height="200" controls>
                                                <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-lg-5 grid__column grid__column--items">
                    <article class="uc-card card-type--horizontal">
                        <img src="http://via.placeholder.com/200x255" class="img-fluid" alt="">
                        <div class="uc-card_body">
                            <a href="#" title="" class="uc-tag">
                                PACE UC
                            </a>
                            <h3 class="uc-h4 mt-16">Lorem ipsum dolor sit amet</h3>
                            <div class="text-right mt-auto">
                                <a href="#" title="" class="uc-btn btn-inline">
                                    Ver más
                                    <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                </a>
                            </div>
                            <div class="lsch lsch--hover">
                                <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                </button>
                                <div id="lsch__popover-1" style="display:none;">
                                    <div class="popover-body">
                                        <video width="150" height="200" controls>
                                            <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="uc-card card-type--horizontal">
                        <img src="http://via.placeholder.com/200x255" class="img-fluid" alt="">
                        <div class="uc-card_body">
                            <a href="#" title="" class="uc-tag">
                                PACE UC
                            </a>
                            <h3 class="uc-h4 mt-16">Lorem ipsum dolor sit amet</h3>
                            <div class="text-right mt-auto">
                                <a href="#" title="" class="uc-btn btn-inline">
                                    Ver más
                                    <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                </a>
                            </div>
                            <div class="lsch lsch--hover">
                                <button aria-label="Desplegar video LSCh" tabindex="0" type="button" class="lsch__button" data-popover-content="#lsch__popover-1" data-toggle="popover" data-placement="bottom">
                                </button>
                                <div id="lsch__popover-1" style="display:none;">
                                    <div class="popover-body">
                                        <video width="150" height="200" controls>
                                            <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
            <div class="text-right d-sm-none">
                <a href="#" title="" class="uc-btn btn-inline">
                    Ver todas las noticias PACE UC
                    <i class="uc-icon icon-shape--rounded" aria-hidden="true">arrow_forward</i>
                </a>
            </div>
        </section>
        <!--Fin Noticias PACE UC-->
    </main>

@endsection
