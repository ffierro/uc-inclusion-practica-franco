@extends('layouts/main')

@section('content')
    <!--Breadcrumb-->
    <ol class="uc-breadcrumb container my-24 my-md-40" aria-label="Breadcrumb">
        <li class="uc-breadcrumb_item">
            <a href="#" title="Portada">Portada</a>
            <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
        </li>
        <li class="uc-breadcrumb_item current">
            Noticias
        </li>
    </ol>
    <!--Fin Breadcrumb-->
    <!--Content-->
    <main crole="main" class="container" id="main-content" aria-label="Noticias">
        <h1 class="uc-h1 mb-32">Noticias</h1>
        <!--Filter-->
        <form class="uc-filters mb-40" aria-label="Filtro de noticias" role="form">
            <fieldset class="no-margin">
                <div class="uc-form-group label-inline mb-0">
                    <label for="categorias">Filtrar por categoría:</label>
                        <select class="uc-input-style w-icon" id="categorias">
                            <option value=''>Todas las categorías (100 artículos)</option>
                            <option value="value1">Categoría 1</option>
                            <option value="value2">Categoría 2</option>
                            <option value="value3">Categoría 3</option>
                        </select>
                    </label>
                </div>
            </fieldset>
        </form>
        <!--Fin Filter-->
        <!--List-->
        <div class="row mb-4 mb-lg-5" aria-label="Listado de noticias">
            <?php for( $a = 0; $a < 8; $a++ ) { ?>
                <div class="col-md-6 col-lg-3 mb-32">
                    <article class="uc-card card-height--same" aria-label="Artículo">
                        <img src="http://via.placeholder.com/300x190" class="img-fluid" alt="">
                        <div class="tags tag-fixed">
                            <a href="#" title="Tag" class="uc-tag">Tag</a>
                        </div>
                        <div class="uc-card_body">
                            <p class="small gray condensed">18 Marzo 2018</p>
                            <h3 class="uc-h4 mb-24" id="articleName">Libro “La UC y Canal 13. De la televisión experimental a la era digital” recorre más de seis décadas de historia conjunta</h3>
                            <div class="text-right mt-auto">
                                <a href="#" aria-labelledby="articleName" title="Ver más de Libro “La UC y Canal 13. De la televisión experimental a la era digital” recorre más de seis décadas de historia conjunt" class="uc-btn btn-inline">
                                    Ver más
                                    <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i>
                                </a>
                            </div>
                            <div class="lsch lsch--hover">
                                <button tabindex="0" type="button" class="lsch__button"
                                        data-popover-content="#lsch__popover" data-toggle="popover"
                                        data-placement="bottom">
                                </button>
                                <div id="lsch__popover" style="display:none;">
                                    <div class="popover-body">
                                        <video width="150" height="200" controls>
                                            <source src="https://storage.googleapis.com/coverr-main/mp4%2Fcoverr-amalfi-landscape-1570370982512.mp4"
                                                    type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            <?php } ?>
            <div class="col-12">
                <!--Paginador-->
                <ol class="paginator mt-1">
                    <li class="paginator__arrow" ><a href=""><i class="uc-icon" aria-hidden="true">keyboard_arrow_left</i></a></li>
                    <li class="paginator__item paginator__item--active"><a href="">1</i></a></li>
                    <li class="paginator__item"><a href="">2</i></a></li>
                    <li class="paginator__item" ><a href="">3</i></a></li>
                    <li class="paginator__item" ><a href="">4</i></a></li>
                    <li class="paginator__item" ><a href="">5</i></a></li>
                    <li class="paginator__arrow" ><a href=""><i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i></a></li>
                </ol>
                <!--Fin Paginador-->
            </div>
        </div>
        <!--Fin List-->
    </main>
    <!--Fin Content-->
@endsection
