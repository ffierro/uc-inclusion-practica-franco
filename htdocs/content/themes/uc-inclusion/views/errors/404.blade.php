@extends('layouts.main')

@section('content')
    <main role="main" id="main-content" class="mb-5">
        <div class="bg-blue--xs py-32 py-lg-4 mb-32 mb-lg-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-xl-8">
                        <div class="card bg-lightgray mt-40 mt-md-5">
                            <div class="p-40">
                                <h4 class="uc-subtitle">Error 404</h4>
                                <h1 class="uc-h1 mb-24">Página no encontrada</h1>
                                <div class="paragaph mb-16">
                                    <p>Lo sentimos, la página que estás buscando ha sido <strong>modificada, borrada o no existe</strong>.</p>
                                </div>

                                <a href="/" class="uc-btn btn-inline">Volver a la página de inicio <i class="uc-icon" aria-hidden="true">keyboard_arrow_right</i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection