<?php


class SiteFunction
{

	public static function getCropImageByAttaachmentName($attachmentId, $name)
	{
		$imageUrl = wp_get_attachment_image_url($attachmentId, $name);
		$image_id = get_post_thumbnail_id($attachmentId);


		return [
			'src'   => $imageUrl,
			'alt'   => get_post_meta($image_id, '_wp_attachment_image_alt', true),
			'title' => get_the_title($image_id)
		];


	}

	public static function getPostImageByName($postId, $name)
	{
		$imageUrl = get_the_post_thumbnail_url($postId, $name);
		$image_id = get_post_thumbnail_id($postId);


		return [
			'src'   => $imageUrl,
			'alt'   => get_post_meta($image_id, '_wp_attachment_image_alt', true),
			'title' => get_the_title($image_id)
		];


	}

	public static function sortArrayByArray(array $array, array $orderArray)
	{
		$ordered = array();
		foreach ($orderArray as $key) {
			if (array_key_exists($key, $array)) {
				$ordered[$key] = $array[$key];
				unset($array[$key]);
			}
		}
		return $ordered + $array;
	}

	public static function getItemsMenu($menu)
	{
		return wp_get_nav_menu_items($menu);

	}

	public static function getGlobalsOptions()
	{
		return [
			'theme_config' => get_fields('theme_config'),

		];

	}

	public static function getImageData($post_id, $size = [100, 100], $crop = false)
	{
		if (!$image_id = get_post_thumbnail_id($post_id)) {
			return;
		}

		if (!$image = fly_get_attachment_image_src($image_id, $size, $crop)) {
			return;
		}

		return [
			'src'    => $image['src'],
			'width'  => $image['width'],
			'height' => $image['height'],
			'alt'    => get_post_meta($image_id, '_wp_attachment_image_alt', true)
		];
	}

	public static function getImageDataFromAttachment($attachment_id, $size = [100, 100], $crop = false)
	{

		if (!$image = fly_get_attachment_image_src($attachment_id, $size, $crop = false)) {
			return;
		}


		return [
			'src'    => $image['src'],
			'width'  => $size[0],
			'height' => $size[1],
			'alt'    => get_post_meta($attachment_id, '_wp_attachment_image_alt', true)


		];
	}

	public static function getPostTermsFilterByTaxonomies($post_id, $taxonomy)
	{
		return get_the_terms($post_id, $taxonomy);
	}

	public static function filesize_formatted($bytes)
	{

		if ($bytes >= 1073741824) {
			$bytes = number_format($bytes / 1073741824, 2) . ' GB';
		} elseif ($bytes >= 1048576) {
			$bytes = number_format($bytes / 1048576, 2) . ' MB';
		} elseif ($bytes >= 1024) {
			$bytes = number_format($bytes / 1024, 2) . ' KB';
		} elseif ($bytes > 1) {
			$bytes = $bytes . ' bytes';
		} elseif ($bytes == 1) {
			$bytes = $bytes . ' byte';
		} else {
			$bytes = '0 bytes';
		}

		return $bytes;
	}

	public static function generateSocialShareLink($url, $title)
	{

		$url = urlencode($url);
		$facebook_url = "https://www.facebook.com/sharer/sharer.php?u=" . $url . "&t=$title";
		$twitter_url = "https://twitter.com/share?url=$url&via=TWITTER_HANDLE&text=$title";

		return [
			'facebook_url' => $facebook_url,
			'twitter_url'  => $twitter_url,
		];

	}
}