<?php

use Themosis\Support\Facades\Action;

Action::add('after_setup_theme', function () {
    add_image_size('equipo-card', 300, 350, true);
    add_image_size('entry-card', 768, 430, true);
    add_image_size('gallery-card', 560, 380, true);
});
