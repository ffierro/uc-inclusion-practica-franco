<?php

use Themosis\Support\Facades\Action;
use Themosis\Support\Facades\Filter;
use Themosis\Support\Facades\Page;

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 *
 * @return array
 */
Filter::add('body_class', function ($classes) {
	// Adds a class of hfeed to non-singular pages.
	if (!is_singular()) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if (!is_active_sidebar('sidebar-1')) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
});

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
Action::add('wp_head', function () {
	if (is_singular() && pings_open()) {
		echo '<link rel="pingback" href="' . esc_url(get_bloginfo('pingback_url')) . '">';
	}
});

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 */
Action::add('after_setup_theme', function () {
	$GLOBALS['content_width'] = 640;
}, 0);

Filter::add('acf/settings/save_json', function () {
	// update path
	$path = get_stylesheet_directory() . '/acf-json';

	return $path;
});
Filter::add('acf/settings/load_json', function ($paths) {
	unset($paths[0]);
	$paths[] = get_stylesheet_directory() . '/acf-json';
	return $paths;
});
Filter::add('rest_authentication_errors', function ($result) {
	return new WP_Error( 'error', 'off', array( 'status' => 401 ) );
//
//	if ( ! empty( $result ) ) {
//		return $result;
//	}
//	if ( ! is_user_logged_in() ) {
//		return new WP_Error( 'rest_not_logged_in', 'You are not currently logged in.', array( 'status' => 401 ) );
//	}
//	return $result;
});





//remove_filter('the_content', 'wpautop');


if (function_exists('acf_add_options_page')) {

	acf_add_options_page(array(
		'page_title' => 'Configuraciones Globales',
		'menu_title' => 'Configuraciones Globales',
		'menu_slug'  => 'theme-config',
		'post_id'    => 'theme_config',
		'capability' => 'edit_posts',
		'redirect'   => false
	));


}

Action::add('admin_menu', function () {
	remove_meta_box('tagsdiv-post_tag', 'post', 'side');
	remove_meta_box('formatdiv', 'post', 'side');
	remove_meta_box('categorydiv', 'post', 'side');
	remove_meta_box('tipo_de_entradadiv', 'post', 'side');
	remove_meta_box('tagsdiv-categorias', 'noticias', 'side');
	remove_meta_box('tagsdiv-categorias_noticias', 'noticias', 'side');
	remove_meta_box('tagsdiv-etiqueta', 'noticias', 'side');
	remove_meta_box('tagsdiv-unidad_academico', 'noticias', 'side');
	remove_meta_box('tagsdiv-eje_estrategico', 'noticias', 'side');
	remove_meta_box('tagsdiv-clasificacion-contenidos', 'post', 'side');
	remove_meta_box('tagsdiv-categorias', 'equipo', 'side');
});
