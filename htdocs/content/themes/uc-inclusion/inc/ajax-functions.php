<?php

Ajax::listen('likeNew', function () {

    $post_id = $_POST['post_id'];
    if (!is_numeric($post_id)) {
        return;
    }

    if (!$post = get_post($post_id)) {
        return;
    }
    if (isset($_COOKIE['likePublish']) && $_COOKIE['likePublish'] == $post_id) {
        return;
    }

    setcookie('likePublish', $post_id, time() + 31556926, '/');

    $counter = get_field('contadores', $post->ID);
    $counter++;


    update_field('contadores', $counter, $post->ID);


    return wp_send_json([
        'status' => 'ok',
        'counter' => $counter
    ]);


});