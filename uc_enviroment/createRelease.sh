#!/bin/bash
PROJECT_NAME='uc-inclusion'
PROJECT_FOLDER='/home/camisi/docker/sites/uc-inclusion'
PROJECT_FOLDER_PLUGINS='/home/camisi/docker/sites/uc-inclusion/htdocs/content/plugins'
PROJECT_FOLDER_THEME='/home/camisi/docker/sites/uc-inclusion/htdocs/content/themes'
PROJECT_FOLDER_UPLOADS='/home/camisi/docker/sites/uc-inclusion/htdocs/content/uploads'

PROJECT_DESTINY_FOLDER_SRC='/home/camisi/docker/sites/uc-inclusion-cliente-entregable/base'
PROJECT_DESTINY_FOLDER_PLUGINS='/home/camisi/docker/sites/uc-inclusion-cliente-entregable/plugins'
PROJECT_DESTINY_FOLDER_THEME='/home/camisi/docker/sites/uc-inclusion-cliente-entregable/themes'
PROJECT_DESTINY_FOLDER_UPLOADS='/home/camisi/docker/sites/uc-inclusion-cliente-entregable/uploads'

PROJECT_DESTINY_FOLDER_COMPRESS='/home/camisi/docker/sites/uc-inclusion-cliente-entregable'
PUT_FOLDER_COMPRESS='/home/camisi/docker/sites'


#PROJECT_DESTINY_COMPRESS_FOLDER='/home/camisi/docker/sites/uc-inclusion-cliente-data'


rsync -avz $PROJECT_FOLDER/ $PROJECT_DESTINY_FOLDER_SRC --exclude '.git/*' --exclude '.env' --exclude 'uc_enviroment'
rsync -avz $PROJECT_FOLDER_PLUGINS/ $PROJECT_DESTINY_FOLDER_PLUGINS --exclude '.git/*' --exclude '.env'
rsync -avz $PROJECT_FOLDER_THEME/ $PROJECT_DESTINY_FOLDER_THEME --exclude '.git/*' --exclude '.env'
rsync -avz $PROJECT_FOLDER_UPLOADS/ $PROJECT_DESTINY_FOLDER_UPLOADS --exclude '.git/*' --exclude '.env'

cp .htaccess  $PROJECT_DESTINY_FOLDER_SRC/base
cp index.php  $PROJECT_DESTINY_FOLDER_SRC/base
cp wordpress.php $PROJECT_DESTINY_FOLDER_SRC/base/config
#mkdir -p $PROJECT_DESTINY_COMPRESS_FOLDER
tar -czvf $PUT_FOLDER_COMPRESS/$PROJECT_NAME-cliente.tar.gz $PROJECT_DESTINY_FOLDER_COMPRESS