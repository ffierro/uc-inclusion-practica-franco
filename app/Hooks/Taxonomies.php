<?php

namespace App\Hooks;

use Themosis\Hook\Hookable;
use Themosis\Support\Facades\Taxonomy;


class Taxonomies extends Hookable
{
    public function register()
    {

        Taxonomy::make('categorias', ['equipo'], 'Categorías', 'Categoría')->set([
            'public'             => true,
            'show_in_nav_menus'  => false,
            'hierarchical'       => true,
            'show_tagcloud'      => false,
            'show_in_quick_edit' => false,
            'show_admin_column'  => true,
            'query_var'          => true,

        ])->set();

        Taxonomy::make('categorias_noticias', ['noticias'], 'Categorías Noticias', 'Categorías Noticias')->set([
            'public'             => true,
            'show_in_nav_menus'  => false,
            'hierarchical'       => true,
            'show_tagcloud'      => false,
            'show_in_quick_edit' => false,
            'show_admin_column'  => true,
            'query_var'          => true,

        ])->set();

        Taxonomy::make('clasificacion-contenidos', ['post'], 'Clasificación Contenidos', 'Clasificación Contenido')->set([
            'public'             => true,
            'show_in_nav_menus'  => false,
            'hierarchical'       => true,
            'show_tagcloud'      => false,
            'show_in_quick_edit' => false,
            'show_admin_column'  => true,
            'query_var'          => true,

        ])
            ->set();
            

        Taxonomy::make('etiqueta_descargas', ['descarga'], 'Etiqueta descargas', 'Etiqueta descarga')->setArguments([
            'public'       => true,
            'rewrite'      => true,
            'query_var'    => true,
            'hierarchical' => true,
            'show_ui'      => true,
            'has_archive'  => false,
            ])
            ->set();

//        Taxonomy::make('tipo-entrada', 'post', 'Tipo de Entradas', 'Tipo de Entrada')->set([
//            'public'             => true,
//            'show_in_nav_menus'  => true,
//            'hierarchical'       => false,
//            'show_tagcloud'      => true,
//            'show_in_quick_edit' => false,
//            'show_admin_column'  => false,
//            'query_var'          => false,
//            'show_ui'            => true,
//
//
//        ])
//            ->set();

    }
}