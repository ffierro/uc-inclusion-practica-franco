<?php

namespace App\Hooks;

use Themosis\Hook\Hookable;
use Themosis\Support\Facades\PostType;

class PostTypes extends Hookable
{
    public function register()
    {
//        PostType::make('programas', 'Programas', 'Programa')->setArguments([
//            'public' => true,
//            'menu_position' => 20,
//            'supports' => ['title', 'editor','thumbnail'],
//            'rewrite' => false,
//            'query_var' => false
//        ])
//            ->set();
        PostType::make('noticias', 'Noticias', 'Noticia')->setArguments([
            'public'        => true,
            'menu_position' => 21,
            'supports'      => ['title', 'editor', 'thumbnail'],
            'rewrite'       => true,
            'query_var'     => true
        ])
            ->set();
        PostType::make('equipo', 'Equipos', 'equipo')->setArguments([
            'public'        => true,
            'has_archive'   => false,
            'menu_position' => 21,
            'supports'      => ['title', 'editor', 'thumbnail'],
            'rewrite'       => true,
            'query_var'     => true
        ])
            ->set();
        PostType::make('descarga', 'Descargas', 'Descarga')->setArguments([
            'has_archive'   => false,
            'public'        => true,
            'menu_position' => 21,
            'supports'      => ['title', 'editor', 'thumbnail'],
            'rewrite'       => true,
            'query_var'     => true
        ])
            ->set();
//        PostType::make('testimonios', 'Testimonios', 'Testimonio')->setArguments([
//            'public' => true,
//            'menu_position' => 22,
//            'supports' => ['title', 'editor'],
//            'rewrite' => false,
//            'query_var' => false
//        ])
//            ->set();
//        PostType::make('articulos', 'Articulos', 'Articulo')->setArguments([
//            'public' => true,
//            'menu_position' => 22,
//            'supports' => ['title', 'editor'],
//            'rewrite' => false,
//            'query_var' => false
//        ])
//            ->set();
    }
}