<?php

namespace App\Hooks;

use Themosis\Hook\Hookable;
use Themosis\Support\Facades\Ajax;
use Themosis\Support\Facades\Filter;

class SiteAjax extends Hookable
{
    public function register()
    {
        // This file is stored in app/Hooks/Ajax.php
        // This code listens for logged in and logged out users
        Ajax::listen('my-custom-action', function () {
            // Check nonce value
            // check_ajax_referer('add-posts', 'security');

            // Run custom code - Make sure to sanitize and check values before
            $result = 2;

            // "Return" the result
            echo $result;

            // Close
            die();
        });

        Ajax::listen('getProjectsByTab', function () {
            $type = '';
            // Check nonce value
            check_ajax_referer('add-posts', 'security');

            $tab = sanitize_text_field($_POST['tab']);
            $search = sanitize_text_field($_POST['keyword']);
            $search_2 = sanitize_text_field($_POST['keyword']);

            if ($tab == 'tab-01') {
                $type = 'proyecto';
            } else {
                $type = 'recursos';
            }

     
          /* $consulta1 = [];
           $consulta2 = [];
           $consulta1 = get_posts(
                array(
                    "s" => $search,
                    'posts_per_page' => -1,
                    'post_type' => 'descarga',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'etiqueta_descargas',
                            'field' => 'slug',
                            'terms' => sanitize_title($search),
                            
                            
                           

                        ),
                        )
                        
                )
                    );*/
                    $term = get_terms([
                        'name__like' => $search,
                        'fields' => 'ids',
                    ]);

                    $args = array (
                        'post_type' => 'descarga',
                        'post_status' => 'publish',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'etiqueta_descargas',
                                'field' => 'id',
                                'terms' => $term,
                               
                                
                               
    
                            ),
                        ),
                       
                        'posts_per_page' => -1,
                        'title_filter' => $search,
                        'title_filter_relation' => 'OR',
                        
                       
                      );
                    
                     
                     $filter= Filter::add('posts_where', 'App\Hooks\Filters@TaxTitle');
                    
                      $the_query = new \WP_Query($args); //jus query
                    
                      $filter->remove('posts_where');
            
                      
                  /*  $consulta2 = get_posts([
                        's' => $search, 'post_type' => 'descarga', 'posts_per_page' => -1, 'meta_query' => [
                            'relation' => 'AND',
                            [
                                'key'     => 'tipo_de_descarga',
                                'value'   => $type,
                                'compare' => 'LIKE',
                            ],
    
                        ]
                    ]
                );*/
    
                   
            /*$consulta2 = get_posts(
                array(
                            
                    'posts_per_page' => -1,
                    'post_type' => 'descarga',
                    's'		=> $title,
	                'value'	=> sanitize_title($search),
	                'operator'	=> 'IN',
                                
                        )
                            );
                           
                  
                     /*   'relation' => 'OR',
                        
                    ),
                    'meta_query' => array(

                        array(
                
                            'key' => 'post_title',
                
                            'value' => 'test',
                
                            'compare' => 'LIKE',
                
                        ),
                        'relation' => 'OR',

    ),
                   
                ),
            );*/
            $posts = [];
         if($the_query->have_posts()){
            $posts = $the_query->posts;

         }
            
              
                $results = view('parts/projects_resources_downloads_projects', ['resources' => $posts])->render();
           
            
            // "Return" the result
            echo json_encode([
                'results' => count($posts),
                'content' => $results,
                'tab'     => $tab
            ]);

            // Close
            die();
        });
    }
}