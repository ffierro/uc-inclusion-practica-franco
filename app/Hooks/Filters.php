<?php
namespace App\Hooks;

class Filters
{
  public function searchTitleLike($where, $wp_query)
  {
    global $wpdb;
    if ($search_term = $wp_query->get('title_filter')) {
      $search_term = $wpdb->esc_like($search_term); //instead of esc_sql()
      $search_term = ' \'%' . $search_term . '%\'';
      $title_filter_relation = (strtoupper($wp_query->get('title_filter_relation')) == 'OR' ? 'OR' : 'AND');
      $where .= ' ' . $title_filter_relation . ' ' . $wpdb->posts . '.post_title LIKE ' . $search_term;
    }
    return $where;
  }
  public function TaxTitle($where, $wp_query)
  {
    global $wpdb;
    if ($search_term = $wp_query->get('title_filter')) {
      $search_term = $wpdb->esc_like($search_term); //instead of esc_sql()
      $search_term = ' \'%' . $search_term . '%\'';
      $title_filter_relation = (strtoupper($wp_query->get('title_filter_relation')) == 'OR' ? 'OR' : 'AND');
    if($type = $wp_query->get('post_type')){
      $type = ' \''. $type . '\'';
      $where .= ' ' . $title_filter_relation . ' ' . $wpdb->posts . '.post_title LIKE ' . $search_term. ' ' . 'AND' . ' ' . $wpdb->posts . '.post_type=' . $type. ' ' . 'AND' . ' ' . $wpdb->posts . '.post_status = "publish" ';
    
    } else {
      $where .= ' ' . $title_filter_relation . ' ' . $wpdb->posts . '.post_title LIKE ' . $search_term. ' ' . 'AND' . ' ' . $wpdb->posts . '.post_status = "publish" ';
    }
  }
    return $where;
  }
}